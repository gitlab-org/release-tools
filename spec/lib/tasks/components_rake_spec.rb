# frozen_string_literal: true

require 'rake_helper'

RSpec.describe 'components', :rake do
  describe 'update_gitaly', task: 'components:update_gitaly' do
    it 'calls the UpdateGitaly task' do
      expect(ReleaseTools::Tasks::Components::UpdateGitaly).to receive(:new)
        .and_return(instance_double(ReleaseTools::Tasks::Components::UpdateGitaly, execute: true))
      task.invoke
    end
  end

  describe 'update_kas', task: 'components:update_kas' do
    context 'when the automatic_kas_update feature flag is enabled' do
      before do
        enable_feature(:automatic_kas_update)
      end

      it 'calls the UpdateKas task' do
        expect(ReleaseTools::Tasks::Components::UpdateKas).to receive(:new)
          .and_return(instance_double(ReleaseTools::Tasks::Components::UpdateKas, execute: true))
        task.invoke
      end
    end

    context 'when the automatic_kas_update feature flag is disabled' do
      before do
        disable_feature(:automatic_kas_update)
      end

      it 'logs a message and does nothing' do
        expect(ReleaseTools::Tasks::Components::UpdateKas).not_to receive(:new)
        expect(ReleaseTools.logger).to receive(:info).with('automatic_kas_update feature flag disabled. Nothing to do')
        task.invoke
      end
    end
  end
end
