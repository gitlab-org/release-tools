# frozen_string_literal: true

require 'rake_helper'

RSpec.describe 'auto_build', :rake do
  describe 'timer', task: 'auto_build:timer' do
    it 'calls ReleaseTools::Tasks::AutoBuild::Timer' do
      expect(ReleaseTools::Tasks::AutoBuild::Timer)
        .to receive(:new)
        .and_return(instance_double(ReleaseTools::Tasks::AutoBuild::Timer, execute: true))

      task.invoke
    end
  end
end
