# frozen_string_literal: true

require 'rake_helper'

describe 'release tasks', :rake do
  describe 'issue', task: 'release:issue' do
    it_behaves_like 'handling invalid version args'

    it 'creates a release issue for the specified version' do
      expect(ReleaseTools::Tasks::Release::Issue).to receive(:new)
        .with('1.0.1')
        .and_return(instance_double(ReleaseTools::Tasks::Release::Issue, execute: true))

      task.invoke('1.0.1')
    end
  end

  describe 'patch_blog_post', task: 'release:patch_blog_post' do
    let(:coordinator) { instance_double(ReleaseTools::PatchRelease::Coordinator) }

    let(:blog_post) do
      ReleaseTools::PatchRelease::BlogMergeRequest.new(
        patch_coordinator: coordinator,
        security_fixes: []
      )
    end

    before do
      allow(ReleaseTools::PatchRelease::Coordinator)
        .to receive(:new)
        .and_return(coordinator)

      allow(blog_post)
        .to receive_messages(create: nil, generate_blog_content: nil, exists?: false, title: 'Title', description: 'description', remote_issuable: build(:issue))
    end

    context 'in dry run mode' do
      it 'does not create MR' do
        expect(ReleaseTools::PatchRelease::BlogMergeRequest)
          .to receive(:new).with(
            patch_coordinator: coordinator,
            security_fixes: []
          ).and_return(blog_post)

        expect(blog_post).not_to receive(:create)

        task.invoke('1.0.1')
      end
    end

    context 'with a version' do
      it 'creates a blog post' do
        expect(ReleaseTools::PatchRelease::BlogMergeRequest)
          .to receive(:new).with(
            patch_coordinator: coordinator,
            security_fixes: []
          ).and_return(blog_post)

        expect(blog_post).to receive(:create)

        without_dry_run { task.invoke('1.0.1') }
      end
    end

    context 'without a version' do
      let(:generate_blog_post) do
        instance_double(ReleaseTools::Security::ReleasePreparation::GenerateBlogPost)
      end

      it 'calls Security::ReleasePreparation::GenerateBlogPost' do
        expect(ReleaseTools::Security::ReleasePreparation::GenerateBlogPost)
          .to receive(:new)
          .and_return(generate_blog_post)

        expect(generate_blog_post).to receive(:execute)

        expect(ReleaseTools::PatchRelease::BlogMergeRequest).not_to receive(:new)
        without_dry_run { task.invoke }
      end
    end
  end
end
