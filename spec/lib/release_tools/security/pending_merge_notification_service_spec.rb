# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::PendingMergeNotificationService do
  subject(:notification_service) { described_class.new }

  let(:client) { stub_const('ReleaseTools::Security::Client', spy) }
  let(:crawler) { instance_double(ReleaseTools::Security::IssueCrawler, upcoming_security_issues_and_merge_requests: security_issues) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:merge_request) { create(:merge_request, web_url: 'mr url') }
  let(:default_merge_request_handled_boolean) { true }
  let(:merge_requests) { [merge_request] }
  let(:opened_merge_requests_targeting_default_branch) { [] }
  let(:security_issues) do
    [
      create(
        :issue,
        default_merge_request_handled?: default_merge_request_handled_boolean,
        web_url: 'issue url',
        merge_request_targeting_default_branch: merge_request,
        merge_requests_targeting_default_branch: merge_requests,
        opened_merge_requests_targeting_default_branch: opened_merge_requests_targeting_default_branch
      )
    ]
  end

  before do
    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(crawler)

    allow(notifier)
      .to receive(:new)
      .and_return(notifier)
  end

  describe '#execute' do
    context 'when all the default merge requests are handled' do
      it 'returns all mrs have been merged on success status' do
        expect(notification_service.logger).to receive(:info).with(/All default merge requests have been successfully merged/)

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Pending merge',
            status: :success,
            release_type: :patch
          )

        notification_service.execute
      end
    end

    context 'when there are pending default merge requests' do
      let(:default_merge_request_handled_boolean) { false }

      it 'outputs information about pending merge issues and raises' do
        logger_output = "

❌ The following issues have not had their default merge requests successfully merged:

- Issue: issue url - Merge Request: mr url"

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Pending merge',
            status: :failed,
            release_type: :patch
          )

        expect(notification_service.logger).to receive(:info).with(logger_output)

        expect { notification_service.execute }.to raise_error(described_class::PendingMergeError)
      end
    end

    context 'when there is no default merge request for a non-reduced backport issue' do
      let(:merge_request) { nil }
      let(:default_merge_request_handled_boolean) { false }

      it 'outputs that there is no default merge request in the log' do
        logger_output = "

❌ The following issues have not had their default merge requests successfully merged:

- Issue: issue url - No default merge request found"

        expect(notification_service.logger).to receive(:info).with('There are more than one merge requests targeting the default branch. All were merged, so continuing to process.')
        expect(notification_service.logger).to receive(:info).with(logger_output)

        expect { notification_service.execute }.to raise_error(described_class::PendingMergeError)
      end
    end

    context 'when the job is unable to complete and an error is raised' do
      before do
        allow(ReleaseTools::Security::IssueCrawler)
          .to receive(:new)
          .and_raise(Gitlab::Error::Error)
      end

      it 'outputs manual instructions and notifies slack' do
        logger_output = "Pending merge check failed. If this job continues to fail, the status of the security implementation issues and their default MRs should be checked manually."

        expect(notification_service.logger).to receive(:fatal).with(logger_output, error: anything)

        expect { notification_service.execute }.to raise_error(Gitlab::Error::Error)
      end
    end

    context 'when there are more than one MR targeting default branch' do
      let(:incorrect_merge_request) do
        create(
          :merge_request,
          web_url: 'mr url'
        )
      end

      let(:merge_requests) { [merge_request, incorrect_merge_request] }

      context 'and one is open' do
        let(:opened_merge_requests_targeting_default_branch) { [incorrect_merge_request] }

        it 'raises an error' do
          expect { notification_service.execute }.to raise_error(described_class::PendingMergeError)
        end
      end

      context 'and all were merged' do
        it 'continue processing' do
          expect(notification_service.logger).to receive(:info).with('There are more than one merge requests targeting the default branch. All were merged, so continuing to process.')
          expect(notification_service.logger).to receive(:info).with(/All default merge requests have been successfully merged/)

          expect(notifier)
            .to receive(:new)
            .with(
              job_type: 'Pending merge',
              status: :success,
              release_type: :patch
            )

          notification_service.execute
        end
      end
    end
  end
end
