# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::IssueHelper do
  before do
    foo_class = Class.new do
      include ReleaseTools::Security::IssueHelper
    end

    stub_const('FooClass', foo_class)
  end

  let(:foo_class) { FooClass.new }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }

  describe '#security_tracking_issue' do
    it 'fetches the security tracking issue' do
      expect(client).to receive(:next_security_tracking_issue)

      foo_class.security_tracking_issue
    end
  end

  describe '#security_task_issue' do
    it 'fetches the security task issue' do
      expect(client).to receive(:current_security_task_issue)

      foo_class.security_task_issue
    end
  end

  describe '#reference_issue' do
    before do
      allow(client)
        .to receive_messages(
          next_security_tracking_issue: 'tracking issue',
          current_security_task_issue: 'task issue'
        )

      allow(ReleaseTools::SharedStatus)
        .to receive(:critical_patch_release?)
        .and_return(critical_patch_release?)
    end

    let(:critical_patch_release?) { false }

    it 'returns the tracking issue' do
      expect(foo_class.reference_issue).to eq('tracking issue')
    end

    context 'critical patch release' do
      let(:critical_patch_release?) { true }

      it 'returns the task issue' do
        expect(foo_class.reference_issue).to eq('task issue')
      end
    end
  end
end
