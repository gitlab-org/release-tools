# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Prepare::ComponentBranchVerifier do
  describe '#execute' do
    subject(:component_branch_verifier) { described_class.new }

    let(:patch_versions) do
      [
        ReleaseTools::Version.new("17.1.1"),
        ReleaseTools::Version.new("16.10.3"),
        ReleaseTools::Version.new("15.11.5")
      ]
    end

    before do
      allow(ReleaseTools::Versions).to receive(:next_versions).and_return(patch_versions)
    end

    it 'calls the BranchStatusService class' do
      expect(ReleaseTools::Services::BranchesStatusService)
        .to receive(:new)
        .with(release_type: :patch, versions: ["17.1.1", "16.10.3", "15.11.5"])
        .and_return(instance_double(ReleaseTools::Services::BranchesStatusService, execute: 'foo'))

      component_branch_verifier.execute
    end
  end
end
