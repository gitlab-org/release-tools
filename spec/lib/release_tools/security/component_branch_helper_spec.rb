# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::ComponentBranchHelper do
  describe '#generate_link_list' do
    before do
      foo_class = Class.new do
        include ReleaseTools::Security::ComponentBranchHelper
      end

      stub_const('FooClass', foo_class)
    end

    let(:foo_class) { FooClass.new }

    let(:ee_project) { double(:ee_project, metadata_project_name: 'gitlab-ee', security_path: 'security/gitlab', ee_branch?: true, default_branch: 'master') }
    let(:project) { double(:project, metadata_project_name: 'project', security_path: 'security/project', ee_branch?: false, default_branch: 'main') }
    let(:projects) { [ee_project, project] }
    let(:versions) { [double(stable_branch: '1.0'), double(stable_branch: '2.0')] }

    context "on default branches" do
      it 'yields the metadata for listing the project branches' do
        expect { |b| foo_class.generate_link_list(projects, versions, &b) }.to yield_successive_args(
          ['gitlab-ee', '1.0', 'https://gitlab.com/security/gitlab/-/commits/1.0'],
          ['gitlab-ee', '2.0', 'https://gitlab.com/security/gitlab/-/commits/2.0'],
          ['project', '1.0', 'https://gitlab.com/security/project/-/commits/1.0'],
          ['project', '2.0', 'https://gitlab.com/security/project/-/commits/2.0']
        )
      end
    end

    context "on stable branches" do
      it 'yields the metadata for listing the project stable branches' do
        expect { |b| foo_class.generate_link_list(projects, versions, &b) }.to yield_successive_args(
          ['gitlab-ee', '1.0', 'https://gitlab.com/security/gitlab/-/commits/1.0'],
          ['gitlab-ee', '2.0', 'https://gitlab.com/security/gitlab/-/commits/2.0'],
          ['project', '1.0', 'https://gitlab.com/security/project/-/commits/1.0'],
          ['project', '2.0', 'https://gitlab.com/security/project/-/commits/2.0']
        )
      end
    end
  end
end
