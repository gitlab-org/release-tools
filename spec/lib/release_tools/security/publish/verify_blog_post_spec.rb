# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Publish::VerifyBlogPost do
  subject(:verify_service) { described_class.new }

  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:pending_blog_post_mr) { nil }

  before do
    allow(client)
      .to receive(:security_blog_merge_request)
      .and_return(pending_blog_post_mr)
  end

  describe '#execute' do
    context 'when the blog post has been merged' do
      it 'returns a success status' do
        expect(verify_service.logger).to receive(:info).with("Blog post has been merged")

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Verify blog post',
            status: :success,
            release_type: :patch
          )

        verify_service.execute
      end
    end

    context 'when the blog post has not been merged' do
      let(:pending_blog_post_mr) do
        create(
          :merge_request,
          iid: 2,
          web_url: 'https://test.com/www-gitlab-com/-/merge_requests/1',
          sha: '123',
          status: 'opened'
        )
      end

      it 'outputs a failure status and raises' do
        expect(verify_service.logger).to receive(:info).with(
          "Blog post has not been merged",
          { pending_blog_post_mr: "https://test.com/www-gitlab-com/-/merge_requests/1" }
        )

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Verify blog post',
            status: :failed,
            release_type: :patch
          )

        expect { verify_service.execute }.to raise_error(described_class::PendingMergeError)
      end
    end
  end
end
