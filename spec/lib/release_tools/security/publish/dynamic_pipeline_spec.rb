# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Publish::DynamicPipeline do
  let(:versions) { ['1.0.1', '1.1.1', '1.2.3'] }

  describe '#generate' do
    it 'generates YAML output' do
      expected_yaml = {
        "stages" => versions.map { |version| "security_release:publish:#{version}" },
        "security_release_publish:1.0.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "security_release:publish:1.0.1",
          "script" => [
            "source scripts/setup_ssh.sh",
            "source scripts/setup_git.sh",
            "bundle exec rake security:publish[1.0.1]"
          ],
          "extends" => [
            ".with-bundle",
            ".common-ci-tokens"
          ]
        },
        "security_release_publish:1.1.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "security_release:publish:1.1.1",
          "script" => [
            "source scripts/setup_ssh.sh",
            "source scripts/setup_git.sh",
            "bundle exec rake security:publish[1.1.1]"
          ],
          "extends" => [
            ".with-bundle",
            ".common-ci-tokens"
          ]
        },
        "security_release_publish:1.2.3" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "security_release:publish:1.2.3",
          "script" => [
            "source scripts/setup_ssh.sh",
            "source scripts/setup_git.sh",
            "bundle exec rake security:publish[1.2.3]"
          ],
          "extends" => [
            ".with-bundle",
            ".common-ci-tokens"
          ]
        }
      }

      publish_jobs = described_class.new(versions)
      expect(publish_jobs.generate).to eq(expected_yaml.to_yaml)
    end
  end
end
