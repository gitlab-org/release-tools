# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::PendingIssuesNotificationService do
  subject(:notification_service) { described_class.new }

  let(:client) { ReleaseTools::GitlabClient }

  let(:security_tracking_issue) do
    build(:issue, id: 1, project_id: 1, url: 'tracking issue url')
  end

  let(:security_implementation_issue) do
    instance_double(
      ReleaseTools::Security::ImplementationIssue,
      web_url: 'security implementation issue url'
    )
  end

  let(:issues_crawler) do
    instance_double(
      ReleaseTools::Security::IssueCrawler,
      related_security_issues: [security_implementation_issue]
    )
  end

  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  let(:error_message) { "Pending issues check failed. If this job continues to fail, manually check that the issues linked to the tracking issue (tracking issue url) are ready to be processed (labeled with `security-target`), and unlink them if they are not ready." }

  before do
    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(issues_crawler)

    allow(ReleaseTools::Issue)
      .to receive(:new)
      .and_return(security_tracking_issue)

    allow(notifier)
      .to receive(:new)
      .and_return(notifier)

    allow(client)
      .to receive(:next_security_tracking_issue)
      .and_return(security_tracking_issue)
  end

  describe '#execute' do
    context 'when all the linked issues are ready' do
      before do
        allow(security_implementation_issue).to receive(:ready_to_be_processed?).and_return(true)
      end

      it 'returns a success status' do
        expect(notification_service.logger).to receive(:info).with("All the issues linked to the security tracking issue are ready.")

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Pending issues',
            status: :success,
            release_type: :patch
          )

        notification_service.execute
      end
    end

    context 'when there are pending linked issues' do
      before do
        allow(security_implementation_issue).to receive(:ready_to_be_processed?).and_return(false)
      end

      it 'outputs information about the issues and raises' do
        logger_output = "

❌ Some linked issues for the tracking issue tracking issue url are not ready to be processed:

- security implementation issue url"

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Pending issues',
            status: :failed,
            release_type: :patch
          )

        expect(notification_service.logger).to receive(:info).with(logger_output)
        expect(notification_service.logger).to receive(:fatal).with(error_message, error: anything)

        expect { notification_service.execute }.to raise_error(described_class::PendingIssuesError)
      end
    end

    context 'when the job is unable to complete and an error is raised' do
      before do
        allow(security_implementation_issue)
          .to receive(:ready_to_be_processed?)
          .and_raise(Gitlab::Error::Error)
      end

      it 'outputs manual instructions and notifies slack' do
        expect(notification_service.logger).to receive(:fatal).with(error_message, error: anything)

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Pending issues',
            status: :failed,
            release_type: :patch
          )

        expect { notification_service.execute }.to raise_error(Gitlab::Error::Error)
      end
    end
  end
end
