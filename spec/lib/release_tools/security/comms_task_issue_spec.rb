# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::CommsTaskIssue do
  subject(:appsec_issue) { described_class.new }

  let(:versions) do
    [
      ReleaseTools::Version.new('12.7.4'),
      ReleaseTools::Version.new('12.6.6'),
      ReleaseTools::Version.new('12.5.9')
    ]
  end

  let(:coordinator) { instance_double(ReleaseTools::PatchRelease::Coordinator, versions: versions) }
  let(:tracking_issue) { build(:issue, due_date: '2024-04-01') }

  before do
    allow(ReleaseTools::PatchRelease::Coordinator)
      .to receive(:new)
      .and_return(coordinator)

    allow(ReleaseTools::GitlabClient)
      .to receive(:next_security_tracking_issue)
      .and_return(tracking_issue)
  end

  it_behaves_like 'issuable #initialize'

  describe '#title' do
    it 'returns the title' do
      expect(appsec_issue.title).to eq('Patch release: 12.7.4, 12.6.6, 12.5.9')
    end
  end

  describe '#confidential?' do
    it 'returns true' do
      expect(appsec_issue).to be_confidential
    end
  end

  describe '#labels' do
    it 'returns the appropriate labels' do
      expect(appsec_issue.labels).to be_present
    end
  end

  describe '#due_date' do
    it 'returns the same date as the tracking issue' do
      expect(appsec_issue.due_date).to eq('2024-04-01')
    end
  end

  describe '#description' do
    it 'generates the description' do
      expect(appsec_issue.description).to be_present
    end
  end

  describe '#versions_title' do
    it 'returns the versions' do
      expect(appsec_issue.versions_title).to eq('12.7.4, 12.6.6, 12.5.9')
    end
  end
end
