# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::ReleasePreparation::GenerateBlogPost do
  subject(:execute) { described_class.new.execute }

  let(:blog_post) { instance_spy(ReleaseTools::PatchRelease::BlogMergeRequest) }
  let(:coordinator) { stub_const('ReleaseTools::PatchRelease::Coordinator', spy) }
  let(:blog_post_mr) { build(:merge_request) }
  let(:notifier) { instance_spy(ReleaseTools::Slack::ReleaseJobEndNotifier) }
  let(:appsec_notifier) { instance_spy(ReleaseTools::Slack::Security::AppSecNotifier) }
  let(:tracking_issue) { create(:issue) }

  let(:issue_crawler) do
    instance_spy(
      ReleaseTools::Security::IssueCrawler,
      related_security_issues: [create(:issue)]
    )
  end

  let(:versions) do
    [
      ReleaseTools::Version.new('16.1.1'),
      ReleaseTools::Version.new('16.0.1'),
      ReleaseTools::Version.new('15.11.1')
    ]
  end

  before do
    allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(issue_crawler)

    allow(ReleaseTools::PatchRelease::BlogMergeRequest)
      .to receive(:new)
      .and_return(blog_post)

    allow(ReleaseTools::GitlabClient)
      .to receive(:next_security_tracking_issue)
      .and_return(tracking_issue)

    allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .and_return(notifier)

    allow(ReleaseTools::Slack::Security::AppSecNotifier)
      .to receive(:new)
      .and_return(appsec_notifier, tracking_issue)

    allow(blog_post)
      .to receive_messages(create: nil, url: blog_post_mr.web_url, exists?: false)
  end

  it 'creates blog post' do
    expect(coordinator).to receive(:new)
    expect(blog_post).to receive(:create)

    expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .with(
        job_type: 'Generate blog post',
        status: :success,
        release_type: :patch,
        extra_string: "Blog MR: #{blog_post_mr.web_url}"
      )

    expect(ReleaseTools::Slack::Security::AppSecNotifier)
      .to receive(:new)
      .with(issuable: blog_post, issue_type: 'patch_blog_post')

    expect(ReleaseTools::GitlabClient)
      .to receive(:create_merge_request_comment)

    expect(notifier).to receive(:send_notification)

    without_dry_run { execute }
  end

  context 'when the blog post exists' do
    it 'posts a comment on the security merge request' do
      expect(coordinator).to receive(:new)

      security_blog_post = create(:merge_request)

      allow(ReleaseTools::GitlabClient)
        .to receive(:security_blog_merge_request)
        .and_return(security_blog_post)

      allow(blog_post)
        .to receive_messages(exists?: true, generate_blog_content: 'foo')

      expect(ReleaseTools::GitlabClient)
        .to receive(:create_merge_request_comment)
        .with(
          'gitlab-org/security/www-gitlab-com',
          security_blog_post.iid,
          a_string_matching(/foo/)
        )

      without_dry_run { execute }
    end
  end

  context 'with dry run' do
    it 'does not create MR' do
      expect(coordinator).to receive(:new)
      expect(blog_post).not_to receive(:create)
      expect(blog_post).to receive(:generate_blog_content)

      expect(ReleaseTools::Slack::ReleaseJobEndNotifier).not_to receive(:new)

      execute
    end
  end

  context 'when something goes wrong' do
    before do
      allow(blog_post)
        .to receive(:create)
        .and_raise(Gitlab::Error::Error)
    end

    it 'sends a slack notification and raises exception' do
      expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
        .to receive(:new)
        .with(
          job_type: 'Generate blog post',
          status: :failed,
          release_type: :patch
        )

      without_dry_run do
        expect { execute }.to raise_error(Gitlab::Error::Error)
      end
    end
  end
end
