# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::SecurityReleaseTrackingIssueNotifier do
  let(:client) { ReleaseTools::GitlabClient }
  let(:issue) { create(:security_implementation_issue, project_id: ReleaseTools::Project::Gitaly::IDS[:security]) }
  let(:tracking_issue) { create(:issue) }
  let(:task_issue) { create(:issue) }
  let(:notifier) { described_class.new(issue) }

  before do
    allow(client).to receive_messages(next_security_tracking_issue: tracking_issue, current_security_task_issue: task_issue)
    allow(issue).to receive(:pending_reasons).and_return(%w(a b))
  end

  describe '.notify' do
    it 'creates a new instance and calls execute' do
      issue = build(:issue)
      notifier = instance_spy(described_class)

      expect(described_class).to receive(:new).with(issue).and_return(notifier)
      expect(notifier).to receive(:execute).and_return(true)

      described_class.notify(issue)
    end
  end

  describe '#execute' do
    it 'does not post a comment in a dry run' do
      expect(client).not_to receive(:create_issue_note)

      notifier.execute
    end

    it 'posts a comment pinging release managers' do
      expect(client).to receive(:create_issue_note).with(
        tracking_issue.project_id,
        issue: tracking_issue,
        body: /@gitlab-org\/release\/managers, a managed versioning project issue/
      ).and_return(true)

      without_dry_run do
        notifier.execute
      end
    end

    context 'with assignees' do
      it 'pings the assignees in the comment' do
        allow(issue).to receive(:assignees)
          .and_return([double(:user, username: 'foo'), double(:user, username: 'bar')])

        expect(client).to receive(:create_issue_note).with(
          tracking_issue.project_id,
          issue: tracking_issue,
          body: /@foo, @bar, a managed versioning project issue/
        ).and_return(true)

        without_dry_run do
          notifier.execute
        end
      end
    end

    context 'non managed versioning project' do
      let(:issue) { create(:security_implementation_issue, project_id: 1) }

      it 'does nothing if the project is not a managed versioning project' do
        expect(client).not_to receive(:create_issue_note)

        without_dry_run do
          notifier.execute
        end
      end
    end
  end
end
