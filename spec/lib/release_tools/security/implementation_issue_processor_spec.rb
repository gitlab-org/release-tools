# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::ImplementationIssueProcessor do
  subject(:processor) { described_class.new(implementation_issue) }

  let(:implementation_issue) do
    ReleaseTools::Security::ImplementationIssue.new(build(:issue), [])
  end

  let(:validator) { instance_spy(ReleaseTools::Security::ImplementationIssueValidator) }
  let(:client) { stub_const('ReleaseTools::Security::Client', spy) }

  describe '#execute' do
    before do
      allow(ReleaseTools::Security::ImplementationIssueValidator)
        .to receive(:new)
        .and_return(validator)

      allow(validator)
        .to receive(:validate)
    end

    context 'with valid implementation issues' do
      before do
        allow(validator)
          .to receive(:errors)
          .and_return([])
      end

      it 'does not notify AppSec' do
        expect(client).not_to receive(:create_issue_note)

        processor.execute
      end
    end

    context 'with invalid implementation issues' do
      let(:schedule) do
        instance_spy(
          ReleaseTools::ReleaseManagers::Schedule,
          active_appsec_release_managers: [
            build(:user, id: 3, username: 'foo'),
            build(:user, id: 4, username: 'bar')
          ]
        )
      end

      before do
        allow(validator)
          .to receive(:errors)
          .and_return([:foo])

        allow(ReleaseTools::ReleaseManagers::Schedule)
          .to receive(:new)
          .and_return(schedule)
      end

      it 'posts a message on the issue' do
        expect(client)
          .to receive(:create_issue_note)
          .with(
            implementation_issue.project_id,
            implementation_issue.iid,
            a_string_including('This security implementation issue does not meet the requirement for automation')
          )

        without_dry_run do
          processor.execute
        end
      end

      context 'with a dry-run' do
        it 'does nothing' do
          expect(client).not_to receive(:create_issue_note)

          processor.execute
        end
      end
    end
  end
end
