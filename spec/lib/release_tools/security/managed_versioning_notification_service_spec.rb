# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::ManagedVersioningNotificationService do
  subject(:notification_service) { described_class.new }

  let(:client) { ReleaseTools::GitlabClient }
  let(:managed_versioning_issue) { instance_double(ReleaseTools::Security::ImplementationIssue, project_id: ReleaseTools::Project::Gitaly::IDS[:security], web_url: 'gitaly.url') }
  let(:non_managed_versioning_issue) { instance_double(ReleaseTools::Security::ImplementationIssue, project_id: 1, web_url: 'gitlab.url') }
  let(:related_issues) { [managed_versioning_issue, non_managed_versioning_issue] }
  let(:crawler) { instance_double(ReleaseTools::Security::IssueCrawler, related_security_issues: related_issues) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:tracking_issue) { instance_double(ReleaseTools::Security::ImplementationIssue, project_id: 1, web_url: 'tracking.url') }
  let(:task_issue) { build(:issue, project_id: 1) }

  before do
    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(crawler)

    allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .and_return(notifier)

    allow(client).to receive_messages(next_security_tracking_issue: tracking_issue, current_security_task_issue: task_issue)
  end

  describe '#execute' do
    context 'when there are issues for projects under managed versioning' do
      it 'outputs logs and creates an issue note with the list of issues' do
        summary_log_message = "Notifying RMs that issues for projects under managed versioning were linked to patch release tracking issue"
        note_message = /@gitlab-org\/release\/managers, there are managed versioning project issues linked/
        issues_log_message = "\n\nIssues for projects that are under managed versioning:\n\n- gitaly.url"

        expect(notification_service.logger).to receive(:info).with(summary_log_message, security_tracking_issue: "tracking.url")

        expect(client).to receive(:create_issue_note).with(
          tracking_issue.project_id,
          issue: tracking_issue,
          body: note_message
        ).and_return(true)

        expect(notification_service.logger).to receive(:info).with(issues_log_message)

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Managed versioning check',
            status: :success,
            release_type: :patch
          )

        without_dry_run do
          notification_service.execute
        end
      end

      context 'when running in dry_run mode' do
        it 'does not create an issue note' do
          expect(client).not_to receive(:create_issue_note)

          notification_service.execute
        end
      end
    end

    context 'when there are no issues for projects under managed versioning' do
      let(:related_issues) { [non_managed_versioning_issue] }

      it 'does not create an issue note' do
        expect(client).not_to receive(:create_issue_note)

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Managed versioning check',
            status: :success,
            release_type: :patch
          )

        without_dry_run do
          notification_service.execute
        end
      end
    end

    context 'when the job is unable to complete and an error is raised' do
      before do
        allow(client)
          .to receive(:next_security_tracking_issue)
          .and_raise(Gitlab::Error::Error)
      end

      it 'outputs manual instructions and notifies slack' do
        logger_output = /Managed versioning check failed. If this job continues to fail, manually check/

        expect(notification_service.logger).to receive(:fatal).with(logger_output, error: anything)

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Managed versioning check',
            status: :failed,
            release_type: :patch
          )

        without_dry_run do
          expect { notification_service.execute }.to raise_error(Gitlab::Error::Error)
        end
      end
    end
  end
end
