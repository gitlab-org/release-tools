# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::Checks::OrchestratorStatus do
  let(:orchestrator_status) { described_class.new }
  let(:consul_client) { instance_double(ReleaseTools::Consul::Client) }

  before do
    allow(ReleaseTools::Consul::Client).to receive(:new).and_return(consul_client)
  end

  describe '#fine?' do
    context 'when check_orchestrator_status feature is enabled' do
      let(:consul_client) { ReleaseTools::Consul::Client.new }

      before do
        enable_feature(:check_orchestrator_status)
        allow(ReleaseTools::Consul::Client).to receive(:new).and_return(consul_client)
      end

      it 'returns true when orchestrator is enabled' do
        allow(consul_client).to receive(:get).with(described_class::ORCHESTRATOR_KEY).and_return('{"status": "enabled"}')
        expect(orchestrator_status.fine?).to be true
      end

      it 'returns false when orchestrator is disabled' do
        allow(consul_client).to receive(:get).with(described_class::ORCHESTRATOR_KEY).and_return('{"status": "disabled"}')
        expect(orchestrator_status.fine?).to be false
      end

      it 'handles JSON parsing errors' do
        allow(consul_client).to receive(:get).with(described_class::ORCHESTRATOR_KEY).and_return('invalid json')
        expect(orchestrator_status.logger).to receive(:fatal).with('Failed checking Orchestrator status', instance_of(JSON::ParserError))
        expect(orchestrator_status.fine?).to be false
      end
    end

    context 'when check_orchestrator_status feature is disabled' do
      before do
        disable_feature(:check_orchestrator_status)
      end

      it 'returns true' do
        expect(orchestrator_status.fine?).to be true
      end
    end
  end

  describe '#to_slack_blocks' do
    context 'when orchestrator is enabled' do
      before do
        enable_feature(:check_orchestrator_status)
        allow(orchestrator_status).to receive(:fine?).and_return(true)
      end

      it 'returns the correct slack block structure for enabled status' do
        expected_block = [{ type: 'section', text: { type: 'mrkdwn', text: ':white_check_mark: Orchestrator is enabled' } }]
        expect(orchestrator_status.to_slack_blocks).to eq(expected_block)
      end
    end

    context 'when orchestrator is disabled' do
      before do
        enable_feature(:check_orchestrator_status)
        allow(orchestrator_status).to receive(:fine?).and_return(false)
      end

      it 'returns the correct slack block structure for disabled status' do
        expected_block = [{ type: 'section', text: { type: 'mrkdwn', text: ':red_circle: Orchestrator is disabled' } }]
        expect(orchestrator_status.to_slack_blocks).to eq(expected_block)
      end
    end
  end

  describe '#to_issue_body' do
    context 'when orchestrator is enabled' do
      before do
        enable_feature(:check_orchestrator_status)
        allow(orchestrator_status).to receive(:fine?).and_return(true)
      end

      it 'returns the correct message for enabled status' do
        expect(orchestrator_status.to_issue_body).to eq(':white_check_mark: Orchestrator is enabled')
      end
    end

    context 'when orchestrator is disabled' do
      before do
        enable_feature(:check_orchestrator_status)
        allow(orchestrator_status).to receive(:fine?).and_return(false)
      end

      it 'returns the correct message for disabled status' do
        expect(orchestrator_status.to_issue_body).to eq(':red_circle: Orchestrator is disabled')
      end
    end
  end

  describe '#name' do
    it 'returns the correct name' do
      expect(orchestrator_status.name).to eq('orchestrator status')
    end
  end
end
