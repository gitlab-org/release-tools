# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Qa::ProjectChangeset, vcr: { cassette_name: 'commits-api' } do
  let(:project) { ReleaseTools::Project::GitlabEe }
  let(:from_ref) { 'v16.2.6-ee' }
  let(:to_ref) { 'v16.2.7-ee' }

  let(:expected_shas) do
    %w[
      a3b32933d7767e26bf83ff16f78f444c66f5958f
      4b8342d6a0902bf051903db200095d8026bc0eaa
      336d6829bf5268dbbb1ccdaa224ed65c431a9ed6
      603e63f48c0205ade269cc79729001729a5203e7
      b762d7fa87db96f158f2e7c226b46ddfacaeb636
      03fe067a17421bdfb42236b32a5b58705ce2a37d
      e220da8eb242957d041d58f279ebbf21665915b1
    ]
  end

  subject { described_class.new(project: project, from: from_ref, to: to_ref) }

  describe 'validations' do
    let(:from_ref) { 'invalid' }

    it 'raises an argument error for an invalid ref' do
      expect do
        subject
      end.to raise_error(ArgumentError)
    end
  end

  describe '#shas', vcr: { cassette_name: 'repository-api-compare' } do
    it 'has the correct shas' do
      expect(subject.shas).to eq(expected_shas)
    end
  end

  describe '#commits', vcr: { cassette_name: 'repository-api-compare' } do
    let(:shas) { subject.commits.pluck("id") }

    it 'downloads the list of commits from the API' do
      expect(shas).to eq(expected_shas)
    end
  end

  describe '#merge_requests', vcr: { cassette_name: %w[repository-api-compare merge-requests-api] } do
    let(:mr_ids) { subject.merge_requests.pluck("id") }

    it 'downloads the list of Merge Requests' do
      expect(mr_ids).to eq([248_439_676, 251_337_722])
    end

    it 'retries retrieving of a merge request when this times out' do
      raised = false

      allow(ReleaseTools::GitlabClient).to receive(:merge_request) do
        if raised
          raised = true
          raise Errno::ETIMEDOUT
        else
          double(:merge_request)
        end
      end

      expect(subject.merge_requests.size).to be(2)
    end
  end
end
