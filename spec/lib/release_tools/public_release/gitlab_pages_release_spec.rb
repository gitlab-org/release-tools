# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::GitlabPagesRelease do
  include MetadataHelper

  let(:version) { ReleaseTools::Version.new('42.0.0') }
  let(:client) { class_spy(ReleaseTools::GitlabClient) }

  subject(:release) { described_class.new(version, client: client) }

  describe '#execute' do
    it 'runs the release' do
      tag = double('tag')

      expect(release).to receive(:create_target_branch)
      expect(release).to receive(:compile_changelog)
      expect(release).to receive(:update_versions)
      expect(release).to receive(:create_tag).and_return(tag)
      expect(release).to receive(:add_release_metadata).with(tag)
      expect(release).to receive(:notify_slack).with(ReleaseTools::Project::GitlabPages, version)

      without_dry_run { release.execute }
    end
  end

  describe '#compile_changelog' do
    it 'creates a changelog' do
      compiler = instance_spy(ReleaseTools::ChangelogCompiler)

      expect(ReleaseTools::ChangelogCompiler)
        .to receive(:new)
        .with(release.project_path, { client: client })
        .and_return(compiler)

      expect(compiler)
        .to receive(:compile)
        .with(version, { branch: '42-0-stable' })

      release.compile_changelog
    end

    context 'for a release candidate version' do
      let(:version) { ReleaseTools::Version.new('42.0.0-rc42-ee') }

      it 'skips adding a changelog' do
        expect(ReleaseTools::ChangelogCompiler).not_to receive(:new)

        release.compile_changelog
      end
    end
  end

  describe '#update_versions' do
    let(:expected_updates) do
      {
        'VERSION' => '42.0.0'
      }
    end

    it 'updates the version files' do
      expect(release)
        .to receive(:commit_version_files)
        .with('42-0-stable', expected_updates)

      release.update_versions
    end
  end

  describe '#create_tag' do
    it 'creates the Git tag' do
      expect(release.client)
        .to receive(:find_or_create_tag)
        .with(
          release.project_path,
          'v42.0.0',
          '42-0-stable',
          { message: 'Version v42.0.0' }
        )

      release.create_tag
    end
  end

  describe '#add_release_metadata' do
    it 'adds the release metadata' do
      tag =
        double(:tag, name: 'v42.0.0', commit: double(:commit, id: '123'))

      expect(release.release_metadata)
        .to receive(:add_release)
        .with(
          {
            name: release.send(:project).project_name,
            version: '42.0.0',
            sha: '123',
            ref: 'v42.0.0',
            tag: true
          }
        )

      release.add_release_metadata(tag)
    end
  end

  describe '#source_for_target_branch' do
    context 'with a specific commit' do
      it 'returns the commit' do
        release = described_class.new(version, commit: 'foo')

        expect(release.source_for_target_branch).to eq('foo')
      end
    end

    it 'delegates to last_production_commit_metadata' do
      sha = 'abc123'
      expect(release).to receive(:last_production_commit_metadata).and_return(sha)
      expect(release).not_to receive(:last_production_commit_deployments)

      expect(release.source_for_target_branch).to eq(sha)
    end
  end

  describe '#create_target_branch' do
    it_behaves_like 'public_release#create_target_branch'
  end
end
