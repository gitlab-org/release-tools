# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::KasRelease do
  describe '#initialize' do
    it 'converts EE versions to CE versions' do
      version = ReleaseTools::Version.new('42.0.0-rc42-ee')
      release = described_class.new(version)

      expect(release.version).to eq('42.0.0-rc42')
    end
  end

  describe '#execute' do
    subject(:release) { described_class.new(version) }

    let(:version) { ReleaseTools::Version.new('42.2.1') }
    let(:tag) { create(:tag, name: 'v42.2.1', commit: build(:commit, id: 'commit-id')) }
    let(:changelog_compiler) { instance_double(ReleaseTools::ChangelogCompiler) }

    before do
      enable_feature(:release_the_kas)

      allow(ReleaseTools::GitlabClient).to receive(:find_or_create_tag).and_return(tag)

      allow(changelog_compiler).to receive(:compile)
      allow(ReleaseTools::ChangelogCompiler).to receive(:new).and_return(changelog_compiler)

      allow(release).to receive(:create_target_branch)
      allow(release).to receive(:commit_version_files)
      allow(release).to receive(:notify_slack)
      allow(release.release_metadata).to receive(:add_release)
    end

    it 'runs the release' do
      expect(release).to receive(:create_target_branch)

      expect(ReleaseTools::GitlabClient).to receive(:find_or_create_tag).with(
        ReleaseTools::Project::Kas.canonical_or_security_path,
        'v42.2.1',
        release.target_branch,
        message: "Version v42.2.1"
      )

      expect(release.release_metadata).to receive(:add_release).with(
        name: 'gitlab_kas',
        version: version.to_normalized_version,
        sha: 'commit-id',
        ref: 'v42.2.1',
        tag: true
      )

      expect(release).to receive(:notify_slack).with(ReleaseTools::Project::Kas, version)

      without_dry_run do
        release.execute
      end
    end

    it 'updates the version for the target (stable) branch' do
      expect(release).to receive(:commit_version_files).with(
        release.target_branch,
        { 'VERSION' => version.to_s }
      )

      without_dry_run do
        release.execute
      end
    end

    it 'runs the changelog compiler' do
      expect(ReleaseTools::ChangelogCompiler).to receive(:new).with(
        ReleaseTools::Project::Kas.canonical_or_security_path,
        client: ReleaseTools::GitlabClient
      )
      expect(changelog_compiler).to receive(:compile).with(
        version, branch: release.target_branch, skip_ci: false
      )

      without_dry_run do
        release.execute
      end
    end

    context 'when the version is a monthly release' do
      let(:version) { ReleaseTools::Version.new('17.11.0') }

      before do
        allow(ReleaseTools::GitlabClient)
          .to receive(:group_milestones)
                .with('gitlab-org', hash_including(title: '17.12'))
                .and_return([])

        allow(ReleaseTools::GitlabClient)
          .to receive(:group_milestones)
                .with('gitlab-org', hash_including(title: '18.0'))
                .and_return([Gitlab::ObjectifiedHash.new(title: '18.0')])
      end

      it 'updates the version for the target (stable) branch' do
        expect(release).to receive(:commit_version_files).with(
          release.target_branch,
          { 'VERSION' => '17.11.0' }
        )

        without_dry_run do
          release.execute
        end
      end

      context 'when the tag_kas_after_monthly_release feature is enabled' do
        before do
          enable_feature(:release_the_kas, :tag_kas_after_monthly_release)
        end

        it 'updates the version for the default branch' do
          expect(release).to receive(:commit_version_files).with(
            ReleaseTools::Project::Kas.default_branch,
            { 'VERSION' => '18.0.0-rc1' }
          )

          without_dry_run do
            release.execute
          end
        end

        it 'creates a tag for the default branch' do
          expect(ReleaseTools::GitlabClient).to receive(:find_or_create_tag).with(
            ReleaseTools::Project::Kas.canonical_or_security_path,
            'v18.0.0-rc1',
            ReleaseTools::Project::Kas.default_branch,
            message: "Version v18.0.0-rc1"
          )

          without_dry_run do
            release.execute
          end
        end
      end

      context 'when the tag_kas_after_monthly_release feature is disabled' do
        before do
          disable_feature(:tag_kas_after_monthly_release)
        end

        it 'does not update the version file and create a tag for the default branch' do
          expect(release).not_to receive(:update_version_for_default_branch)
          expect(release).not_to receive(:create_tag_for_default_branch)

          without_dry_run do
            release.execute
          end
        end
      end
    end

    context 'when the version is a release candidate' do
      let(:version) { ReleaseTools::Version.new('42.2.1-rc1') }

      it 'updates the version' do
        expect(release).to receive(:commit_version_files)

        without_dry_run do
          release.execute
        end
      end

      it 'does not run the changelog compiler' do
        expect(ReleaseTools::ChangelogCompiler).not_to receive(:new)
        expect(changelog_compiler).not_to receive(:compile)

        without_dry_run do
          release.execute
        end
      end
    end

    context 'if dry run is enabled' do
      it 'creates a target branch' do
        expect(release).to receive(:create_target_branch)

        release.execute
      end

      it 'skips most tasks' do
        expect(ReleaseTools::ChangelogCompiler).not_to receive(:new)
        expect(changelog_compiler).not_to receive(:compile)

        expect(release).not_to receive(:commit_version_files)
        expect(release).not_to receive(:create_tag)
        expect(release).not_to receive(:add_release_metadata)
        expect(release).not_to receive(:notify_slack)

        release.execute
      end
    end

    context 'when `release_the_kas` feature is disabled' do
      before do
        disable_feature(:release_the_kas)
      end

      it 'outputs a log that the feature is disabled' do
        expect(release.logger).to receive(:info).with(':release_the_kas feature is disabled')

        release.execute
      end

      it 'does not execute any of the kas release steps' do
        expect(release).not_to receive(:create_target_branch)
        expect(release).not_to receive(:update_versions)
        expect(release).not_to receive(:compile_changelog)
        expect(release).not_to receive(:create_tag)
        expect(release).not_to receive(:add_release_metadata)
        expect(release).not_to receive(:notify_slack)

        release.execute
      end
    end
  end
end
