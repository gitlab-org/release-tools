# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::VersionClient do
  subject(:version_client) { described_class }

  describe '.versions' do
    it 'returns an Array of version entries', vcr: { cassette_name: 'versions/list' } do
      versions = version_client.versions

      expect(versions.length).to eq(150)
      expect(versions.first.version).to eq('17.2.0')
    end
  end

  describe '.create_version' do
    it 'creates new version', vcr: { cassette_name: 'versions/create' } do
      version = version_client.create_version('42.0.0')

      expect(version.version).to eq('42.0.0')
    end

    context 'with patch releases', vcr: { cassette_name: 'versions/create_patch' } do
      # There is no difference in the response between creating a patch version and a non-patch version. The only difference
      # is that the previous version (e.g. 42.0.0 and 42.0.1) is marked as vulnerable. However, it is done by the server
      # side, so we don't test it from the client side.
      it 'create regular patch version' do
        version = version_client.create_version('42.0.1', version_type: :patch_regular)

        expect(version.version).to eq('42.0.1')

        expect(WebMock).to have_requested(:post, "https://version.gitlab.com/api/v1/versions")
          .with(
            body: {
              version: '42.0.1',
              security_release_type: 'non_critical'
            }
          )
      end

      it 'create critical patch version' do
        version = version_client.create_version('42.0.1', version_type: :patch_critical)

        expect(version.version).to eq('42.0.1')

        expect(WebMock).to have_requested(:post, "https://version.gitlab.com/api/v1/versions")
          .with(
            body: {
              version: '42.0.1',
              security_release_type: 'critical'
            }
          )
      end
    end

    context 'when an invalid version_type is provided' do
      it 'does not send any request' do
        expect { version_client.create_version('42.0.1', version_type: :something) }.to raise_error(ArgumentError)
      end
    end
  end

  describe '.get_version' do
    it 'gets single version', vcr: { cassette_name: 'versions/get' } do
      version = version_client.get_version('16.11.0')

      expect(version.version).to eq('16.11.0')
    end
  end
end
