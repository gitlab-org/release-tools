# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::DeploymentPipeline::Version do
  subject { object }

  let(:object) { described_class.new(product_version) }
  let(:product_version) { build(:product_version, version: version) }

  describe '#valid_auto_deploy_version?' do
    subject(:valid_auto_deploy_version?) { object.valid_auto_deploy_version? }

    where(:version, :is_auto_deploy) do
      [
        ['17.6.202411052200', true],
        ['17.2.0', false],
        ['invalid', false]
      ]
    end

    with_them do
      it 'checks if version is auto deploy' do
        expect(valid_auto_deploy_version?).to eq(is_auto_deploy)
      end
    end
  end

  describe '#suitable?' do
    subject(:suitable?) { object.suitable? }

    let(:version) { '17.6.202411052200' }

    before do
      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Latest)
        .to receive(:version)
        .and_return(ReleaseTools::ProductVersion.new('17.6.202410052200'))

      allow(ReleaseTools::GitlabDevClient)
        .to receive(:pipelines)
        .and_return([build(:pipeline, :success)])
    end

    it 'calls valid_auto_deploy_version?' do
      expect(object).to receive(:valid_auto_deploy_version?).and_call_original

      suitable?
    end

    it 'calls newer_than_latest_deployment?' do
      expect(object).to receive(:newer_than_latest_deployment?).and_call_original

      suitable?
    end

    context 'with packager pipelines in different statuses' do
      where(:status, :is_suitable) do
        [
          [:failed, false],
          [:running, false],
          [:success, true]
        ]
      end

      with_them do
        before do
          allow(ReleaseTools::GitlabDevClient)
            .to receive(:pipelines)
            .and_return([build(:pipeline, status)])
        end

        it 'checks if packager pipelines have completed' do
          expect(suitable?).to eq(is_suitable)
        end
      end
    end

    context 'with skip_checks' do
      let(:object) { described_class.new(product_version, skip_checks: true) }

      it 'only checks valid_auto_deploy_version?' do
        expect(object)
          .to receive(:valid_auto_deploy_version?)
          .and_call_original

        expect(object).not_to receive(:newer_than_latest_deployment?)

        expect(object).not_to receive(:packager_pipelines_complete?)

        expect(suitable?).to be_truthy
      end
    end
  end

  describe '#newer_than_latest_deployment?' do
    before do
      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Latest)
        .to receive(:version)
        .and_return(ReleaseTools::ProductVersion.new(latest_version))
    end

    context 'when version is older than previous deployment pipeline version' do
      let(:latest_version) { '17.6.202411062200' }
      let(:version) { '17.6.202411052200' }

      it { is_expected.not_to be_newer_than_latest_deployment }
    end

    context 'when version is newer than previous deployment pipeline versions' do
      let(:latest_version) { '17.6.202411042200' }
      let(:version) { '17.6.202411072200' }

      it { is_expected.to be_newer_than_latest_deployment }
    end
  end
end
