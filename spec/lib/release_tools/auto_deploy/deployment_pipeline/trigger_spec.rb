# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger do
  describe 'execute' do
    subject(:execute) { described_class.new(product_version).execute }

    let(:product_version) { ReleaseTools::ProductVersion.new('17.6.202411052200') }

    before do
      allow(ReleaseTools::GitlabOpsClient).to receive(:create_pipeline).and_return(build(:pipeline))
    end

    it 'creates pipeline' do
      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:create_pipeline)
        .with(ReleaseTools::Project::ReleaseTools, { AUTO_DEPLOY_TAG: '17.6.202411052200' })

      without_dry_run { execute }
    end

    context 'in dry run mode' do
      it 'does not create pipeline' do
        expect(ReleaseTools::GitlabOpsClient).not_to receive(:create_pipeline)

        execute
      end
    end
  end
end
