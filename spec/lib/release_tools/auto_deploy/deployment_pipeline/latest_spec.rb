# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::DeploymentPipeline::Latest do
  let(:deployment_pipeline) { build(:pipeline, name: "Deployment pipeline - #{deployment_pipeline_version}") }
  let(:invalid_deployment_pipeline) { build(:pipeline, name: "Deployment pipeline - 42.0") }
  let(:coordinator_pipeline) { build(:pipeline, name: 'Coordinator pipeline', ref: coordinator_version) }

  before do
    allow(ReleaseTools::GitlabOpsClient)
      .to receive(:pipelines)
      .with(ReleaseTools::Project::ReleaseTools, { name: 'Coordinator pipeline', per_page: 1 })
      .and_return(Gitlab::PaginatedResponse.new([coordinator_pipeline]))

    allow(ReleaseTools::GitlabOpsClient)
      .to receive(:pipelines)
      .with(
        ReleaseTools::Project::ReleaseTools,
        {
          ref: ReleaseTools::Project::ReleaseTools.default_branch,
          author: "gitlab-release-tools-bot",
          source: 'api'
        }
      )
      .and_return(Gitlab::PaginatedResponse.new([build(:pipeline), invalid_deployment_pipeline, deployment_pipeline]))
  end

  after do
    # Remove the memoization variable otherwise it leaks state between tests
    described_class.remove_instance_variable(:@pipeline)
  end

  describe '.pipeline' do
    subject(:pipeline) { described_class.pipeline }

    context 'when coordinator version is newer' do
      let(:coordinator_version) { '17.6.202411062200' }
      let(:deployment_pipeline_version) { '17.6.202411042200' }

      it { is_expected.to eq(coordinator_pipeline) }
    end

    context 'when decoupled deployment version is newer' do
      let(:deployment_pipeline_version) { '17.6.202411062200' }
      let(:coordinator_version) { '17.6.202411042200' }

      it { is_expected.to eq(deployment_pipeline) }
    end
  end

  describe '.version' do
    subject(:pipeline) { described_class.version }

    context 'when coordinator version is newer' do
      let(:coordinator_version) { '17.6.202411062200' }
      let(:deployment_pipeline_version) { '17.6.202411042200' }

      it { is_expected.to eq(ReleaseTools::ProductVersion.new(coordinator_version)) }
    end

    context 'when decoupled deployment version is newer' do
      let(:deployment_pipeline_version) { '17.6.202411062200' }
      let(:coordinator_version) { '17.6.202411042200' }

      it { is_expected.to eq(ReleaseTools::ProductVersion.new(deployment_pipeline_version)) }
    end
  end
end
