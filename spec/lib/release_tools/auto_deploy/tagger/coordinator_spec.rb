# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::Tagger::Coordinator do
  let(:fake_client) { spy(project_path: described_class::PROJECT.path) }
  let(:version_string) { '17.7.202412040800' }

  subject(:tagger) { described_class.new(version_string) }

  before do
    stub_const('ReleaseTools::GitlabOpsClient', fake_client)
  end

  describe '#tag!' do
    it 'creates a tag in the coordinator project' do
      without_dry_run do
        tagger.tag!
      end

      expect(fake_client).to have_received(:create_tag).with(
        described_class::PROJECT,
        tagger.tag_name,
        described_class::PROJECT.default_branch,
        tagger.tag_message
      )
    end
  end

  describe '#tag_name' do
    it 'returns a tag name in the appropriate format' do
      expect(tagger.tag_name).to eq(version_string)
    end
  end

  describe '#tag_message' do
    it 'returns a formatted tag message' do
      expected = 'Created via https://example.com'

      ClimateControl.modify(CI_JOB_URL: 'https://example.com') do
        expect(tagger.tag_message).to eq(expected)
      end
    end
  end
end
