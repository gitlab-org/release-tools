# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Pre::Prepare do
  subject(:prepare) { described_class.new }

  let(:deployment) { build(:deployment, :success, sha: 'sha1') }
  let(:product_version) { build(:product_version, metadata_commit_id: 'sha1') }
  let(:client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }

  before do
    allow(ReleaseTools::ProductVersion)
      .to receive(:from_metadata_sha)
      .with('sha1')
      .and_return(product_version)
  end

  after do
    FileUtils.rm_r('build.env') if File.exist?('build.env')
  end

  describe '#execute' do
    it 'stores the last version of production' do
      allow(client)
        .to receive(:last_successful_deployment)
        .and_return(deployment)

      result = "DEPLOY_VERSION=#{product_version.auto_deploy_package}"

      prepare.execute

      expect(File.read('build.env')).to eq(result)
    end
  end
end
