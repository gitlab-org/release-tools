# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::CherryPick::AutoDeployService do
  let(:project) { ReleaseTools::Project::GitlabCe }
  let(:target) { 'branch-name' }
  let(:refs_containing_merge_commit) { Gitlab::PaginatedResponse.new([]) }

  subject do
    described_class.new(project, target)
  end

  describe '#execute' do
    before do
      enable_feature(:cherry_pick_security_merge_requests)
    end

    context 'with no pickable MRs' do
      it 'does nothing' do
        expect(subject).to receive(:canonical_pickable_mrs).and_return([])
        expect(subject).to receive(:security_pickable_mrs).and_return([])

        expect(subject.execute).to eq([])
      end
    end

    context 'with pickable MRs' do
      def stub_picking
        # If the `merge_commit_sha` contains `failure`, we raise an error to
        # simulate a failed pick; otherwise return a valid response
        allow(internal_client).to receive(:cherry_pick) do |_, keywords|
          if keywords[:ref].start_with?('failure')
            raise gitlab_error(:BadRequest, code: 400)
          else
            build(:gitlab_response, short_id: "newsha#{keywords[:ref]}")
          end
        end
      end

      let(:notifier) { spy }
      let(:internal_client) { spy }
      let(:metrics_client) { spy }
      let(:picks) do
        Gitlab::PaginatedResponse.new(
          [
            build(
              :merge_request,
              iid: 1,
              project_id: 13_083,
              merge_commit_sha: 'success-a',
              labels: %w[severity::1 bug],
              web_url: 'https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/1'
            ).as_null_object,

            build(
              :merge_request,
              iid: 2,
              project_id: 13_084,
              merge_commit_sha: 'success-b',
              labels: %w[severity::2],
              web_url: 'https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/2'
            ).as_null_object,

            build(
              :merge_request,
              iid: 3,
              project_id: 13_083,
              merge_commit_sha: 'success-c',
              labels: [],
              web_url: 'https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/3'
            ).as_null_object,

            build(
              :merge_request,
              iid: 4,
              project_id: 13_083,
              merge_commit_sha: 'failure-a',
              labels: %w[priority::3],
              web_url: 'https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/4'
            ).as_null_object,

            build(
              :merge_request,
              iid: 5,
              project_id: 13_083,
              merge_commit_sha: 'failure-b',
              labels: %w[priority::1 severity::1],
              web_url: 'https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/5'
            ).as_null_object,

            build(
              :merge_request,
              iid: 6,
              project_id: 13_083,
              merge_commit_sha: 'failure-c',
              labels: [],
              web_url: 'https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/6'
            ).as_null_object,
          ]
        )
      end

      let(:security_picks) do
        Gitlab::PaginatedResponse.new(
          [
            build(
              :merge_request,
              iid: 7,
              project_id: 13_085,
              merge_commit_sha: 'success-d',
              labels: %w[severity::1],
              web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/7'
            ).as_null_object
          ]
        )
      end

      before do
        allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(metrics_client)

        allow(subject).to receive_messages(
          notifier: notifier,
          client: internal_client,
          canonical_pickable_mrs: picks,
          security_pickable_mrs: security_picks
        )

        allow(internal_client).to receive(:commit_refs).and_return(refs_containing_merge_commit)
      end

      it 'attempts to cherry pick only severity::1 and severity::2 merge requests' do
        stub_picking

        # Ignore assert_mirrored!
        allow(internal_client).to receive(:commit).and_return(true)

        expect(internal_client).to receive(:cherry_pick)
          .with(project.auto_deploy_path, ref: 'success-a', target: target)

        expect(internal_client).to receive(:cherry_pick)
          .with(project.auto_deploy_path, ref: 'success-b', target: target)

        expect(internal_client).to receive(:cherry_pick)
          .with(project.auto_deploy_path, ref: 'failure-b', target: target)

        expect(internal_client).to receive(:cherry_pick)
          .with(project.security_path, ref: 'success-d', target: target)

        without_dry_run do
          results = subject.execute

          success = results.select(&:success?)
          expect(success.size).to be(3)
          expect(success[0].merge_request.iid).to eql(picks[0].iid)
          expect(success[1].merge_request.iid).to eql(picks[1].iid)
          expect(success[2].merge_request.iid).to eql(security_picks[0].iid)

          denied = results.select(&:denied?)
          expect(denied.size).to be(3)
          expect(denied.map(&:reason).uniq).to eq(['Missing ~severity::1 or ~severity::2 label'])
          expect(results.count(&:failure?)).to be(1)
        end
      end

      it 'posts a comment to each merge request' do
        stub_picking

        subject.execute

        expect(notifier).to have_received(:comment).exactly(7).times
      end

      it 'tracks metrics on delivery-metrics' do
        stub_picking

        allow(internal_client)
          .to receive(:commit_refs)
          .with(anything, picks[0].merge_commit_sha, { type: 'branch' })
          .and_return(
            Gitlab::PaginatedResponse.new([build(:commit_ref, name: target), build(:commit_ref)])
          )

        allow(internal_client)
          .to receive(:search_in_project)
          .and_return([])

        allow(internal_client)
          .to receive(:search_in_project)
          .with(anything, anything, a_string_including(picks[3].merge_commit_sha), anything)
          .and_return([build(:commit)])

        without_dry_run do
          subject.execute
        end

        # Expect 2 success, 1 completed, 1 not_required, 3 denied, 1 failure
        expect(metrics_client).to have_received(:inc)
          .with('auto_deploy_picks_total', labels: 'success').twice

        expect(metrics_client).to have_received(:inc)
          .with('auto_deploy_picks_total', labels: 'failed').exactly(4).times
      end

      context 'when merge_commit already exists on target auto deploy branch' do
        let(:refs_containing_merge_commit) do
          Gitlab::PaginatedResponse.new([build(:commit_ref, name: target), build(:commit_ref)])
        end

        let(:picks) do
          Gitlab::PaginatedResponse.new([
            build(
              :merge_request,
              iid: 1,
              project_id: 13_083,
              merge_commit_sha: 'success-a',
              labels: %w[severity::1 bug]
            ).as_null_object,

            build(
              :merge_request,
              iid: 2,
              project_id: 13_083,
              merge_commit_sha: 'success-b',
              labels: %w[severity::2]
            ).as_null_object
          ])
        end

        it 'does not attempt to cherry-pick' do
          expect(internal_client).not_to receive(:cherry_pick)

          results = without_dry_run { subject.execute }

          expect(notifier).to have_received(:comment).exactly(3).times
          expect(results.length).to eq(3)
          expect(results.count(&:completed?)).to eq(3)
        end

        it 'verifies merge commits exist on Security' do
          non_security_mr = build(
            :merge_request,
            iid: 1,
            project_id: 13_083,
            merge_commit_sha: 'success-a',
            labels: %w[severity::1 bug],
            web_url: 'https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/1'
          ).as_null_object

          allow(subject).to receive(:canonical_pickable_mrs)
            .and_return(Gitlab::PaginatedResponse.new([non_security_mr]))

          allow(internal_client).to receive(:commit)
            .and_raise(gitlab_error(:NotFound, code: 404))

          expect { subject.execute }
            .to raise_error(described_class::MirrorError)
        end
      end

      it 'skips security mirror check for security MRs' do
        allow(subject).to receive_messages(
          canonical_pickable_mrs: Gitlab::PaginatedResponse.new([]),
          security_pickable_mrs: security_picks
        )

        expect(internal_client).not_to receive(:commit)

        without_dry_run do
          subject.execute
        end
      end

      context 'when cherry-picked commit exists on branch' do
        let(:picks) do
          Gitlab::PaginatedResponse.new([
            build(
              :merge_request,
              iid: 1,
              project_id: 13_083,
              merge_commit_sha: 'success-a',
              labels: %w[severity::1 bug]
            ).as_null_object
          ])
        end

        before do
          allow(internal_client).to receive(:search_in_project).and_return([build(:commit)])
        end

        it 'does not attempt to cherry-pick' do
          expect(internal_client).not_to receive(:cherry_pick)
          expect(notifier).to receive(:comment).twice

          results = without_dry_run { subject.execute }

          expect(results.length).to eq(2)
          expect(results.all?(&:not_required?)).to be true
        end
      end

      context 'when cherry_pick_security_merge_requests is disabled' do
        before do
          disable_feature(:cherry_pick_security_merge_requests)
        end

        it 'does not include security merge requests' do
          stub_picking

          # Ignore assert_mirrored!
          allow(internal_client).to receive(:commit).and_return(true)

          expect(internal_client).to receive(:cherry_pick)
            .with(project.auto_deploy_path, ref: 'success-a', target: target)

          expect(internal_client).to receive(:cherry_pick)
            .with(project.auto_deploy_path, ref: 'success-b', target: target)

          expect(internal_client).to receive(:cherry_pick)
            .with(project.auto_deploy_path, ref: 'failure-b', target: target)

          expect(internal_client).not_to receive(:cherry_pick)
            .with(project.security_path, ref: 'success-d', target: target)

          without_dry_run do
            results = subject.execute

            success = results.select(&:success?)
            expect(success.size).to be(2)
            expect(success[0].merge_request.iid).to eql(picks[0].iid)
            expect(success[1].merge_request.iid).to eql(picks[1].iid)

            denied = results.select(&:denied?)
            expect(denied.size).to be(3)
            expect(denied.map(&:reason).uniq).to eq(['Missing ~severity::1 or ~severity::2 label'])
            expect(results.count(&:failure?)).to be(1)
          end
        end
      end
    end
  end
end
