# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Slack::Utilities do
  describe '.api_link' do
    it 'returns link in Slack API format' do
      result = described_class.api_link('hello world', 'https://gitlab.com/foo')

      expect(result).to eq('<https://gitlab.com/foo|hello world>')
    end
  end
end
