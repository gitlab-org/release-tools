# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::ReleaseEnvironment::DeployNotification do
  subject(:notifier) do
    described_class.new(
      pipeline: pipeline,
      environment_name: environment,
      release_environment_version: release_environment_version
    )
  end

  let(:pipeline) { create(:pipeline, :running) }
  let(:environment) { 'my_environment' }
  let(:release_environment_version) { '16-10-stable-9dce3c3d' }
  let(:status) { 'started' }
  let(:local_time) { Time.utc(2024, 9, 14, 15, 55, 0) }

  let(:context_elements) do
    [
      { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' }
    ]
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
  end

  describe '#execute' do
    context 'when the pipeline starts' do
      it 'sends a slack message' do
        block_content = ":ci_running: Release Environment *my_environment* <#{pipeline.web_url}|#{status}> `#{release_environment_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: 'Release environment my_environment: started 16-10-stable-9dce3c3d',
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end

    context 'when the pipeline succeeds' do
      let(:pipeline) { create(:pipeline, :success, created_at: '2024-09-14T15:15:23.923Z') }
      let(:status) { 'finished' }

      let(:context_elements) do
        [
          { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' },
          { type: 'mrkdwn', text: ":timer_clock: 39 minutes" }
        ]
      end

      it 'sends a slack message' do
        block_content = ":ci_passing: Release Environment *my_environment* <#{pipeline.web_url}|#{status}> `#{release_environment_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: 'Release environment my_environment: finished 16-10-stable-9dce3c3d',
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end

    context 'when the pipeline fails' do
      let(:pipeline) { create(:pipeline, :failed, created_at: '2024-09-14T15:15:23.923Z') }
      let(:status) { 'failed' }

      let(:context_elements) do
        [
          { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' },
          { type: 'mrkdwn', text: ":timer_clock: 39 minutes" }
        ]
      end

      it 'sends a slack message' do
        block_content = ":ci_failing: Release Environment *my_environment* <#{pipeline.web_url}|#{status}> `#{release_environment_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: 'Release environment my_environment: failed 16-10-stable-9dce3c3d',
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end
  end
end
