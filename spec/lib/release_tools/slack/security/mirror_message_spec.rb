# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::Security::MirrorMessage do
  subject(:mirror_message) do
    described_class.new(security_mirrors: [mirror_status])
  end

  let(:complete_status) { true }

  let(:environment_variables) do
    {
      'SLACK_TOKEN' => 'token',
      'CHAT_CHANNEL' => 'channel',
      'GITLAB_TOKEN' => 'token',
      'CI_JOB_URL' => 'https://example.com/foo/bar/-/jobs/1'
    }
  end

  let(:forked_from_project) do
    {
      'avatar_url' => 'avatar.png',
      'name' => 'GitLab',
      'path_with_namespace' => 'gitlab-org/gitlab'
    }
  end

  let(:job_message) do
    ':security-tanuki: :ci_passing: *Mirror check <https://example.com/foo/bar/-/jobs/1|successfully> executed*'
  end

  let(:mirror_status) do
    instance_double(
      ReleaseTools::Security::MirrorStatus,
      available?: true,
      canonical: forked_from_project,
      mirror_chain: 'Mirror chain',
      security_error: 'Security failed',
      build_error: 'Build failed',
      complete?: complete_status
    )
  end

  around do |ex|
    # These checks only apply to CI
    ClimateControl.modify(environment_variables) do
      ex.run
    end
  end

  describe '#general_status' do
    it 'posts an slack message' do
      expect(ReleaseTools::Slack::Message).to receive(:post).with(
        channel: 'channel', blocks: MirrorStatusBlockMatcher.new(mirror_status), message: job_message
      )

      mirror_message.general_status
    end

    context 'when CHAT_CHANNEL is not set' do
      let(:environment_variables) do
        {
          'SLACK_TOKEN' => 'token',
          'GITLAB_TOKEN' => 'token',
          'CI_JOB_URL' => 'https://example.com/foo/bar/-/jobs/1'
        }
      end

      it 'send the message to the default channel F_UPCOMING_RELEASE' do
        expect(ReleaseTools::Slack::Message).to receive(:post).with(
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE, blocks: MirrorStatusBlockMatcher.new(mirror_status), message: job_message
        )

        mirror_message.general_status
      end
    end
  end

  describe '#job_status' do
    let(:block_message) do
      [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: job_message
          }
        }
      ]
    end

    it 'posts an slack message' do
      expect(ReleaseTools::Slack::Message).to receive(:post).with(
        channel: 'channel', blocks: block_message, message: job_message
      )

      mirror_message.job_status
    end

    context 'when repositories are not synced' do
      let(:complete_status) { false }

      let(:job_message) do
        ':security-tanuki: :ci_failing: *Some projects are out of sync, review the Slack output or the <https://example.com/foo/bar/-/jobs/1|job log> for details*'
      end

      it 'posts an slack message' do
        expect(ReleaseTools::Slack::Message).to receive(:post).with(
          channel: 'channel', blocks: block_message, message: job_message
        )

        mirror_message.job_status
      end
    end
  end

  describe '#synced_repositories?' do
    it 'returns true if all repositories are complete' do
      expect(mirror_message.synced_repositories?).to be(true)
    end

    context 'with an unsynced repository' do
      let(:complete_status) { false }

      it 'returns false if not all repositories are complete' do
        expect(mirror_message.synced_repositories?).to be(false)
      end
    end
  end
end

# RSpec argument matcher for verifying the complex `block` Hash passed to
# `Slack::Message#send` from the described class
class MirrorStatusBlockMatcher
  def initialize(status)
    @status = status
  end

  def ===(other)
    project = @status.canonical
    json = other.to_json

    includes_security_error?(other) && includes_build_error?(other) &&
      json.include?(@status.mirror_chain) &&
      json.include?(project['avatar_url']) &&
      json.include?(project['name'])
  end

  private

  def includes_security_error?(other)
    other.one? do |block|
      block[:type] == 'section' &&
        block[:text][:text] == "*Security*:\n```#{@status.security_error}```"
    end
  end

  def includes_build_error?(other)
    other.one? do |block|
      block[:type] == 'section' &&
        block[:text][:text] == "*Build*:\n```#{@status.build_error}```"
    end
  end
end
