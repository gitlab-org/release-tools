# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::Security::AppSecNotifier do
  subject(:notifier) do
    described_class.new(issuable: issuable, issue_type: issue_type)
  end

  let(:tracking_issue) { build(:issue) }
  let(:local_time) { Time.utc(2023, 5, 14, 15, 55, 0) }

  let(:context_elements) do
    [
      { 'text' => ':clock1: 2023-05-14 15:55 UTC', 'type' => 'mrkdwn' }
    ]
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
      .and_return({ 'ok' => true, 'channel' => 'channel' })

    allow(ReleaseTools::GitlabClient)
      .to receive(:next_security_tracking_issue)
      .and_return(tracking_issue)
  end

  describe '#send_notification' do
    context 'with the blog post' do
      let(:issuable) { instance_double(ReleaseTools::PatchRelease::BlogMergeRequest, url: 'http://foo') }
      let(:issue_type) { 'patch_blog_post' }

      it 'posts a message on the AppSec channel' do
        content =
          ":security-tanuki: :ci_passing: *The <#{issuable.url}|Patch Blog Post> for the <#{tracking_issue.web_url}|upcoming patch release> has been successfully created.*"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::SEC_APPSEC,
              message: "The Patch Blog Post has been created: #{issuable.url}",
              blocks: slack_mrkdwn_block(text: content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.send_notification
        end
      end
    end

    context 'with the AppSec task issue' do
      let(:issuable) { instance_double(ReleaseTools::Security::AppSecIssue, url: 'http://foo') }
      let(:issue_type) { 'appsec_task_issue' }

      it 'posts a message on the AppSec channel' do
        content =
          ":security-tanuki: :ci_passing: *The <#{issuable.url}|Appsec Task Issue> for the <#{tracking_issue.web_url}|upcoming patch release> has been successfully created.*"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::SEC_APPSEC,
              message: "The Appsec Task Issue has been created: #{issuable.url}",
              blocks: slack_mrkdwn_block(text: content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.send_notification
        end
      end
    end

    context 'with the Security comms issue' do
      let(:issuable) { instance_double(ReleaseTools::Security::CommsTaskIssue, url: 'http://foo') }
      let(:issue_type) { 'comms_security_task_issue' }

      it 'posts a message on the AppSec channel' do
        content =
          ":security-tanuki: :ci_passing: *The <#{issuable.url}|Comms Security Task Issue> for the <#{tracking_issue.web_url}|upcoming patch release> has been successfully created.*"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::SEC_APPSEC,
              message: "The Comms Security Task Issue has been created: #{issuable.url}",
              blocks: slack_mrkdwn_block(text: content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.send_notification
        end
      end
    end
  end
end
