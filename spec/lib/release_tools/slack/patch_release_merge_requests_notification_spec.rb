# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::PatchReleaseMergeRequestsNotification do
  include SlackWebhookHelpers

  let(:webhook_url) { 'https://slack.example.com/' }

  describe '.webhook_url' do
    subject { described_class.webhook_url }

    it 'returns ENV value when set' do
      ClimateControl.modify(SLACK_CHATOPS_URL: webhook_url) do
        expect(subject).to eq(webhook_url)
      end
    end

    it 'raises KeyError when not set' do
      ClimateControl.modify(SLACK_CHATOPS_URL: nil) do
        expect { subject }.to raise_error(KeyError)
      end
    end
  end

  describe '.notify' do
    let(:patch_version_data) do
      [
        {
          version: '9.2.2',
          pressure: 2,
          merge_requests: {
            'gitlab-org/gitlab' => [
              { 'title' => 'foo', 'web_url' => 'https://foo.com' },
              { 'title' => 'bar', 'web_url' => 'https://bar.com' }
            ],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => []
          }
        },
        {
          version: '9.1.3',
          pressure: 1,
          merge_requests: {
            'gitlab-org/gitlab' => [],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => [{ 'title' => 'baz', 'web_url' => 'https://baz.com' }]
          }
        },
        {
          version: '9.0.4',
          pressure: 0,
          merge_requests: {
            'gitlab-org/gitlab' => [],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => []
          }
        }
      ]
    end

    subject { described_class.notify(patch_version_data) }

    around do |ex|
      ClimateControl.modify(SLACK_CHATOPS_URL: webhook_url) do
        ex.run
      end
    end

    it 'posts a webhook' do
      expect_post(
        json: {
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: '*Merge requests merged into upcoming patch versions*'
              }
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: "```\n### 9.2.2\n\n* [foo](https://foo.com)\n* [bar](https://bar.com)\n\n### 9.1.3\n\n* [baz](https://baz.com)\n```"
              }
            }
          ],
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE
        }
      ).and_return(response(200))

      subject
    end

    context 'when there are no merge requests' do
      let(:patch_version_data) do
        [
          {
            version: '9.2.2',
            pressure: 0,
            merge_requests: {
              'gitlab-org/gitlab' => [],
              'gitlab-org/gitaly' => [],
              'gitlab-org/omnibus-gitlab' => []
            }
          },
          {
            version: '9.1.3',
            pressure: 0,
            merge_requests: {
              'gitlab-org/gitlab' => [],
              'gitlab-org/gitaly' => [],
              'gitlab-org/omnibus-gitlab' => []
            }
          },
          {
            version: '9.0.4',
            pressure: 0,
            merge_requests: {
              'gitlab-org/gitlab' => [],
              'gitlab-org/gitaly' => [],
              'gitlab-org/omnibus-gitlab' => []
            }
          }
        ]
      end

      it 'returns an alternate message' do
        expect_post(
          json: {
            blocks: [
              {
                type: 'section',
                text: {
                  type: 'mrkdwn',
                  text: '*No merge requests are awaiting patch release*'
                }
              }
            ],
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE
          }
        ).and_return(response(200))

        subject
      end
    end
  end
end
