# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::ReleaseJobEndNotifier do
  let(:job_type) { 'Omnibus nightly builds' }
  let(:status) { :success }
  let(:job_url) { 'https://example.com/foo/bar/-/jobs/1' }
  let(:local_time) { Time.utc(2023, 5, 14, 15, 55, 0) }

  let(:context_elements) do
    [
      { 'text' => ':clock1: 2023-05-14 15:55 UTC', 'type' => 'mrkdwn' }
    ]
  end

  subject(:notifier) do
    described_class.new(
      job_type: job_type,
      status: status,
      release_type: :patch
    )
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
      .and_return({ 'ok' => true, 'channel' => 'channel' })
  end

  around do |ex|
    ClimateControl.modify(CI_JOB_URL: job_url) do
      ex.run
    end
  end

  describe '#send_notification' do
    it 'posts a message on Slack' do
      content = ":security-tanuki: :ci_passing: *Omnibus nightly builds job <#{job_url}|successfully> executed*"

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            message: "Omnibus nightly builds job successfully executed - #{job_url}",
            blocks: slack_mrkdwn_block(text: content, context_elements: context_elements)
          }
        )

      Timecop.freeze(local_time) do
        notifier.send_notification
      end
    end
  end

  context 'with a failure' do
    let(:status) { :failed }

    it 'posts a message on Slack' do
      content =
        ":security-tanuki: :ci_failing: *Omnibus nightly builds job failed, review the <#{job_url}|job log> for details*"

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            message: "Omnibus nightly builds job failed, review the job log for details - #{job_url}",
            blocks: slack_mrkdwn_block(text: content, context_elements: context_elements)
          }
        )

      Timecop.freeze(local_time) do
        notifier.send_notification
      end
    end
  end

  context 'with extra string' do
    subject(:notifier) do
      described_class.new(
        job_type: job_type,
        status: status,
        release_type: :patch,
        extra_string: 'An extra string'
      )
    end

    it 'posts a message on Slack' do
      content = ":security-tanuki: :ci_passing: *Omnibus nightly builds job <#{job_url}|successfully> executed*"

      block1 = slack_mrkdwn_block(text: content)
      block2 = slack_mrkdwn_block(text: 'An extra string', context_elements: context_elements)
      slack_blocks = block1 + block2

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            message: "Omnibus nightly builds job successfully executed - #{job_url}",
            blocks: slack_blocks
          }
        )

      Timecop.freeze(local_time) do
        notifier.send_notification
      end
    end
  end
end
