# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::PostDeployPipelineInitialNotification do
  subject(:initial_notification) { described_class.new }

  before do
    allow(ReleaseTools::Slack::ChatopsNotification)
      .to receive(:fire_hook)
      .and_return({})
  end

  describe '#no_pending_post_migration_message' do
    it 'sends a chatops message' do
      block_message =
        ":ci_skipped: No pending post migrations available, skipping <https://test.com/-/release-tools/pipeline/123|post-deploy pipeline> execution\n\n"

      expect(ReleaseTools::Slack::ChatopsNotification)
        .to receive(:fire_hook)
        .with(
          {
            text: 'No pending post migrations available',
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            blocks: slack_mrkdwn_block(text: block_message)
          }
        )

      pipeline_url = 'https://test.com/-/release-tools/pipeline/123'

      ClimateControl.modify(CI_PIPELINE_URL: pipeline_url) do
        without_dry_run do
          initial_notification.no_pending_post_migrations_message
        end
      end
    end

    context 'with dry-run' do
      it 'skips sending a message' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        initial_notification.no_pending_post_migrations_message
      end
    end
  end

  describe '#wraparound_vacuum_on_post_migrations_message' do
    it 'sends a chatops message' do
      block_message =
        ":ci_skipped: <https://dashboards.gitlab.net/explore?schemaVersion=1&orgId=1&panes=%7B%22abc%22%3A%7B%22datasource%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%2C%22queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22expr%22%3A%22pg_stat_activity_autovacuum_age_in_seconds%7Btype%3D%5C%22patroni-ci%5C%22%2C+relname%3D~%5C%22.%2Awraparound.%2A%5C%22%7D%22%2C%22range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22uid%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D%2C%22editorMode%22%3A%22code%22%7D%5D%2C%22range%22%3A%7B%22from%22%3A%22now-2h%22%2C%22to%22%3A%22now%22%7D%7D%7D|Wraparound autovacuum detected>, skipping <https://test.com/-/release-tools/pipeline/123|post-deploy pipeline> execution. Please retry the PDM at a later time\n\n"

      expect(ReleaseTools::Slack::ChatopsNotification)
        .to receive(:fire_hook)
        .with(
          {
            text: 'Wraparound autovacuum detected',
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            blocks: slack_mrkdwn_block(text: block_message)
          }
        )

      pipeline_url = 'https://test.com/-/release-tools/pipeline/123'

      ClimateControl.modify(CI_PIPELINE_URL: pipeline_url) do
        without_dry_run do
          initial_notification.wraparound_vacuum_on_post_migrations_message
        end
      end
    end

    context 'with dry-run' do
      it 'skips sending a message' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        initial_notification.wraparound_vacuum_on_post_migrations_message
      end
    end
  end

  describe '#production_status_failed_message' do
    let(:production_status) do
      instance_double(
        ReleaseTools::Promotion::ProductionStatus,
        {
          to_slack_blocks: ['blocks']
        }
      )
    end

    it 'sends a chatops message' do
      block_message =
        ":red_circle: <!subteam^#{ReleaseTools::Slack::RELEASE_MANAGERS}> Post-deployment pipeline is blocked\n\n"

      expect(ReleaseTools::Slack::ChatopsNotification)
        .to receive(:fire_hook)
        .with(
          {
            text: 'Post-deployment pipeline is blocked',
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            blocks: slack_mrkdwn_block(text: block_message) + ['blocks']
          }
        )

      without_dry_run do
        initial_notification.production_status_failed_message(production_status)
      end
    end

    context 'with a dry-run' do
      it 'skips sending a message' do
        expect(ReleaseTools::Slack::ChatopsNotification)
          .not_to receive(:fire_hook)

        initial_notification.production_status_failed_message(production_status)
      end
    end
  end
end
