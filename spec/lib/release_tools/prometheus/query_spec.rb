# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Prometheus::Query do
  subject(:prom_query) { described_class.new }

  let(:client) { instance_double(HTTP::Client) }

  before do
    allow(HTTP)
      .to receive(:headers)
      .with({
              'X-Scope-OrgID' => 'gitlab-gprd',
              'Authorization' => anything
            })
      .and_return(client)

    allow(client)
      .to receive(:get)
      .with(described_class::ENDPOINT, anything)
      .and_return(
        instance_spy(
          HTTP::Response,
          status: HTTP::Response::Status.new(200),
          parse: json_response
        )
      )
  end

  describe '#run' do
    subject(:run_query) { prom_query.run(query) }

    let(:json_response) { {} }
    let(:query) { 'gitlab_deployment_health:service' }

    it 'returns parsed response' do
      expect(run_query).to eq(json_response)
    end

    context 'with connection error' do
      before do
        allow(client)
          .to receive(:get)
          .and_raise(HTTP::ConnectionError)
      end

      it 'retries and raises error on repeated failure' do
        expect(client).to receive(:get).at_least(3).times

        expect { run_query }.to raise_error(HTTP::ConnectionError)
      end
    end

    context 'with error response' do
      before do
        allow(client)
          .to receive(:get)
          .and_return(
            instance_spy(
              HTTP::Response,
              status: HTTP::Response::Status.new(400)
            )
          )
      end

      it 'retries and raises error on repeated failure' do
        expect(client).to receive(:get).at_least(3).times

        expect { run_query }.to raise_error(described_class::PromQLError)
      end
    end
  end

  describe '#monthly_release_status' do
    let(:version) { '16.10.0' }
    let(:query) { "last_over_time(delivery_release_monthly_status{version=#{version}}[30m])" }
    let(:json_response) { JSON.parse(File.read(File.expand_path("../../../fixtures/promql/delivery_monthly_release_status.json", __dir__))) }

    before do
      allow(HTTP)
      .to receive(:headers)
      .with({
              'X-Scope-OrgID' => 'gitlab-ops',
              'Authorization' => anything
            })
      .and_return(client)
    end

    it 'returns the monthly release status' do
      ClimateControl.modify(MIMIR_URL: "mimir_url") do
        expect(prom_query.monthly_release_status(version)).to eq(1)
      end
    end
  end

  describe '#patch_release_status' do
    let(:versions) { '16.10 16.9 16.8' }
    let(:query) { "last_over_time(delivery_release_patch_status{versions=#{versions}}[30m])" }
    let(:json_response) { JSON.parse(File.read(File.expand_path("../../../fixtures/promql/delivery_patch_release_status.json", __dir__))) }

    before do
      allow(HTTP)
      .to receive(:headers)
      .with({
              'X-Scope-OrgID' => 'gitlab-ops',
              'Authorization' => anything
            })
      .and_return(client)
    end

    it 'returns the patch release status' do
      ClimateControl.modify(MIMIR_URL: "mimir_url") do
        expect(prom_query.patch_release_status(versions)).to eq(1)
      end
    end
  end
end
