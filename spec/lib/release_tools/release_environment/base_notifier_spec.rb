# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::ReleaseEnvironment::BaseNotifier do
  subject(:notifier) do
    described_class.new(pipeline_url: pipeline_url,
                        environment_name: environment_name,
                        release_environment_version: release_environment_version)
  end

  let(:pipeline_url) { "https://gitlab.com/gitlab-org/gitlab/-/pipelines/1" }
  let(:environment_name) { 'staging' }
  let(:release_environment_version) { '12.9.0' }

  describe '#initialize' do
    it 'sets the pipeline_url' do
      expect(notifier.instance_variable_get(:@pipeline_url)).to eq(pipeline_url)
    end

    it 'sets the environment_name' do
      expect(notifier.instance_variable_get(:@environment_name)).to eq(environment_name)
    end

    it 'sets the release_environment_version' do
      expect(notifier.instance_variable_get(:@release_environment_version)).to eq(release_environment_version)
    end
  end

  describe '#project_path' do
    context 'when mirror is "canonical"' do
      it 'returns the canonical path' do
        expect(notifier.send(:project_path)).to eq(ReleaseTools::Project::GitlabEe.path)
      end
    end

    context 'when mirror is "security"' do
      let(:pipeline_url) { "https://gitlab.com/gitlab-org/security/gitlab/-/pipelines/1" }

      it 'returns the security path' do
        expect(notifier.send(:project_path)).to eq(ReleaseTools::Project::GitlabEe.security_path)
      end
    end

    context 'when mirror is invalid' do
      let(:pipeline_url) { "https://gitlab.com/gitlab-org/something/-/pipelines/1" }

      it 'raises an ArgumentError' do
        expect { notifier.send(:project_path) }.to raise_error(ArgumentError, 'Invalid pipeline URL: https://gitlab.com/gitlab-org/something/-/pipelines/1')
      end
    end
  end

  describe '#send_slack_notification' do
    it 'raises a NotImplementedError' do
      expect { notifier.send(:send_slack_notification) }.to raise_error(NotImplementedError)
    end
  end
end
