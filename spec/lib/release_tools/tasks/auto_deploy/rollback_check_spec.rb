# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::AutoDeploy::RollbackCheck do
  include RollbackHelper

  let(:fake_compare_service) { double(:compare_service) }
  let(:current_rails_sha) { 'bf7bc31ffb1' }
  let(:web_url) { 'https://test.gitlab.com/gitlab/-/compare/bf7bc31ffb1...10438b18493' }

  subject(:service) { described_class.new }

  def stub_comparison(safe: true)
    attributes = {
      current: stub_product_version('bf7bc31ffb1', '2c2465c4dca'),
      target: stub_product_version('10438b18493', '6793946906e'),
      minimum_version: ReleaseTools::ProductVersion.new('15.2.202207050620'),
      environment: 'gstg',
      web_url: web_url,
      current_rails_sha: current_rails_sha,
      safe?: safe
    }

    component_diffs = ["omnibus-gitlab - <https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/compare/sha1...sha2|sha1...sha2>"]
    allow(ReleaseTools::Metadata::Presenter)
      .to receive(:new)
      .and_return(instance_double(ReleaseTools::Metadata::Presenter, component_diffs: component_diffs))

    rollback_stub_comparison(attributes)
  end

  def stub_auto_deploy_version(package)
    ReleaseTools::AutoDeploy::Version
      .new(package)
  end

  def stub_main_reason_block(reason)
    {
      text: {
        emoji: true,
        text: reason,
        type: 'plain_text'
      },
      type: 'header'
    }
  end

  def stub_divider
    {
      type: 'divider'
    }
  end

  def stub_rollback_unavailable_reasons_block
    {
      text: {
        text: "- A deployment is in progress\n- Post-deployment migrations were executed after the target version on 15.2.202207050620",
        type: 'mrkdwn'
      },
      type: 'section'
    }
  end

  def stub_comparison_block
    comparison_text = <<~HEREDOC
      *Current:* `42.1.2021110116-bf7bc31ffb1.2c2465c4dca`

      *Target:* `42.1.2021110116-10438b18493.6793946906e`

      *Comparison:* https://test.gitlab.com/gitlab/-/compare/bf7bc31ffb1...10438b18493

      *Comparison for each component:*

      • omnibus-gitlab - <https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/compare/sha1...sha2|sha1...sha2>
    HEREDOC

    {
      text: {
        text: comparison_text,
        type: 'mrkdwn'
      },
      type: 'section'
    }
  end

  def stub_elements_block
    {
      elements: [
        {
          text: ":book: <https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/rollback-a-deployment.md|View runbook>",
          type: 'mrkdwn'
        }
      ],
      type: 'context'
    }
  end

  describe '#execute' do
    around do |ex|
      ClimateControl.modify(
        SLACK_TOKEN: '123456',
        ROLLBACK_TARGET: '42.1.2021121314+f7e5665fa11.3fb2052b8b6',
        ROLLBACK_ENV: 'gprd',
        &ex
      )
    end

    before do
      allow(ReleaseTools::Rollback::CompareService)
        .to receive(:new)
        .with({ target: '42.1.2021121314+f7e5665fa11.3fb2052b8b6', environment: 'gprd' })
        .and_return(fake_compare_service)

      allow(ReleaseTools::Rollback::UpcomingDeployments)
        .to receive(:new)
        .and_return(rollback_upcoming_deployment_stub)

      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
    end

    context 'when a rollback is available' do
      before do
        allow(fake_compare_service)
          .to receive(:execute)
          .and_return(stub_comparison)
      end

      it "states it's safe to rollback" do
        blocks = [
          stub_main_reason_block(':large_green_circle: Rollback available'),
          stub_divider,
          stub_comparison_block,
          stub_elements_block
        ]

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              message: 'Rollback check results',
              blocks: blocks
            }
          )

        service.execute
      end
    end

    context 'when post migrations have been executed' do
      it "states it's not safe to rollback" do
        comparison = stub_comparison(safe: false)

        allow(fake_compare_service)
          .to receive(:execute)
          .and_return(comparison)

        blocks = [
          stub_main_reason_block(':red_circle: Rollback unavailable - Post-deployment migrations were executed after the target version on 15.2.202207050620'),
          stub_divider,
          stub_comparison_block,
          stub_elements_block
        ]

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              message: 'Rollback check results',
              blocks: blocks
            }
          )

        service.execute
      end
    end

    context 'with a deployment in progress' do
      it "states it's not safe to rollback" do
        allow(fake_compare_service)
          .to receive(:execute)
          .and_return(stub_comparison)

        allow(ReleaseTools::Rollback::UpcomingDeployments)
          .to receive(:new)
          .and_return(rollback_upcoming_deployment_stub(any?: true))

        blocks = [
          stub_main_reason_block(':red_circle: Rollback unavailable - A deployment is in progress'),
          stub_divider,
          stub_comparison_block,
          stub_elements_block
        ]

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              message: 'Rollback check results',
              blocks: blocks
            }
          )

        service.execute
      end
    end
  end
end
