# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::AutoDeploy::Tag do
  subject(:task) { described_class.new }

  let(:branch) { ReleaseTools::AutoDeployBranch.current }
  let(:version) { ReleaseTools::AutoDeploy::Tag.generate }

  around do |ex|
    ClimateControl.modify(AUTO_DEPLOY_BRANCH: '42-1-auto-deploy-2021010200') do
      ex.run
    end
  end

  describe '#execute' do
    let(:passing_build) { instance_double(ReleaseTools::PassingBuild) }
    let(:omnibus_builder) { instance_double(ReleaseTools::AutoDeploy::Builder::Omnibus) }
    let(:cng_builder) { instance_double(ReleaseTools::AutoDeploy::Builder::CNGImage) }
    let(:omnibus_changes) { true }
    let(:cng_changes) { true }
    let(:metadata) { double('metadata') }
    let(:fake_metrics) { spy('metrics') }

    before do
      commit = double('commit', id: 'abc123')

      allow(passing_build).to receive(:for_auto_deploy_tag).and_return(commit)
      allow(task).to receive(:metadata).and_return(metadata)

      allow(ReleaseTools::PassingBuild).to receive(:new)
        .with(branch.to_s)
        .and_return(passing_build)
      allow(ReleaseTools::AutoDeploy::Builder::Omnibus).to receive(:new)
        .with(branch, commit.id, version, metadata)
        .and_return(omnibus_builder)
      allow(ReleaseTools::AutoDeploy::Builder::CNGImage).to receive(:new)
        .with(branch, commit.id, version, metadata)
        .and_return(cng_builder)

      allow(omnibus_builder).to receive(:changes?).and_return(omnibus_changes)
      allow(cng_builder).to receive(:changes?).and_return(cng_changes)

      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(fake_metrics)
    end

    shared_examples 'a full tagging run' do
      it 'performs a full tagging run' do
        expect(task).to receive(:find_passing_build)

        expect(omnibus_builder).to receive(:execute)
        expect(cng_builder).to receive(:execute)
        expect(task).to receive(:upload_metadata)
        expect(task).to receive(:tag_coordinator)
        expect(fake_metrics).to receive(:inc)

        task.execute
      end
    end

    it_behaves_like 'a full tagging run'

    context 'when at least a builder has changes' do
      [true, false].permutation do |omnibus, cng|
        context "omnibus_changes: #{omnibus} cng_changes: #{cng}" do
          let(:omnibus_changes) { omnibus }
          let(:cng_changes) { cng }

          it_behaves_like 'a full tagging run'
        end
      end
    end

    it 'does not tag coordinator without builder changes' do
      expect(task).to receive(:find_passing_build)

      expect(omnibus_builder).to receive(:changes?).and_return(false)
      expect(cng_builder).to receive(:changes?).and_return(false)

      expect(omnibus_builder).not_to receive(:execute)
      expect(cng_builder).not_to receive(:execute)
      expect(task).not_to receive(:upload_metadata)
      expect(task).not_to receive(:tag_coordinator)
      expect(fake_metrics).not_to receive(:inc)

      task.execute
    end

    it 'executes Coordinator tagger' do
      tagger = stub_const('ReleaseTools::AutoDeploy::Tagger::Coordinator', spy)

      allow(task).to receive(:find_passing_build)
      allow(omnibus_builder).to receive(:execute)
      allow(cng_builder).to receive(:execute)
      allow(task).to receive(:upload_metadata)

      expect(task).to receive(:tag_coordinator).and_call_original

      task.execute

      expect(tagger).to have_received(:new)
      expect(tagger).to have_received(:tag!)
    end

    it 'uploads release metadata' do
      uploader = stub_const('ReleaseTools::ReleaseMetadataUploader', spy)

      allow(task).to receive(:find_passing_build)
      allow(omnibus_builder).to receive(:execute)
      allow(cng_builder).to receive(:execute)
      allow(task).to receive(:tag_coordinator)

      expect(task).to receive(:upload_metadata).and_call_original

      task.execute

      expect(uploader).to have_received(:new)
      expect(uploader).to have_received(:upload)
        .with(version, metadata, auto_deploy: true)
    end

    it 'increments the package tagging metric' do
      stub_const('ReleaseTools::ReleaseMetadataUploader', spy)

      allow(task).to receive(:find_passing_build)
      allow(omnibus_builder).to receive(:execute)
      allow(cng_builder).to receive(:execute)
      allow(task).to receive(:tag_coordinator)

      expect(fake_metrics).to receive(:inc).with("packages_tagging_total", labels: 'auto_deploy,no')

      task.execute
    end

    context 'with packagers_only' do
      subject(:task) { described_class.new(packagers_only: true) }

      it 'tags only packagers' do
        expect(task).to receive(:find_passing_build)

        expect(omnibus_builder).to receive(:execute)
        expect(cng_builder).to receive(:execute)
        expect(task).to receive(:upload_metadata)
        expect(task).not_to receive(:tag_coordinator)

        task.execute
      end
    end
  end
end
