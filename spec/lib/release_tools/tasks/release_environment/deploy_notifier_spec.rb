# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::ReleaseEnvironment::DeployNotifier do
  subject(:instance) do
    described_class.new(
      pipeline_url: pipeline_url,
      environment_name: environment_name,
      release_environment_version: release_environment_version
    )
  end

  describe '#execute' do
    let(:pipeline_url) { 'https://gitlab.com/gitlab-org/gitlab/-/pipelines/1' }
    let(:environment_name) { 'my_env' }
    let(:release_environment_version) { 'my_version' }
    let(:notifier) { instance_double(ReleaseTools::ReleaseEnvironment::DeployNotifier) }

    before do
      allow(ReleaseTools::ReleaseEnvironment::DeployNotifier).to receive(:new).and_return(notifier)
    end

    it 'create ReleaseTools::ReleaseEnvironment::DeployNotifier instance' do
      expect(ReleaseTools::ReleaseEnvironment::DeployNotifier).to receive(:new).with(
        pipeline_url: pipeline_url,
        environment_name: environment_name,
        release_environment_version: release_environment_version
      )
      expect(notifier).to receive(:execute)
      instance.execute
    end
  end
end
