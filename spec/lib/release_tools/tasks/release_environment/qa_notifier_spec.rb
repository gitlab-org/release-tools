# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::ReleaseEnvironment::QaNotifier do
  subject(:instance) do
    described_class.new(
      pipeline_url: pipeline_url,
      environment_name: environment_name,
      release_environment_version: release_environment_version
    )
  end

  describe '#execute' do
    let(:pipeline_url) { 'https://gitlab.com/gitlab-org/security/gitlab/-/pipelines/1' }
    let(:environment_name) { 'my_env' }
    let(:release_environment_version) { 'my_version' }
    let(:notifier) { instance_double(ReleaseTools::ReleaseEnvironment::QaNotifier) }

    before do
      allow(ReleaseTools::ReleaseEnvironment::QaNotifier).to receive(:new).and_return(notifier)
    end

    it 'create ReleaseTools::ReleaseEnvironment::QaNotifier instance' do
      expect(ReleaseTools::ReleaseEnvironment::QaNotifier).to receive(:new).with(
        pipeline_url: pipeline_url,
        environment_name: environment_name,
        release_environment_version: release_environment_version
      )
      expect(notifier).to receive(:execute)
      instance.execute
    end
  end
end
