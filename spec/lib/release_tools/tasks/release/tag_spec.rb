# frozen_string_literal: true

require 'spec_helper'
require 'rake_helper'

RSpec.describe ReleaseTools::Tasks::Release::Tag do
  let(:version) { '1.2.0' }
  let(:tag_release) { described_class.new(version) }
  let(:notifier) { instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: true) }
  let(:logger) { instance_double(SemanticLogger::Logger) }
  let(:exception) { StandardError.new('Something went wrong') }

  before do
    allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .and_return(notifier)

    allow(described_class).to receive(:logger).and_return(logger)
  end

  describe '#initialize' do
    it 'sets the version' do
      expect(tag_release.instance_variable_get(:@version)).to eq(version)
    end
  end

  describe '#execute' do
    context 'when successful' do
      it 'sends a success notification' do
        expect(tag_release).to receive(:send_slack_notification).with(:success)
        expect(Rake::Task).to receive(:[]).with('release:tag').and_return(instance_double(Rake::Task, invoke: true))
        expect(logger).to receive(:info).with(include("Starting to tag #{version}"))

        without_dry_run do
          tag_release.execute
        end
      end
    end

    context 'when an error occurs' do
      it 'sends a failed notification and logs the expected error message' do
        expect(tag_release).to receive(:send_slack_notification).with(:failed)

        error_message = "Tagging the monthly release failed with error: #{exception.message}.\nReview the error log and consider retrying this job."
        expect(logger).to receive(:fatal).with(include(error_message), error: exception)
        expect(logger).to receive(:info).with(include("Starting to tag #{version}"))

        allow(Rake::Task).to receive(:[]).with('release:tag').and_raise(exception)

        without_dry_run do
          expect { tag_release.execute }.to raise_error(StandardError)
        end
      end
    end

    context 'when tagging a monthly release' do
      let(:expected_release_type) { :monthly }

      it 'sends a notification with the correct parameters' do
        allow(Rake::Task)
          .to receive(:[])
          .with('release:tag')
          .and_return(instance_double(Rake::Task, invoke: true))

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
          .to receive(:new)
          .with(job_type: "Tag #{expected_release_type} release version #{version}", status: :success, release_type: expected_release_type)
          .and_return(notifier)

        expect(logger).to receive(:info).with(include("Starting to tag #{version}"))
        expect(notifier).to receive(:send_notification)

        without_dry_run do
          tag_release.execute
        end
      end
    end

    context 'when tagging a patch release' do
      let(:version) { '1.2.3' }
      let(:expected_release_type) { :patch }

      it 'sends a notification with the correct parameters' do
        allow(Rake::Task)
          .to receive(:[])
          .with('release:tag')
          .and_return(instance_double(Rake::Task, invoke: true))

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
          .to receive(:new)
          .with(job_type: "Tag #{expected_release_type} release version #{version}", status: :success, release_type: expected_release_type)
          .and_return(notifier)

        expect(logger).to receive(:info).with(include("Starting to tag #{version}"))
        expect(logger).to receive(:warn).with(include("Using security repository only!"))
        expect(notifier).to receive(:send_notification)

        without_dry_run do
          tag_release.execute
        end
      end
    end
  end
end
