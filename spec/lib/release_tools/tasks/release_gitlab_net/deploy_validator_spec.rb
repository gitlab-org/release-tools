# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Tasks::ReleaseGitlabNet::DeployValidator do
  subject(:validator) { described_class.new(version) }

  let(:version) { '15.5.5' } # Provide a default version for the tests
  let(:comparer) do
    instance_double(
      ReleaseTools::Deployments::CompareReleaseVersions,
      equal_to_actual_release_version?: expected_version_deployed,
      actual_version: actual_version
    )
  end
  let(:notifier) { instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: true) }

  before do
    allow(ReleaseTools::Deployments::CompareReleaseVersions)
      .to receive(:new)
      .and_return(comparer)

    allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .and_return(notifier)
  end

  describe '#execute' do
    shared_examples 'when the platform runs the expected version' do
      let(:expected_version_deployed) { true }

      it 'outputs a success log message and sends a slack notification' do
        ee_version = ReleaseTools::Version.new(version).to_ee

        expect(validator.logger).to receive(:info).with(
          "Release.gitlab.net is running the expected #{release_type} version",
          version: ee_version
        )

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
          .to receive(:new)
          .with(
            job_type: "Validate release.gitlab.net running #{ee_version}",
            status: :success,
            release_type: release_type
          )

        validator.execute
      end
    end

    shared_examples 'when the platform runs a different version' do
      let(:expected_version_deployed) { false }

      it 'outputs a failure log message and sends a slack notification' do
        ee_version = ReleaseTools::Version.new(version).to_ee

        expect(validator.logger).to receive(:warn).with(
          "Release.gitlab.net is not running the expected #{release_type} version. Retrying",
          expected: ee_version,
          actual: actual_version
        ).exactly(110).times

        expect(validator.logger).to receive(:fatal).with(
          "Release.gitlab.net is not running the expected #{release_type} version. Check that the deploy has successfully completed or if the version reflects in https://release.gitlab.net/help.",
          expected_version: ee_version,
          actual_version: actual_version
        )

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
          .to receive(:new)
          .with(
            job_type: "Validate release.gitlab.net running #{ee_version}",
            status: :failed,
            release_type: release_type
          )

        expect { validator.execute }.to raise_error(described_class::UnexpectedReleaseVersionError)
      end
    end

    context 'with a patch release' do
      let(:version) { '15.5.5' }
      let(:release_type) { :patch }

      context 'when release.gitlab.net is running the expected version' do
        let(:actual_version) { ReleaseTools::Version.new('15.5.5-ee') }

        it_behaves_like 'when the platform runs the expected version'
      end

      context 'when release.gitlab.net is not running the expected version' do
        let(:actual_version) { ReleaseTools::Version.new('15.5.4-ee') }

        it_behaves_like 'when the platform runs a different version'
      end
    end

    context 'with a monthly release' do
      let(:version) { '15.5.0' }
      let(:release_type) { :monthly }

      context 'when release.gitlab.net is running the expected version' do
        let(:actual_version) { ReleaseTools::Version.new('15.5.0-ee') }

        it_behaves_like 'when the platform runs the expected version'
      end

      context 'when release.gitlab.net is not running the expected version' do
        let(:actual_version) { ReleaseTools::Version.new('15.4.0-ee') }

        it_behaves_like 'when the platform runs a different version'
      end
    end
  end
end
