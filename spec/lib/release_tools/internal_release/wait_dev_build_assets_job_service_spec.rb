# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::WaitDevBuildAssetsJobService do
  subject(:service) { described_class.new(version: version, commit: commit) }

  let(:version) { ReleaseTools::Version.new('17.7.0') }
  let(:commit) { build(:commit, id: 'abcd', web_url: 'https://test.com/project/-/abcd') }
  let(:dev_client) { stub_const('ReleaseTools::GitlabDevClient', spy) }
  let(:gitlab_client) { stub_const('ReleaseTools::GitlabClient', spy) }

  before do
    enable_feature(:internal_release_wait_for_build_assets_image)
  end

  describe '#execute' do
    before do
      allow(dev_client).to receive_messages(
        commit: commit,
        pipelines: [create(:pipeline, id: 4)],
        pipeline_job_by_name: create(:job, :success, name: 'build-assets-image')
      )
    end

    it 'waits for the build assets image' do
      expect(dev_client)
        .to receive(:commit)
        .with('gitlab/gitlab-ee', ref: 'abcd')

      expect(dev_client)
        .to receive(:pipelines)
        .with('gitlab/gitlab-ee', {
                ref: '17-7-stable-ee',
                sha: 'abcd',
                order_by: 'id',
                sort: 'asc',
                per_page: 1
              })

      expect(dev_client)
        .to receive(:pipeline_job_by_name)
        .with('gitlab/gitlab-ee', 4, 'build-assets-image')

      service.execute
    end

    context 'when the commit is not available on Dev' do
      before do
        allow(dev_client)
          .to receive(:commit)
          .and_return(nil)
      end

      it 'raises an error' do
        expect { service.execute }
          .to raise_error(described_class::CommitNotFound)
      end
    end

    context 'when the QA job image job is not successful' do
      before do
        allow(dev_client)
          .to receive(:pipeline_job_by_name)
          .and_return(create(:job, :failed, name: 'build-assets-image'))
      end

      it 'raises an error' do
        expect { service.execute }
          .to raise_error(described_class::JobNotSuccessful)
      end
    end

    context 'with internal_release_wait_for_build_assets_image disabled' do
      it 'only waits for commit' do
        disable_feature(:internal_release_wait_for_build_assets_image)

        expect(dev_client).to receive(:commit)
        expect(dev_client).not_to receive(:pipelines)
        expect(dev_client).not_to receive(:pipeline_job_by_name)

        service.execute
      end
    end
  end
end
