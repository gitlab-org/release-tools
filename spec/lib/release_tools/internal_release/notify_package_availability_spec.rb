# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::InternalRelease::NotifyPackageAvailability do
  subject(:notify_availability) { described_class.new(1) }

  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:schedule) { stub_const('ReleaseTools::ReleaseManagers::Schedule', spy) }

  let(:task_project) { ReleaseTools::Project::Release::Tasks }

  let(:iteration) { 1 }

  let(:versions) do
    [
      ReleaseTools::Version.new('15.2.1-internal1'),
      ReleaseTools::Version.new('15.1.2-internal1')
    ]
  end

  let(:slack_notifier) do
    instance_double(ReleaseTools::Slack::InternalRelease::PackageAvailabilityNotifier)
  end

  let(:slack_notifier_job_end) do
    instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: true)
  end

  let(:coordinator) do
    instance_double(ReleaseTools::InternalRelease::Coordinator, versions: versions)
  end

  describe '#execute' do
    let(:issue) do
      build(
        :issue,
        web_url: 'https://gitlab.com/issue/1',
        labels: ['internal releases']
      )
    end

    before do
      allow(client).to receive(:issues).and_return([issue])
      allow(client).to receive(:create_issue_note)

      allow(ReleaseTools::Slack::InternalRelease::PackageAvailabilityNotifier)
        .to receive(:new)
        .and_return(slack_notifier)

      allow(slack_notifier).to receive(:execute)

      allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
        .to receive(:new)
        .and_return(slack_notifier_job_end)

      allow(ReleaseTools::InternalRelease::Coordinator)
        .to receive(:new)
        .with(iteration)
        .and_return(coordinator)

      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      allow(schedule)
        .to receive(:active_release_managers)
        .and_return([
          build(:user, id: 1, username: 'user1'),
          build(:user, id: 2, username: 'user2')
        ])
    end

    context 'when in dry-run mode' do
      it 'does not perform actions when in dry-run mode' do
        notify_availability.execute

        expect(slack_notifier).not_to have_received(:execute)
        expect(client).not_to have_received(:create_issue_note)
        expect(client).not_to have_received(:issues)
        expect(slack_notifier_job_end).not_to have_received(:send_notification)
      end
    end

    it 'notifies release managers via Slack' do
      without_dry_run do
        notify_availability.execute

        expect(ReleaseTools::Slack::InternalRelease::PackageAvailabilityNotifier)
          .to have_received(:new).with(versions)

        expect(slack_notifier).to have_received(:execute)
        expect(slack_notifier_job_end).to have_received(:send_notification)
      end
    end

    it 'creates an issue note' do
      expected_body = <<~BODY
        @user1 @user2

        The internal packages for 15.2 and 15.1 are now available on the pre-release channel:

        * `15.2.1-internal1`: https://packages.gitlab.com/app/gitlab/pre-release/search?q=15.2.1%2Binternal
        * `15.1.2-internal1`: https://packages.gitlab.com/app/gitlab/pre-release/search?q=15.1.2%2Binternal
      BODY

      without_dry_run do
        notify_availability.execute

        expect(client).to have_received(:issues).with(
          task_project,
          hash_including(
            state: 'opened',
            author_id: ReleaseTools::Bot::GITLAB_COM_ID,
            labels: ['internal releases']
          )
        )

        expect(client).to have_received(:create_issue_note)
          .with(task_project, issue: issue, body: a_string_including(expected_body))
      end
    end

    context 'when an error occurs' do
      before do
        allow(ReleaseTools::Slack::InternalRelease::PackageAvailabilityNotifier)
          .to receive(:new)
          .and_raise(StandardError, 'Slack API Error')
      end

      it 'logs the error and re-raises it' do
        expect { without_dry_run { notify_availability.execute } }.to raise_error(StandardError, 'Slack API Error')

        expect(slack_notifier_job_end).to have_received(:send_notification)
      end
    end
  end
end
