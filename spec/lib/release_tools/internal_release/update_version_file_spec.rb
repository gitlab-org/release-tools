# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::InternalRelease::UpdateVersionFile do
  subject(:updater) { described_class.new(version: version, gitlab_branch: '16-1-stable-ee') }

  let(:version) { '15.1.0-internal0' }
  let(:gitlab_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:pipeline) { create(:pipeline, id: "456") }
  let(:commit) { create(:commit) }

  before do
    stub_const('ReleaseTools::GitlabClient', gitlab_client)
  end

  describe '#execute' do
    context 'when in dry run mode' do
      it 'returns nil' do
        expect(updater.execute).to be_nil
      end
    end

    context 'when version matches' do
      let(:commit_from_branch) { create(:commit, short_id: '123abc') }

      before do
        allow(gitlab_client).to receive(:file_contents)
          .with(
            'gitlab-org/security/gitlab',
            'VERSION',
            '16-1-stable-ee'
          ).and_return(version)

        allow(gitlab_client).to receive(:commits).and_return([commit_from_branch])
      end

      it 'returns last commit from stable branch' do
        without_dry_run do
          expect(updater.execute).to eq(commit_from_branch)
        end
      end

      it 'does not create a new commit' do
        without_dry_run do
          expect(gitlab_client)
            .not_to receive(:create_commit)

          updater.execute
        end
      end
    end

    context 'when version does not match' do
      before do
        allow(gitlab_client).to receive(:file_contents)
          .with(
            'gitlab-org/security/gitlab',
            'VERSION',
            '16-1-stable-ee'
          ).and_return('15.1.0')

        allow(gitlab_client).to receive(:create_commit)
          .with(
            'gitlab-org/security/gitlab',
            '16-1-stable-ee',
            "Update VERSION file for #{version}",
            [
              {
                action: 'update',
                file_path: 'VERSION',
                content: version
              }
            ]
          ).and_return(commit)
      end

      it 'creates commit with version update' do
        without_dry_run do
          expect(updater.execute).to eq(commit)
        end
      end
    end

    context 'when version file does not exist' do
      before do
        allow(gitlab_client).to receive(:file_contents).and_raise(StandardError)
      end

      it 'raises an error' do
        expect { without_dry_run { updater.execute } }.to raise_error(StandardError)
      end
    end
  end
end
