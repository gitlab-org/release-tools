# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::SyncToDevService do
  subject(:service) { described_class.new }

  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }

  before do
    allow(client)
      .to receive(:remote_mirrors)
      .and_return([create(:remote_mirror, id: 10, url: 'dev.gitlab.org/gitlab/gitlab-ee.git')])
  end

  describe '#execute' do
    it 'triggers a mirror from security to dev' do
      expect(client)
        .to receive(:sync_remote_mirror)
        .with('gitlab-org/security/gitlab', 10)

      service.execute
    end
  end
end
