# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::SharedStatus do
  describe '.dry_run?' do
    it 'returns true when set' do
      ClimateControl.modify(TEST: 'true') do
        expect(described_class.dry_run?).to be(true)
      end
    end

    it 'returns false when unset' do
      ClimateControl.modify(TEST: nil) do
        expect(described_class.dry_run?).to be(false)
      end
    end

    it 'returns false when set to "false"' do
      ClimateControl.modify(TEST: 'false') do
        expect(described_class.dry_run?).to be(false)
      end
    end

    it 'returns false when set to "False"' do
      ClimateControl.modify(TEST: 'False') do
        expect(described_class.dry_run?).to be(false)
      end
    end

    it 'returns true when not set to "false"' do
      ClimateControl.modify(TEST: 'yes') do
        expect(described_class.dry_run?).to be(true)
      end
    end
  end

  describe '.security_release?' do
    it 'returns true when set' do
      ClimateControl.modify(SECURITY: 'true') do
        expect(described_class.security_release?).to be(true)
      end
    end

    it 'returns false when unset' do
      ClimateControl.modify(SECURITY: nil) do
        expect(described_class.security_release?).to be(false)
      end
    end
  end

  describe '.as_security_release' do
    it 'allows enabling of patch releases for the duration of the block' do
      described_class.as_security_release do
        expect(described_class.security_release?).to be(true)
      end

      expect(described_class.security_release?).to be(false)
    end
  end

  describe '.rspec?' do
    it 'returns false' do
      expect(described_class.rspec?).to be(true)
    end

    it 'returns true when not rspec' do
      allow(File).to receive(:basename).with($PROGRAM_NAME).and_return('rake')

      expect(described_class.rspec?).to be(false)
    end
  end

  describe '.critical_patch_release?' do
    let(:security_fixes) { [] }

    before do
      allow(described_class).to receive(:security_fixes).and_return(security_fixes)
    end

    context 'when there are no security fixes' do
      it 'is not a critical patch release' do
        expect(described_class).not_to be_critical_patch_release
      end
    end

    context 'when there are security fixes' do
      let(:issue_with_critical_label) do
        create(:issue, labels: [described_class::CRITICAL_RELEASE_LABEL])
      end

      let(:issue_without_critical_label) do
        create(:issue, labels: ['other_label'])
      end

      context 'with a critical release label' do
        let(:security_fixes) { [issue_without_critical_label, issue_with_critical_label] }

        it 'is a critical patch release' do
          expect(described_class).to be_critical_patch_release
        end
      end

      context 'without a critical release label' do
        let(:security_fixes) { [issue_without_critical_label] }

        it 'is not a critical patch release' do
          expect(described_class).not_to be_critical_patch_release
        end
      end
    end
  end
end
