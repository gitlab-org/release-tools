# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::BlogPost::MergeRequestContentGenerator do
  subject(:generator) do
    described_class.new(
      bug_fixes: bug_fixes,
      security_fixes: security_fixes,
      metadata: metadata
    )
  end

  let(:bug_fixes) { [] }
  let(:blog_metadata_title) { 'Test Title' }
  let(:blog_metadata_description) { 'Test Description' }
  let(:author_gitlab) { 'Author GitLab' }
  let(:blog_metadata_tags) { 'blog_metadata_tags' }
  let(:release_name) { 'Release Name' }
  let(:security_fixes) { [] }
  let(:template_path) { File.expand_path('spec/fixtures/merge_requests/generator_template.md.erb') }
  let(:versions) { %w[18.0 18.1 18.2] }

  let(:metadata) do
    instance_spy(
      ReleaseTools::PatchRelease::BlogPost::Metadata,
      blog_title: blog_metadata_title,
      blog_description: blog_metadata_description,
      author_gitlab: author_gitlab,
      blog_tags: blog_metadata_tags,
      release_name: release_name,
      template_path: template_path,
      versions: versions
    )
  end

  let(:patch_coordinator) do
    instance_spy(
      ReleaseTools::PatchRelease::Coordinator,
      single_version?: false
    )
  end

  shared_context 'security content' do
    let(:cves_issue_low) { build(:cve_issue, :low_severity, id: 1) }
    let(:cves_issue_high) { build(:cve_issue, :high_severity, id: 2) }
    let(:cves_issue_none) { build(:cve_issue, id: 3) }
    let(:cves_issue_high_other) { build(:cve_issue, :high_severity, id: 4) }

    let(:issue_low) { build(:issue, cves_issue: cves_issue_low, title: 'sec issue 1') }
    let(:issue_high) { build(:issue, cves_issue: cves_issue_high, title: 'sec issue 2') }
    let(:issue_none) { build(:issue, cves_issue: cves_issue_none, title: 'sec issue 3') }

    let(:issue_high_other) do
      build(
        :issue,
        cves_issue: cves_issue_high_other,
        title: 'sec issue 4',
        project_id: ReleaseTools::Project::OmnibusGitlab.security_id
      )
    end

    let(:issue_no_cves) do
      build(
        :issue,
        cves_issue: nil,
        issue: instance_double(ReleaseTools::PatchRelease::Issue, title: 'sec issue 3')
      )
    end

    let(:security_fixes) do
      [issue_low, issue_no_cves, issue_high, issue_none, issue_high_other]
    end
  end

  describe "#execute" do
    before do
      allow(ReleaseTools::SharedStatus)
        .to receive(:critical_patch_release?)
        .and_return(false)
    end

    it 'generates the content with static data' do
      content = generator.execute

      expect(content).not_to be_blank
      expect(content).to include(blog_metadata_title)
      expect(content).to include(blog_metadata_description)
      expect(content).to include(author_gitlab)
      expect(content).to include(blog_metadata_tags)
      expect(content).to include(release_name)
    end

    context 'with invalid content path' do
      let(:template_path) { '/some/fake/path' }

      it 'raises a file not found error' do
        expect { generator.execute }.to raise_error(Errno::ENOENT)
      end
    end

    context 'with bug fixes' do
      let(:bug_fixes) { %w[foo bar] }

      it 'has the right amout of bug fixes' do
        content = generator.execute

        expect(content).to include("bug_fixes: Count 2")
        expect(content).to include("bug_fixes?: true")
      end
    end

    context 'with security fixes' do
      include_context 'security content'

      it 'has the right amout of security fixes' do
        content = generator.execute

        expect(content).to include("sorted_security_content: Count 5")
        expect(content).to include("sorted_table_content: Count 4")
      end

      it 'renders the right security issue header for security_issue_header' do
        content = generator.execute

        expect(content).to include("security - security_issue_header: cve issue 1")
        expect(content).to include("security - security_issue_header: cve issue 2")
        expect(content).to include("security - security_issue_header: cve issue 3")
        expect(content).to include("security - security_issue_header: cve issue 4")
        expect(content).to include("security - security_issue_header: sec issue 3") # cves_issue exists

        expect(content).not_to include("security - security_issue_header: sec issue 1")
        expect(content).not_to include("security - security_issue_header: sec issue 2")
        expect(content).not_to include("security - security_issue_header: sec issue 4")

        expect(content).to include("security_issue_slug: cve-issue-1")
        expect(content).to include("security_issue_slug: cve-issue-2")
        expect(content).to include("security_issue_slug: cve-issue-3")
        expect(content).to include("security_issue_slug: cve-issue-4")
        expect(content).to include("security_issue_slug: sec-issue-3")
        expect(content).not_to include("security_issue_slug: sec-issue-1")
        expect(content).not_to include("security_issue_slug: sec-issue-2")
        expect(content).not_to include("security_issue_slug: sec-issue-4")

        expect(content).to include("displayed_severity: High")
        expect(content).to include("displayed_severity: Low")
        expect(content).to include("displayed_severity: TODO")
      end

      it 'renders the right security issue header for sorted_table_content' do
        content = generator.execute

        expect(content).to include("table - security_issue_header: cve issue 1")
        expect(content).to include("table - security_issue_header: cve issue 2")
        expect(content).to include("table - security_issue_header: cve issue 3")
        expect(content).to include("table - security_issue_header: sec issue 3")

        expect(content).not_to include("table - security_issue_header: cve issue 4")
        expect(content).not_to include("table - security_issue_header: sec issue 1")
        expect(content).not_to include("table - security_issue_header: sec issue 2")
        expect(content).not_to include("table - security_issue_header: sec issue 4")
      end
    end

    context 'with versions' do
      let(:versions) { %w[1.0 1.1 1.2 1.3 1.4] }

      it 'renders the right versions' do
        content = generator.execute

        expect(content).to include("versions: 1.0, 1.1, 1.2, 1.3, 1.4")
      end
    end
  end
end
