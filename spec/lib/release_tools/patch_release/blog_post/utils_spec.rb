# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::BlogPost::Utils do
  before do
    stub_const("DummyClass", Struct.new(:versions, :release_name))
    DummyClass.send(:include, described_class)
  end

  let(:versions) { [] }
  let(:release_name) { nil }
  let(:utils) { DummyClass.new(versions: versions, release_name: release_name) }

  context "when versions exist" do
    let(:versions) { %w[1.2 1.3 1.4 1.5 1.6] }

    describe "hyphenated_version" do
      it "returns a hyphenated string of the first version" do
        expect(utils.hyphenated_version).to eq "1-2"
      end
    end

    describe "versions_str" do
      it "returns a comma-separated list of versions" do
        expect(utils.versions_str).to eq "1.2, 1.3, 1.4, 1.5, 1.6"
      end
    end
  end

  context "when versions don't exist" do
    describe "hyphenated_version" do
      it "returns nil" do
        expect(utils.hyphenated_version).to be_nil
      end
    end

    describe "versions_str" do
      it "returns an empty string" do
        expect(utils.versions_str).to be_empty
      end
    end
  end

  context "when release name exists" do
    let(:release_name) { "My Release" }

    describe "release_name_hyphen" do
      it "returns a hyphenized release name" do
        expect(utils.release_name_hyphen).to eq "my-release"
      end
    end
  end

  context "when release name doesn't exist" do
    describe "release_name_hyphen" do
      it "returns nil" do
        expect(utils.release_name_hyphen).to be_nil
      end
    end
  end
end
