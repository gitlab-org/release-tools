# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::BlogMergeRequest do
  let(:project) { ReleaseTools::Project::WWWGitlabCom }
  let(:versions_str) { '9.1.3' }
  let(:hyphenated_version) { '9-1-3' }
  let(:source_branch) { "create-#{hyphenated_version}-post" }
  let(:blog_file_path) { "sites/uncategorized/source/releases/posts/#{blog_post_filename}" }
  let(:blog_post_filename) { "#{Date.current + 1.day}-patch-release-gitlab-#{hyphenated_version}-released.html.md" }
  let(:security_fixes) { [] }
  let(:critical_patch_release?) { false }

  let(:patch_content) do
    [
      {
        version: ReleaseTools::Version.new('9.1.3'),
        pressure: 1,
        merge_requests: {
          'gitlab-org/gitlab' => [],
          'gitlab-org/gitaly' => [],
          'gitlab-org/omnibus-gitlab' => [{ 'title' => 'baz', 'web_url' => 'https://baz.com' }]
        }
      }
    ]
  end

  let(:patch_coordinator) do
    instance_spy(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content, pressure: 2, single_version?: false)
  end

  before do
    allow(ReleaseTools::SharedStatus)
      .to receive(:critical_patch_release?)
      .and_return(critical_patch_release?)
  end

  around do |example|
    Timecop.freeze(Time.utc(2023, 10, 26, 15, 0, 0), &example)
  end

  subject(:merge_request) do
    described_class.new(patch_coordinator: patch_coordinator, security_fixes: security_fixes)
  end

  shared_context 'security content' do
    let(:cves_issue1) { build(:issue, title: 'cve issue 1', cvss_base_score: 2, cvss_severity: 'Low', valid_yaml?: true) }
    let(:cves_issue2) { build(:issue, title: 'cve issue 2', cvss_base_score: 5, cvss_severity: 'High', valid_yaml?: true) }
    let(:cves_issue3) { build(:issue, title: 'cve issue 3', cvss_base_score: 0.0, cvss_severity: 'None', valid_yaml?: true) }
    let(:cves_issue4) { build(:issue, title: 'cve issue 4', cvss_base_score: 6, cvss_severity: 'High', valid_yaml?: true) }
    let(:issue1) { build(:issue, cves_issue: cves_issue1, title: 'sec issue 1') }
    let(:issue2) { build(:issue, cves_issue: nil, issue: double('Issue', title: 'sec issue 3')) }
    let(:issue3) { build(:issue, cves_issue: cves_issue2, title: 'sec issue 2') }
    let(:issue4) { build(:issue, cves_issue: cves_issue3, title: 'sec issue 3') }
    let(:issue5) { build(:issue, cves_issue: cves_issue4, title: 'sec issue 4', project_id: ReleaseTools::Project::OmnibusGitlab.security_id) }

    let(:security_fixes) do
      [issue1, issue2, issue3, issue4, issue5]
    end
  end

  shared_context 'multi patch blog content' do
    let(:patch_content) do
      [
        {
          version: ReleaseTools::Version.new('9.2.2'),
          pressure: 2,
          merge_requests: {
            'gitlab-org/gitlab' => [
              { 'title' => 'foo', 'web_url' => 'https://foo.com' },
              { 'title' => 'bar', 'web_url' => 'https://bar.com' }
            ],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => []
          }
        },
        {
          version: ReleaseTools::Version.new('9.1.3'),
          pressure: 1,
          merge_requests: {
            'gitlab-org/gitlab' => [],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => [{ 'title' => 'baz', 'web_url' => 'https://baz.com' }]
          }
        },
        {
          version: ReleaseTools::Version.new('9.0.4'),
          pressure: 1,
          merge_requests: {
            'gitlab-org/gitlab' => [{ 'title' => 'foo', 'web_url' => 'https://foo.com' }],
            'gitlab-org/gitaly' => [],
            'gitlab-org/omnibus-gitlab' => []
          }
        }
      ]
    end
  end

  it_behaves_like 'issuable #initialize'

  describe '#create' do
    context 'with dry run mode' do
      include_context 'multi patch blog content'
      include_context 'security content'

      it 'does not commit' do
        allow(merge_request)
          .to receive(:security_issue_blog_content)
          .and_return('Description')

        expect(ReleaseTools::GitlabClient).not_to receive(:find_or_create_branch)
        expect(ReleaseTools::GitlabClient).not_to receive(:create_commit)
        expect(ReleaseTools::GitlabClient).not_to receive(:create_merge_request)

        subject.create
      end
    end

    context 'multi version with bug and security fixes' do
      include_context 'multi patch blog content'
      include_context 'security content'

      let(:versions_str) { '9.2.2, 9.1.3, 9.0.4' }
      let(:hyphenated_version) { '9-2-2' }

      it_behaves_like 'issuable #create', :create_merge_request do
        let(:project_path) { project.security_path }

        let(:commit_actions) do
          [{
            action: 'create',
            file_path: blog_file_path,
            content: File.read('spec/fixtures/merge_requests/patch_release_blog_post.html.md')
          }]
        end

        before do
          allow(ReleaseTools::GitlabClient)
            .to receive(:find_or_create_branch)
            .with(source_branch, project.default_branch, project_path)

          allow(ReleaseTools::GitlabClient)
            .to receive(:create_commit)
            .with(project_path, source_branch, "Adding #{versions_str} blog post", commit_actions)
            .and_return(instance_double(Gitlab::ObjectifiedHash))

          allow(merge_request)
            .to receive(:security_issue_blog_content)
            .and_return('Description')
        end

        around do |ex|
          without_dry_run do
            ClimateControl.modify(USER: 'user1') { ex.run }
          end
        end
      end
    end

    context 'with one version' do
      let(:blog_post_filename) { "#{Date.current}-gitlab-#{hyphenated_version}-released.html.md" }
      let(:versions_str) { '9.1.3' }
      let(:hyphenated_version) { '9-1-3' }

      let(:patch_coordinator) do
        instance_spy(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content, pressure: 2, single_version?: true)
      end

      it_behaves_like 'issuable #create', :create_merge_request do
        let(:project_path) { project.path }

        let(:commit_actions) do
          [{
            action: 'create',
            file_path: blog_file_path,
            content: File.read('spec/fixtures/merge_requests/single_patch_blog_merge_request_template.html.md')
          }]
        end

        before do
          allow(ReleaseTools::GitlabClient)
            .to receive(:find_or_create_branch)
            .with(source_branch, project.default_branch, project_path)

          allow(ReleaseTools::GitlabClient)
            .to receive(:create_commit)
            .with(project_path, source_branch, "Adding #{versions_str} blog post", commit_actions)
            .and_return(instance_double(Gitlab::ObjectifiedHash))
        end

        around do |ex|
          without_dry_run do
            ClimateControl.modify(USER: 'user1') { ex.run }
          end
        end
      end
    end
  end

  it 'has an informative Draft title', :aggregate_failures do
    expect(merge_request.title).to eq "Draft: Adding #{versions_str} blog post"
  end

  describe '#labels' do
    it 'are set correctly on the MR' do
      expect(merge_request.labels).to eq 'patch release post'
    end
  end

  describe '#source_branch' do
    it 'adds the patch version to branch name' do
      expect(merge_request.source_branch).to eq source_branch
    end
  end

  describe '#target_branch' do
    it 'sets target_branch to the default branch' do
      expect(merge_request.target_branch).to eq project.default_branch
    end
  end

  describe '#assignee_ids' do
    let(:schedule) do
      instance_spy(
        ReleaseTools::ReleaseManagers::Schedule,
        active_release_managers: [
          double('user1', id: 1),
          double('user2', id: 2)
        ],
        active_appsec_release_managers: [
          double('user3', id: 3),
          double('user4', id: 4)
        ]
      )
    end

    before do
      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)
    end

    it 'returns the current RM user IDs' do
      expect(merge_request.assignee_ids).to eq([1, 2])
    end

    context 'with security changes' do
      let(:security_fixes) { %w[foo bar] }

      it 'only assigns the current appsec RM user IDs' do
        expect(merge_request.assignee_ids).to eq([3, 4])
      end
    end
  end

  describe '#description' do
    before do
      issue = instance_spy(ReleaseTools::PatchRelease::Issue, url: 'https://dummy-issue.url')

      allow(ReleaseTools::PatchRelease::Issue)
        .to receive(:new)
        .and_return(issue)

      allow(patch_coordinator)
        .to receive(:single_version?)
        .and_return(true)
    end

    it 'includes a link to the release issue' do
      without_dry_run do
        expect(merge_request.description).to include 'https://dummy-issue.url'
      end
    end

    it 'explains that the MR is adding a blog post for the patch release' do
      expect(merge_request.description).to include "Add blog post for #{versions_str} patch release."
    end
  end

  describe '#patch_issue_url' do
    it 'finds patch issue and returns url' do
      allow(patch_coordinator)
        .to receive(:single_version?)
        .and_return(true)

      issue = ReleaseTools::PatchRelease::Issue.new

      allow(ReleaseTools::PatchRelease::Issue)
        .to receive(:new)
        .and_return(issue)

      allow(issue)
        .to receive(:remote_issuable)
        .and_return(build(:issue, web_url: 'https://dummy-issue.url'))

      without_dry_run do
        expect(merge_request.patch_issue_url).to eq('https://dummy-issue.url')
      end
    end

    context 'with multiple versions' do
      it 'returns nothing' do
        without_dry_run do
          expect(merge_request.patch_issue_url).to be_nil
        end
      end
    end

    context 'with dry-run' do
      it 'returns a fake url' do
        expect(merge_request.patch_issue_url).to eq('test.gitlab.com')
      end
    end
  end

  describe '#blog_post_filename' do
    context 'with bug and security fixes' do
      include_context 'security content'

      it 'returns patch release filename' do
        expect(merge_request.blog_post_filename)
          .to eq("#{Date.current + 1.day}-patch-release-gitlab-9-1-3-released.html.md")
      end
    end

    context 'with a single version' do
      it 'returns blog post filename' do
        expect(merge_request.blog_post_filename).to eq("#{Date.current}-gitlab-9-1-3-released.html.md")
      end
    end
  end

  describe '#metadata_canonical_path' do
    context 'with bug and security content' do
      include_context 'security content'

      it 'returns the canonical path' do
        expect(merge_request.metadata_canonical_path)
          .to eq('/releases/2023/10/27/patch-release-gitlab-9-1-3-released/')
      end
    end

    context 'with a single version' do
      let(:patch_coordinator) do
        instance_spy(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content, single_version?: true)
      end

      it 'returns nil' do
        expect(merge_request.metadata_canonical_path).to be_nil
      end
    end
  end

  describe '#security_issue_header' do
    let(:cves_issue) { build(:issue, title: 'bar') }
    let(:security_implementation_issue) do
      build(:issue, cves_issue: cves_issue, issue: double(:issue, title: 'foo'))
    end

    subject(:header) { merge_request.security_issue_header(security_implementation_issue) }

    it 'uses the cves_issue title' do
      expect(header).to eq(cves_issue.title)
    end

    context 'when there is no cves_issue' do
      let(:cves_issue) { nil }

      it 'uses the implementation issue description' do
        expect(header).to eq(security_implementation_issue.issue.title)
      end
    end
  end

  describe '#generate_blog_content' do
    around do |ex|
      ClimateControl.modify(USER: 'user1') { ex.run }
    end

    context 'with bug and security content' do
      include_context 'multi patch blog content'
      include_context 'security content'

      it 'returns blog post with bug and security content' do
        allow(merge_request)
          .to receive(:security_issue_blog_content)
          .and_return('Description')

        expected_content = File
          .read('spec/fixtures/merge_requests/patch_release_blog_post.html.md')

        expect(merge_request.generate_blog_content).to eq(expected_content)
      end
    end

    context 'with a single version' do
      include_context 'security content'

      let(:patch_coordinator) do
        instance_spy(
          ReleaseTools::PatchRelease::Coordinator,
          merge_requests: patch_content,
          pressure: 2,
          single_version?: true
        )
      end

      it 'returns blog post for a single version' do
        expected_content = File
          .read('spec/fixtures/merge_requests/single_patch_blog_merge_request_template.html.md')

        expect(merge_request.generate_blog_content).to eq(expected_content)
      end
    end
  end

  describe '#security_issue_blog_content' do
    let(:todo_response) { 'TODO: add description for this issue' }
    let(:valid_yaml) { true }
    let(:cves_issue) do
      instance_double(
        ReleaseTools::Security::CvesIssue,
        vulnerability_description: 'A bug.',
        cvss_severity: 'low',
        cvss_string: 'asdf',
        cvss_base_score: '1.0',
        credit: 'foo',
        cve_id: '1a',
        valid_yaml?: valid_yaml
      )
    end

    let(:implemenation_issue) do
      build(:issue, cves_issue: cves_issue, issue: build(:issue, title: 'foo'))
    end

    subject { merge_request.security_issue_blog_content(implemenation_issue) }

    it 'returns the security content' do
      expected_response = <<~STR
        A bug.
        This is a low severity issue ([`asdf`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=asdf), 1.0).
        It is now mitigated in the latest release and is assigned [1a](https://cve.mitre.org/cgi-bin/cvename.cgi?name=1a).

        foo.
      STR

      expect(subject).to eq(expected_response)
    end

    context 'with no cve issue' do
      let(:cves_issue) { nil }

      it 'returns TODO text' do
        expect(subject).to include(todo_response)
      end
    end

    context 'with invalid CVE YAML' do
      let(:valid_yaml) { false }

      it 'returns TODO text' do
        expect(subject).to include(todo_response)
      end
    end
  end

  describe '#cves_id_link' do
    let(:cve_id) { '1a' }
    let(:cves_issue) { instance_double(ReleaseTools::Security::CvesIssue, cve_id: cve_id) }

    subject { merge_request.cves_id_link(cves_issue) }

    it 'returns a string with the cve link' do
      expect(subject).to eq("It is now mitigated in the latest release and is assigned [#{cve_id}](https://cve.mitre.org/cgi-bin/cvename.cgi?name=#{cve_id}).")
    end

    context 'with no cve_id' do
      let(:cve_id) { '' }

      it 'returns alternate text' do
        expect(subject).to eq("We have requested a CVE ID and will update this blog post when it is assigned.")
      end
    end
  end

  describe '#security_issue_slug' do
    include_context 'security content'

    it 'returns the sluggified cves_issue title' do
      expect(merge_request.security_issue_slug(security_fixes.first)).to eq('cve-issue-1')
    end

    it 'handles when special characters are included' do
      allow(merge_request).to receive(:security_issue_header).and_return('foo  a.b/c 12-3 - 4')

      expect(merge_request.security_issue_slug('issue')).to eq('foo--abc-12-3---4')
    end
  end

  describe '#sorted_security_content' do
    include_context 'security content'

    it 'sorts security issues by the cvss_base_score with nil values last' do
      expect(merge_request.sorted_security_content).to eq([issue5, issue3, issue1, issue4, issue2])
    end
  end

  describe '#sorted_table_content' do
    include_context 'security content'

    it 'removes the NON_TABLE_OF_FIXES_PROJECTS issues' do
      expect(merge_request.sorted_table_content).to eq([issue3, issue1, issue4, issue2])
    end
  end

  describe '#displayed_severity' do
    include_context 'security content'

    it 'returns TODO when the severity is None' do
      expect(merge_request.displayed_severity(issue3)).to eq('High')
      expect(merge_request.displayed_severity(issue4)).to eq('TODO')
    end
  end

  describe '#blog_metadata_title' do
    include_context 'security content'

    it 'returns the merge request title' do
      expect(merge_request.blog_metadata_title)
        .to eq('GitLab Patch Release: 9.1.3')
    end

    context 'critical patch release' do
      let(:critical_patch_release?) { true }

      it 'includes critical in the title' do
        expect(merge_request.blog_metadata_title)
          .to eq('GitLab Critical Patch Release: 9.1.3')
      end
    end
  end

  describe '#blog_metadata_description' do
    include_context 'security content'

    it 'returns the merge request description' do
      expect(merge_request.blog_metadata_description)
        .to eq('Learn more about GitLab Patch Release: 9.1.3 for GitLab Community Edition (CE) and Enterprise Edition (EE).')
    end

    context 'critical patch release' do
      let(:critical_patch_release?) { true }

      it 'includes critical in the description' do
        expect(merge_request.blog_metadata_description)
          .to eq('Learn more about GitLab Critical Patch Release: 9.1.3 for GitLab Community Edition (CE) and Enterprise Edition (EE).')
      end
    end
  end

  describe '#blog_metadata_tags' do
    context 'with bug and security content' do
      include_context 'security content'

      it 'returns security as metadata' do
        expect(merge_request.blog_metadata_tags).to eq('security')
      end
    end

    context 'with a single version' do
      let(:patch_coordinator) do
        instance_spy(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content, pressure: 2, single_version?: true)
      end

      it 'returns no metadata tags' do
        expect(merge_request.blog_metadata_tags).to be_nil
      end
    end
  end

  describe '#exist?' do
    context 'with security content' do
      include_context 'security content'

      it 'verifies the presence of the security blog post' do
        allow(ReleaseTools::GitlabClient)
          .to receive(:security_blog_merge_request)
          .and_return(create(:merge_request))

        expect(merge_request).to exist
      end
    end

    context 'with non-security content' do
      it 'checks the presence of remote_issuable' do
        expect(ReleaseTools::GitlabClient)
          .to receive(:merge_requests)
          .and_return([])

        expect(merge_request).not_to exist
      end
    end
  end

  describe '#security_fixes?' do
    context 'with security fixes' do
      let(:security_fixes) { [build(:issue), build(:issue)] }

      it 'indicates if security fixes are present' do
        expect(merge_request).to be_security_fixes
      end
    end

    context 'without security fixes' do
      let(:security_fixes) { [] }

      it 'indicates security fixes are not present' do
        expect(merge_request).not_to be_security_fixes
      end
    end
  end

  describe '#bug_fixes?' do
    before do
      allow(patch_coordinator)
        .to receive(:pressure)
        .and_return(pressure)
    end

    context 'with positive patch pressure' do
      let(:pressure) { 2 }

      it 'indicates the presence of bug fixes' do
        expect(merge_request).to be_bug_fixes
      end
    end

    context 'with negative patch pressure' do
      let(:pressure) { 0 }

      it 'indicates the lack of bug fixes' do
        expect(merge_request).not_to be_bug_fixes
      end
    end
  end
end
