# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Monthly::ReleaseDay::NotifyReleasePublish do
  subject(:notify_release_publish) { described_class.new }

  let(:logger) { instance_double(SemanticLogger::Logger) }
  let(:version) { instance_double(ReleaseTools::Version) }
  let(:active_version) { '1.2.3' }

  before do
    allow(GitlabReleases).to receive(:active_version).and_return(active_version)

    allow(logger).to receive(:info)
    allow(notify_release_publish).to receive(:logger).and_return(logger)
    allow(ReleaseTools::Slack::Message).to receive(:post)
  end

  describe '#execute' do
    it 'logs release notification messages' do
      expect(logger).to receive(:info).with("Notifying that the release packages for #{active_version} will be published")

      notify_release_publish.execute
    end

    it 'posts release notification message to Slack' do
      expect(ReleaseTools::Slack::Message).to receive(:post).with(
        channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
        message: ":mega: Packages for #{active_version} are built and will be published at 13:10 UTC"
      )

      notify_release_publish.execute
    end
  end
end
