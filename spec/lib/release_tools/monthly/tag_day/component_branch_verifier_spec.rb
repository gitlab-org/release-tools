# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Monthly::TagDay::ComponentBranchVerifier do
  describe '#execute' do
    subject(:component_branch_verifier) { described_class.new }

    let(:monthly_version) { ReleaseTools::Version.new("15.9") }

    before do
      allow(GitlabReleases).to receive(:active_version).and_return(monthly_version)
    end

    it 'calls the BranchStatusService class with correct parameters' do
      expect(ReleaseTools::Services::BranchesStatusService)
        .to receive(:new)
        .with(release_type: :monthly, versions: ["15.9.0"])
        .and_return(instance_double(ReleaseTools::Services::BranchesStatusService, execute: 'foo'))

      component_branch_verifier.execute
    end
  end
end
