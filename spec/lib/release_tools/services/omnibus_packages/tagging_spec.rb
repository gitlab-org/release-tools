# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::OmnibusPackages::Tagging do
  packages_status_service = ReleaseTools::Services::PackagesStatus::Functionality

  context 'with tag package type' do
    it_behaves_like 'check omnibus packages service',
                    packages_status_service,
                    :tag
  end

  context 'with internal package type' do
    it_behaves_like 'check omnibus packages service',
                    packages_status_service,
                    :internal
  end
end
