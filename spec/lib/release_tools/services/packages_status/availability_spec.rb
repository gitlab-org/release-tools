# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::PackagesStatus::Availability do
  it_behaves_like 'packages status service', 'check-packages-availability'
end
