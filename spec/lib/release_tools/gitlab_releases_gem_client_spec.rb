# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::GitlabReleasesGemClient do
  subject(:client) { described_class }

  describe '.active_version' do
    it 'calls releases method' do
      expect(GitlabReleases).to receive(:active_version)

      client.active_version
    end
  end

  describe '.current_version' do
    it 'calls releases method' do
      expect(GitlabReleases).to receive(:current_version)

      client.current_version
    end
  end

  describe '.version_for_date' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:version_for_date)

      client.version_for_date(Date.today)
    end
  end

  describe '.upcoming_releases' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:upcoming_releases)

      client.upcoming_releases
    end
  end

  describe '.previous_minors' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:previous_minors)

      client.previous_minors('16.1')
    end
  end

  describe '.current_minor_for_date' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:current_minor_for_date)

      client.current_minor_for_date(Date.today)
    end
  end

  describe '.next_versions' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:next_versions)

      client.next_versions
    end
  end

  describe '.next_patch_release_date' do
    it 'calls a releases method' do
      expect(GitlabReleases).to receive(:next_patch_release_date)

      client.next_patch_release_date
    end
  end
end
