# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::EnvironmentState::Production do
  environment = 'gprd'
  stage = 'main'

  include_context 'environment state transition context', environment, stage
  it_behaves_like 'environment state transition', environment, stage # Stubs request to Prometheus for `auto_deploy_environment_state` metric

  describe '#perform_event' do
    it_behaves_like 'staging and production'
  end
end
