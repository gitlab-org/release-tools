# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::AutoDeployBuildingPackageState do
  subject(:service) { described_class.new }

  let(:delivery_metrics) { instance_double(ReleaseTools::Metrics::Client) }

  let(:gstg_cny_version) do
    build(:product_version, auto_deploy_ref: '16.10.202402100400-abdf1234567.abcdef12345')
  end

  let(:version) { '16.10.202402200600-f54d5e31aa4.5527995deba' }

  let(:deployments) { [build(:deployment, :success, sha: 'abcd1234')] }

  let(:cng_version) { '16.10.202402100200+f54d5e31aa4' }
  let(:omnibus_version) { '16.10.202402100200+f54d5e31aa4.5527995deba' }

  let(:omnibus_running_pipelines) { Gitlab::PaginatedResponse.new([build(:pipeline, :running, ref: omnibus_version)]) }
  let(:cng_running_pipelines) { Gitlab::PaginatedResponse.new([build(:pipeline, :running, ref: cng_version)]) }

  let(:omnibus_failed_pipelines) { Gitlab::PaginatedResponse.new([build(:pipeline, :failed, ref: omnibus_version)]) }
  let(:cng_success_pipelines) { Gitlab::PaginatedResponse.new([build(:pipeline, :success, ref: cng_version)]) }

  before do
    allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(delivery_metrics)

    allow(ReleaseTools::GitlabOpsClient)
      .to receive(:deployments)
      .with(
        ReleaseTools::Project::Release::Metadata,
        'gstg-cny',
        hash_including({ status: 'success', order_by: 'id', sort: 'desc', opts: { per_page: 1 } })
      )
      .and_return(deployments)

    allow(ReleaseTools::ProductVersion).to receive(:from_metadata_sha).with('abcd1234').and_return(gstg_cny_version)

    allow(ReleaseTools::GitlabDevClient).to receive(:pipelines).and_return(Gitlab::PaginatedResponse.new([]))

    allow(ReleaseTools::GitlabDevClient)
      .to receive(:pipelines)
      .with(
        ReleaseTools::Project::OmnibusGitlab,
        hash_including({ name: "AUTO_DEPLOY_BUILD_PIPELINE", status: "running" })
      )
      .and_return(omnibus_running_pipelines)

    allow(ReleaseTools::GitlabDevClient)
      .to receive(:pipelines)
      .with(
        ReleaseTools::Project::CNGImage,
        hash_including({ name: "AUTO_DEPLOY_BUILD_PIPELINE", status: "running" })
      )
      .and_return(cng_running_pipelines)

    allow(ReleaseTools::GitlabDevClient)
      .to receive(:pipelines)
      .with(
        ReleaseTools::Project::CNGImage,
        hash_including({ name: "AUTO_DEPLOY_BUILD_PIPELINE", status: "success" })
      )
      .and_return(cng_success_pipelines)

    allow(ReleaseTools::GitlabDevClient)
      .to receive(:pipelines)
      .with(
        ReleaseTools::Project::OmnibusGitlab,
        hash_including({ name: "AUTO_DEPLOY_BUILD_PIPELINE", status: "failed" })
      )
      .and_return(omnibus_failed_pipelines)
  end

  around do |tests|
    ClimateControl.modify(AUTO_DEPLOY_TAG: version, &tests)
  end

  shared_examples 'does not set metric when current package is older than gstg-cny' do
    let(:gstg_cny_version) do
      build(:product_version, auto_deploy_ref: version)
    end

    it 'does not set metric' do
      expect(delivery_metrics)
        .not_to receive(:set)
        .with(
          'auto_deploy_building_package_state',
          1,
          anything
        )

      execute
    end
  end

  describe '#package_building' do
    subject(:execute) { service.package_building }

    it_behaves_like 'does not set metric when current package is older than gstg-cny'

    it 'sets metric for running state' do
      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 1, { labels: "running" })

      execute
    end
  end

  describe '#package_built' do
    subject(:execute) { service.package_built }

    it_behaves_like 'does not set metric when current package is older than gstg-cny'

    it 'sets metric to success state and unsets running and failed states' do
      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 1, { labels: "success" })

      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 0, { labels: "running" })

      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 0, { labels: "failed" })

      execute
    end

    context 'when there are newer pipelines running and failed' do
      let(:cng_version) { '16.10.202402200600+f54d5e31aa4' }
      let(:omnibus_version) { '16.10.202402200600+f54d5e31aa4.5527995deba' }

      it 'does not unset running and failed state metrics' do
        expect(delivery_metrics)
          .not_to receive(:set)
          .with('auto_deploy_building_package_state', 0, anything)

        execute
      end
    end
  end

  describe '#package_building_failed' do
    subject(:execute) { service.package_building_failed }

    it_behaves_like 'does not set metric when current package is older than gstg-cny'

    it 'sets metric to failed state and unsets running state' do
      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 1, { labels: "failed" })

      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 0, { labels: "running" })

      execute
    end

    context 'when there are newer pipelines running' do
      let(:cng_version) { '16.10.202402200600+f54d5e31aa4' }
      let(:omnibus_version) { '16.10.202402200600+f54d5e31aa4.5527995deba' }

      it 'does not unset running state metric' do
        expect(delivery_metrics)
          .not_to receive(:set)
          .with('auto_deploy_building_package_state', 0, { labels: "running" })

        execute
      end
    end
  end

  describe '#deployment_completed' do
    subject(:execute) { service.deployment_completed }

    it_behaves_like 'does not set metric when current package is older than gstg-cny'

    it 'unsets running, failed and success state metrics' do
      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 0, { labels: "running" })

      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 0, { labels: "failed" })

      expect(delivery_metrics)
        .to receive(:set)
        .with('auto_deploy_building_package_state', 0, { labels: "success" })

      execute
    end

    context 'when there are newer pipelines in running, success and failed states' do
      let(:cng_version) { '16.10.202402200600+f54d5e31aa4' }
      let(:omnibus_version) { '16.10.202402200600+f54d5e31aa4.5527995deba' }

      it 'does not unset running, success and failed state metrics' do
        expect(delivery_metrics)
          .not_to receive(:set)
          .with('auto_deploy_building_package_state', 0, anything)

        execute
      end
    end
  end
end
