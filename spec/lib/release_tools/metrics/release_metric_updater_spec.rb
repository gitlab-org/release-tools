# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::ReleaseMetricUpdater do
  subject(:updater) { described_class.new }

  let(:delivery_prometheus) { instance_double(ReleaseTools::Prometheus::Query) }
  let(:active_version) { "16.10" }
  let(:next_patch_release_date) { "2024-02-15" }
  let(:current_minor_for_date) { "16.10" }
  let(:previous_minors) { ["16.9", "16.8", "16.7"] }
  let(:current_version) { "16.9" }

  describe '#execute' do
    before do
      allow(ReleaseTools::Prometheus::Query)
        .to receive(:new)
        .and_return(delivery_prometheus)

      allow(delivery_prometheus)
        .to receive_messages(
          monthly_release_status: 3,
          patch_release_status: 1
        )

      allow(ReleaseTools::GitlabReleasesGemClient)
        .to receive_messages(
          active_version: active_version,
          current_minor_for_date: current_minor_for_date,
          current_version: current_version,
          next_patch_release_date: next_patch_release_date,
          previous_minors: previous_minors
        )

      enable_feature(:release_status_metric_update)
    end

    context 'when updating the metric' do
      it 'creates new monthly and patch release statuses' do
        expect(ReleaseTools::Metrics::MonthlyReleaseStatus).to receive(:new).with(status: :tagged_rc)
          .and_return(instance_double(ReleaseTools::Metrics::MonthlyReleaseStatus, execute: true))

        expect(ReleaseTools::Metrics::PatchReleaseStatus).to receive(:new).with(status: :open)
          .and_return(instance_double(ReleaseTools::Metrics::PatchReleaseStatus, execute: true))

        updater.execute
      end

      context 'when the monthly release status is invalid' do
        before do
          allow(delivery_prometheus)
            .to receive_messages(
              monthly_release_status: nil,
              patch_release_status: 1
            )
        end

        it 'raises an error' do
          expect { updater.execute }.to raise_error(ReleaseTools::Metrics::ReleaseMetricUpdater::InvalidReleaseStatusError)
        end
      end

      context 'when feature flag is not set' do
        before do
          disable_feature(:release_status_metric_update)
        end

        it 'does nothing' do
          expect(ReleaseTools::Metrics::MonthlyReleaseStatus)
            .not_to receive(:new)

          expect(ReleaseTools::Metrics::PatchReleaseStatus)
            .not_to receive(:new)

          updater.execute
        end
      end

      context 'when a new monthly release became available today' do
        let(:tomorrow_active_version) { "16.11" }
        let(:current_version) { "16.10" }

        before do
          allow(ReleaseTools::GitlabReleasesGemClient)
            .to receive(:version_for_date)
            .and_return(tomorrow_active_version)

          allow(delivery_prometheus)
            .to receive(:monthly_release_status)
            .with(tomorrow_active_version)
            .and_return(1)
        end

        it 'creates a new open monthly release status for the next minor version' do
          expect(ReleaseTools::Metrics::MonthlyReleaseStatus).to receive(:new).with(status: :open)
            .and_return(instance_double(ReleaseTools::Metrics::MonthlyReleaseStatus, execute: true))

          updater.execute
        end
      end
    end
  end
end
