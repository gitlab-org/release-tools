# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockersMetrics do
  subject(:deployment_blocker) { described_class.new }

  let(:local_time) { Time.utc(2023, 4, 1, 0, 0, 0) }

  let(:client) { instance_double(ReleaseTools::Metrics::Client) }
  let(:fetcher) { instance_double(ReleaseTools::Deployments::BlockerIssueFetcher) }
  let(:calculator) { instance_double(ReleaseTools::Deployments::BlockersCalculator) }

  before do
    allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(client)
    allow(ReleaseTools::Deployments::BlockerIssueFetcher).to receive(:new).and_return(fetcher)
    allow(ReleaseTools::Deployments::BlockersCalculator).to receive(:new).and_return(calculator)
  end

  describe '#execute' do
    context 'when the feature flag is disabled' do
      before do
        disable_feature(:deployment_blockers_metrics)
      end

      it 'does nothing' do
        expect(fetcher).not_to receive(:fetch)
        expect(calculator).not_to receive(:failure_types)

        deployment_blocker.execute
      end
    end

    context 'when the feature flag is enabled' do
      before do
        enable_feature(:deployment_blockers_metrics)
      end

      context 'when creating a new metric for the deployment blockers' do
        let(:root_cause_list) do
          [
            "RootCause::Broken-Security-Mirror",
            "RootCause::Config-Change",
            "RootCause::DB-Migration",
            "RootCause::Dev-Instance",
            "RootCause::External-Dependency",
            "RootCause::Feature-Flag",
            "RootCause::Flaky-Test",
            "RootCause::Indeterminate",
            "RootCause::Known-Software-Issue",
            "RootCause::Native-Traffic",
            "RootCause::Needed",
            "RootCause::Ops-Instance",
            "RootCause::Release-Compatibility",
            "RootCause::Saturation",
            "RootCause::Software-Change",
            "RootCause::SPoF"
          ]
        end

        let(:blocker_types) do
          {
            "RootCause::Flaky-Test" => { gstg: 10.0, gprd: 5.0, count: 3 },
            "RootCause::Feature-Flag" => { gstg: 20.0, gprd: 15.0, count: 2 }
          }
        end

        before do
          allow(fetcher).to receive(:fetch)
          allow(fetcher).to receive_messages(deployment_blockers: [], start_time: local_time)
          allow(calculator).to receive_messages(failure_types: blocker_types)
        end

        it 'calculates and sets metrics for each blocker type' do
          Timecop.freeze(local_time) do
            root_cause_list.each do |root_cause|
              blocked_time = blocker_types[root_cause] || { gprd: 0, gstg: 0, count: 0 }

              expect(client).to receive(:set).with(
                'deployment_blocker_count',
                blocked_time[:count],
                labels: "#{root_cause},2023-04-01"
              )

              expect(client).to receive(:set).with(
                'deployment_hours_blocked',
                blocked_time[:gprd],
                labels: "#{root_cause},gprd,2023-04-01"
              )

              expect(client).to receive(:set).with(
                'deployment_hours_blocked',
                blocked_time[:gstg],
                labels: "#{root_cause},gstg,2023-04-01"
              )
            end

            deployment_blocker.execute
          end
        end
      end
    end
  end
end
