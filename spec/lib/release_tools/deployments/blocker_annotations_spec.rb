# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockerAnnotations do
  subject(:blocker_annotations) { described_class.new }

  let(:blockers) do
    create_list(:issue, 2).map do |issue|
      ReleaseTools::Deployments::BlockerIssue.new(issue)
    end
  end

  let(:non_blocker) do
    ReleaseTools::Deployments::BlockerIssue.new(create(:issue))
  end

  let(:fetcher) do
    instance_spy(ReleaseTools::Deployments::BlockerIssueFetcher)
  end

  before do
    allow(ReleaseTools::Deployments::BlockerIssueFetcher)
      .to receive(:new)
      .and_return(fetcher)

    allow(fetcher)
      .to receive(:fetch)

    blockers.each do |blocker|
      allow(blocker)
        .to receive(:need_to_annotate?)
        .and_return(true)
    end

    allow(non_blocker)
      .to receive(:need_to_annotate?)
      .and_return(false)

    allow(fetcher)
      .to receive(:deployment_blockers)
      .and_return(blockers.push(non_blocker))

    enable_feature(:grafana_annotations)
  end

  describe '#execute' do
    it 'calls add_grafana_annotations' do
      expect(blocker_annotations)
        .to receive(:add_grafana_annotations)

      blocker_annotations.execute
    end

    it 'only annotate valid blockers' do
      annotator =
        instance_double(ReleaseTools::Deployments::GrafanaAnnotator, execute: true)

      expect(ReleaseTools::Deployments::GrafanaAnnotator)
        .to receive(:new)
        .twice
        .and_return(annotator)

      expect(ReleaseTools::Deployments::GrafanaAnnotator)
        .not_to receive(:new)
        .with(non_blocker)

      blocker_annotations.execute
    end

    context 'when the feature flag is disabled' do
      before do
        disable_feature(:grafana_annotations)
      end

      it 'does not call add_grafana_annotations' do
        expect(blocker_annotations)
          .not_to receive(:add_grafana_annotations)

        blocker_annotations.execute
      end
    end
  end
end
