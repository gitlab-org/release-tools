# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::GitlabOperator::VersionFinder do
  let(:client) { class_spy(ReleaseTools::GitlabClient) }
  let(:finder) { described_class.new(client) }
  let(:tags) do
    {
      '0.6.1' => %w[5.9.1 5.8.4 5.7.4],
      '0.5.1' => %w[5.8.2 5.7.4 5.6.5],
      '0.4.1' => %w[5.7.1 5.6.4 5.5.4]
    }
  end

  describe '#execute' do
    before do
      allow(finder).to receive(:release_tags).and_return(tags)
    end

    context 'when an operator tag already exists for corresponding chart_version' do
      it 'returns the latest existing operator version which supports specified chart_version' do
        expect(finder.execute(ReleaseTools::Version.new('5.7.4'))).to eq(ReleaseTools::Version.new('0.6.1'))
      end
    end

    context 'when chart version is a patch release' do
      it 'returns the next patch release of latest operator version' do
        expect(finder.execute(ReleaseTools::Version.new('5.0.5'))).to eq(ReleaseTools::Version.new('0.6.2'))
      end
    end

    context 'when chart version is a minor release' do
      it 'returns the next minor release of latest operator version' do
        expect(finder.execute(ReleaseTools::Version.new('5.1.0'))).to eq(ReleaseTools::Version.new('0.7.0'))
      end
    end

    context 'when chart version is a major release' do
      it 'returns the next major release after latest operator version' do
        expect(finder.execute(ReleaseTools::Version.new('6.0.0'))).to eq(ReleaseTools::Version.new('1.0.0'))
      end
    end
  end

  describe '#release_tags' do
    it 'returns the tags and their GitLab versions' do
      tag1 = double(:tag, name: '0.6.1', message: 'supports GitLab Charts 5.9.1, 5.8.4, 5.7.5')
      tag2 = double(:tag, name: '0.5.1', message: 'supports GitLab Charts 5.8.2, 5.7.4, 5.6.5')
      tag3 = double(:tag, name: '0.4.1', message: 'supports GitLab Charts 5.7.1, 5.6.4, 5.5.4')
      page = Gitlab::PaginatedResponse.new([tag1, tag2, tag3])

      allow(client)
        .to receive(:tags)
        .with(ReleaseTools::Project::GitlabOperator)
        .and_return(page)

      expect(finder.release_tags).to eq(
        {
          '0.6.1' => %w[5.9.1 5.8.4 5.7.5],
          '0.5.1' => %w[5.8.2 5.7.4 5.6.5],
          '0.4.1' => %w[5.7.1 5.6.4 5.5.4]
        }
      )
    end
  end

  describe '#latest_version' do
    context 'when a list of versions is provided' do
      it 'returns the latest version from the list' do
        versions = %w[0.6.1 0.5.1 0.4.1]

        expect(finder.latest_version(versions)).to eq(ReleaseTools::Version.new('0.6.1'))
      end
    end

    context 'when a list of versions is not provided' do
      it 'returns the latest version from the release tags' do
        allow(finder).to receive(:release_tags).and_return(tags)

        expect(finder.latest_version).to eq(ReleaseTools::Version.new('0.6.1'))
      end
    end
  end
end
