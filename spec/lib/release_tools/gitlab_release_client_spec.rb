# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::GitlabReleaseClient do
  describe '.client' do
    let(:mock_client) { instance_double(Gitlab::Client) }

    before do
      allow(Gitlab).to receive(:client).and_return(mock_client)
      described_class.instance_variable_set(:@client, nil) # Reset memoized client
    end

    it 'creates a new Gitlab client with correct endpoint' do
      expect(Gitlab).to receive(:client).with(
        hash_including(endpoint: described_class::RELEASE_API_ENDPOINT)
      )

      described_class.client
    end

    it 'uses the RELEASE_BOT_RELEASE_TOKEN environment variable' do
      token = 'test_token'
      allow(ENV).to receive(:fetch).with('RELEASE_BOT_RELEASE_TOKEN', nil).and_return(token)

      expect(Gitlab).to receive(:client).with(
        hash_including(private_token: token)
      )

      described_class.client
    end

    it 'includes httparty options' do
      httparty_opts = { foo: 'bar' }
      allow(described_class).to receive(:httparty_opts).and_return(httparty_opts)

      expect(Gitlab).to receive(:client).with(
        hash_including(httparty: httparty_opts)
      )

      described_class.client
    end

    it 'memoizes the client' do
      expect(Gitlab).to receive(:client).once.and_return(mock_client)

      2.times { described_class.client }
    end
  end

  describe '.current_version' do
    let(:mock_client) { instance_double(Gitlab::Client) }
    let(:version_attribute) { '17.1.2-ee' }

    before do
      allow(described_class).to receive(:client).and_return(mock_client)
      allow(mock_client).to receive(:version).and_return(
        Gitlab::ObjectifiedHash.new(
          version: version_attribute
        )
      )
    end

    it 'returns the current version' do
      expect(described_class.current_version).to eq(version_attribute)
    end
  end
end
