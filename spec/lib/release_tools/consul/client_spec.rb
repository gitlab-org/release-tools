# frozen_string_literal: true

# spec/lib/release_tools/consul/client_spec.rb

require 'spec_helper'

describe ReleaseTools::Consul::Client do
  let(:consul_host) { 'test-host' }
  let(:consul_port) { '8500' }
  let(:client) { described_class.new }

  before do
    allow(ENV).to receive(:fetch).with('CONSUL_HOST', 'localhost').and_return(consul_host)
    allow(ENV).to receive(:fetch).with('CONSUL_PORT', '8500').and_return(consul_port)
  end

  describe '#initialize' do
    it 'configures Diplomat with the correct URL' do
      expect(Diplomat).to receive(:configure) do |&block|
        config = instance_double(Diplomat::Configuration)
        expect(config).to receive(:url=).with("http://#{consul_host}:#{consul_port}")
        block.call(config)
      end

      described_class.new
    end

    context 'when environment variables are not set' do
      let(:consul_host) { 'localhost' }
      let(:consul_port) { '8500' }

      it 'uses default values' do
        expect(Diplomat).to receive(:configure) do |&block|
          config = instance_double(Diplomat::Configuration)
          expect(config).to receive(:url=).with("http://localhost:8500")
          block.call(config)
        end

        described_class.new
      end
    end
  end

  describe '#get' do
    let(:key) { 'test-key' }

    context 'when the key exists' do
      let(:value) { 'test-value' }

      it 'returns the value for the given key' do
        expect(Diplomat::Kv).to receive(:get).with(key).and_return(value)
        expect(client.get(key)).to eq(value)
      end
    end

    context 'when the key does not exist' do
      it 'raises a KeyNotFoundError' do
        expect(Diplomat::Kv).to receive(:get).with(key).and_raise(Diplomat::KeyNotFound)
        expect { client.get(key) }.to raise_error(ReleaseTools::Consul::Client::KeyNotFoundError, "The key '#{key}' was not found in Consul")
      end
    end
  end
end
