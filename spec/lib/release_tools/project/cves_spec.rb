# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::Cves do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .to_s'
  it_behaves_like 'project .security_group', 'gitlab-org'
  it_behaves_like 'project .security_path', 'gitlab-org/cves'
  it_behaves_like 'project IDs'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-org/cves' }
  end

  describe '.group' do
    it { expect(described_class.group).to eq 'gitlab-org' }
  end

  describe '.canonical_id' do
    it 'returns the canonical id' do
      expect(described_class.canonical_id).to eq(described_class::IDS.fetch(:canonical))
    end

    context 'without a remote defined' do
      it 'raises an error' do
        expect do
          described_class.ops_id
        end.to raise_error('Invalid remote for gitlab-org/cves: ops')
      end
    end
  end

  describe '.security_id' do
    it 'returns the security id' do
      expect(described_class.security_id).to eq(described_class::IDS.fetch(:security))
    end
  end
end
