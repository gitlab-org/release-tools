# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Mimir::Utils do
  describe '.explore_pane_url' do
    it 'returns link to Grafana explore pane' do
      result = described_class.explore_pane_url(
        query: 'pg_stat_activity_autovacuum_age_in_seconds{type="patroni-ci", relname=~".*wraparound.*"}',
        from: 'now-2h',
        to: 'now'
      )

      expect(result).to eq(
        'https://dashboards.gitlab.net/explore?schemaVersion=1&orgId=1&' \
        'panes=%7B%22abc%22%3A%7B%22datasource%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%2C%22' \
        'queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22' \
        'expr%22%3A%22pg_stat_activity_autovacuum_age_in_seconds%7Btype%3D%5C%22patroni-ci%5C%22%2C+relname%3D~%5C%22.%2Awraparound.%2A%5C%22%7D%22%2C%22' \
        'range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22' \
        'uid%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D%2C%22editorMode%22%3A%22code%22%7D%5D%2C%22' \
        'range%22%3A%7B%22from%22%3A%22now-2h%22%2C%22to%22%3A%22now%22%7D%7D%7D'
      )
    end
  end
end
