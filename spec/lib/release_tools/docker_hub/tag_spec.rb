# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::DockerHub::Tag do
  subject(:tag) { described_class.new(data) }

  let(:status) { 'active' }
  let(:name) { '16.0.0-ee.0' }

  let(:data) do
    {
      "name" => name,
      "v2" => true,
      "tag_status" => status,
      "content_type" => "image"
    }
  end

  it 'converts hash into objectified hash' do
    expect(tag.name).to eq(data["name"])
  end

  describe '#active?' do
    context 'when the tag is active' do
      it { is_expected.to be_active }
    end

    context 'when the tag is inactive' do
      let(:status) { 'inactive' }

      it { is_expected.not_to be_active }
    end
  end

  describe '#version' do
    context 'when the tag is for Enterprise Edition' do
      it 'returns the version with -ee.0 suffix' do
        expect(tag.version('12.10')).to eq('12.10-ee.0')
      end
    end

    context 'when the tag is for Community Edition' do
      let(:name) { 'gitlab-ce' }

      it 'returns the version with -ce.0 suffix' do
        expect(tag.version('12.10')).to eq('12.10-ce.0')
      end
    end
  end

  describe '#ee?' do
    context 'when the raw name includes "ee"' do
      it { is_expected.to be_ee }
    end

    context 'when the raw name does not include "ee"' do
      let(:name) { 'gitlab-ce' }

      it { is_expected.not_to be_ee }
    end
  end
end
