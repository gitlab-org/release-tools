# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::DockerHub::Verifier do
  subject(:verifier) { described_class.new(release_type: release_type, version: version) }

  let(:release_type) { 'monthly' }
  let(:version) { '16.0.0' }
  let(:client) { ReleaseTools::DockerHub::Client.new }
  let(:status) { 'active' }
  let(:name) { '16.0.0-ee.0' }
  let(:data) do
    {
      "name" => name,
      "v2" => true,
      "tag_status" => status,
      "content_type" => "image"
    }
  end
  let(:tag) { ReleaseTools::DockerHub::Tag.new(data) }
  let(:notifier) { instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier) }

  before do
    allow(ReleaseTools::DockerHub::Client).to receive(:tag).and_return(tag)
  end

  describe '#execute' do
    context 'when tags exist on Docker Hub' do
      it 'sends a success notification to Slack' do
        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new)
          .with(
            job_type: "Verify docker tags for 16.0.0",
            status: :success,
            release_type: release_type
          )
          .and_return(notifier)
        expect(notifier).to receive(:send_notification)

        without_dry_run do
          verifier.execute
        end
      end
    end
  end

  context 'when tags do not exist on Docker Hub' do
    before do
      allow(tag).to receive(:active?).and_return(false)
    end

    it 'raises an error' do
      expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new)
        .with(
          job_type: "Verify docker tags for 16.0.0",
          status: :failed,
          release_type: release_type
        )
        .and_return(notifier)
      expect(notifier).to receive(:send_notification)

      without_dry_run do
        expect { verifier.execute }.to raise_error('Tag 16.0.0-ee.0 does not exist on Docker Hub')
      end
    end
  end

  context 'when something goes wrong' do
    before do
      allow(ReleaseTools::DockerHub::Client)
      .to receive(:tag)
      .and_raise(StandardError)
    end

    it 'raises an error' do
      expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new)
        .with(
          job_type: "Verify docker tags for 16.0.0",
          status: :failed,
          release_type: release_type
        )
       .and_return(notifier)
      expect(notifier).to receive(:send_notification)
      without_dry_run do
        expect { verifier.execute }.to raise_error(StandardError)
      end
    end
  end

  context 'dry run' do
    it 'does not check the tags' do
      expect(ReleaseTools::DockerHub::Client).not_to receive(:tag)
      expect(notifier).not_to receive(:send_notification)

      verifier.execute
    end
  end
end
