# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::ReleaseMetadataUploader do
  describe '#upload' do
    let(:uploader) { described_class.new }
    let(:data) { ReleaseTools::ReleaseMetadata.new }
    let(:json) do
      JSON.pretty_generate(
        security: false,
        releases: {
          foo: {
            version: '1.2.3',
            sha: '123abc',
            ref: 'master',
            tag: false
          },
          bar: {
            version: '4.5.6',
            sha: '123abc',
            ref: 'master',
            tag: false
          },
          'gitlab-ee': {
            version: '16.1.0-internal0',
            sha: '123',
            ref: '123',
            tag: false
          }
        }
      )
    end

    before do
      data.add_release(
        name: 'foo',
        version: '1.2.3',
        sha: '123abc',
        ref: 'master',
        tag: false
      )

      data.add_release(
        name: 'bar',
        version: '4.5.6',
        sha: '123abc',
        ref: 'master',
        tag: false
      )

      data.add_release(
        name: 'gitlab-ee',
        version: '16.1.0-internal0',
        sha: '123',
        ref: '123',
        tag: false
      )
    end

    it 'does not upload anything if there are no releases' do
      expect(ReleaseTools::GitlabOpsClient).not_to receive(:create_file)

      uploader.upload('1.2.3', ReleaseTools::ReleaseMetadata.new, auto_deploy: false)
    end

    it 'uploads the release data' do
      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:get_file)
        .with(
          described_class::PROJECT,
          'releases/1/1.2.3.json',
          'master'
        )
        .and_raise(gitlab_error(:NotFound))

      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:create_file)
        .with(
          described_class::PROJECT,
          'releases/1/1.2.3.json',
          'master',
          json,
          "Add release data for 1.2.3\n\nProduct-Version: 1.2.3\nAuto-Deploy: true"
        )

      without_dry_run do
        uploader.upload('1.2.3', data, auto_deploy: true)
      end
    end

    it 'overwrites existing data when it already exists' do
      commit_msg = "Add release data for 1.2.3\n\nProduct-Version: 1.2.3\nAuto-Deploy: false"

      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:get_file)
        .with(
          described_class::PROJECT,
          'releases/1/1.2.3.json',
          'master'
        )
        .and_return(build(:gitlab_response, file_name: '1.2.3.json'))

      expect(ReleaseTools::GitlabOpsClient).not_to receive(:create_file)

      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:edit_file)
        .with(
          described_class::PROJECT,
          'releases/1/1.2.3.json',
          'master',
          json,
          commit_msg
        )

      without_dry_run do
        uploader.upload('1.2.3', data, auto_deploy: false)
      end
    end
  end
end
