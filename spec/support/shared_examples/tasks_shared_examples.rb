# frozen_string_literal: true

RSpec.shared_examples 'handling invalid version args' do
  context 'with an invalid version' do
    before do
      # this test writes to stdout, so we suppress it here so it does
      # not bloat the test output.
      $stdout.stub(:write)
    end

    it 'exits early' do
      expect { subject.invoke('invalid') }.to raise_error(SystemExit)
    end
  end
end
