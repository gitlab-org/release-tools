# frozen_string_literal: true

RSpec.shared_examples 'packages status service' do |job_name|
  subject(:checker) { described_class.new(version, package_type: package_type) }

  let(:project) { ReleaseTools::Project::OmnibusGitlab }
  let(:client) { stub_const('ReleaseTools::GitlabDevClient', spy) }

  shared_examples 'common package checks' do
    context "when jobs are found" do
      it 'returns the status of check-packages jobs' do
        results = checker.execute

        expect(results).to eq(expected_results)
      end
    end

    context 'when a downstream pipeline is not yet created' do
      let(:job) { create(:bridge_job, :success, name: job_name, downstream_pipeline: nil) }

      before do
        allow(client).to receive(:pipeline_bridges).and_return([job])
      end

      it 'raises error' do
        expect { checker.execute }.to raise_error(/No downstream pipeline/)
      end
    end
  end

  context 'with a tag package type' do
    let(:package_type) { :tag }
    let(:version) { ReleaseTools::Version.new('15.11.0') }

    let(:ce_pipeline) do
      create(
        :pipeline,
        id: 1,
        web_url: 'https://gitlab.com/pipeline/1',
        ref: "#{version}+ce.0"
      )
    end

    let(:ee_pipeline) do
      create(
        :pipeline,
        id: 2,
        web_url: 'https://gitlab.com/pipeline/2',
        ref: "#{version}+ee.0"
      )
    end

    let(:ce_job) do
      create(
        :bridge_job,
        :success,
        name: job_name,
        downstream_pipeline: ce_downstream_pipeline
      )
    end

    let(:ee_job) do
      create(
        :bridge_job,
        :failed,
        name: job_name,
        downstream_pipeline: ee_downstream_pipeline
      )
    end

    let(:ce_downstream_pipeline) do
      create(:pipeline,
             :success,
             web_url: 'https://gitlab.com/downstream/1',
             id: 1,
             ref: "#{version}+ce.0")
    end

    let(:ee_downstream_pipeline) do
      create(
        :pipeline,
        :failed,
        web_url: 'https://gitlab.com/downstream/2',
        id: 2,
        ref: "#{version}+ee.0"
      )
    end

    let(:expected_results) { { "15.11.0+ce.0" => 'success', "15.11.0+ee.0" => 'failed' } }

    before do
      allow(client).to receive(:pipelines).with(project, ref: "#{version}+ce.0").and_return([ce_pipeline])
      allow(client).to receive(:pipelines).with(project, ref: "#{version}+ee.0").and_return([ee_pipeline])
      allow(client).to receive(:pipeline_bridges).with(project, 1).and_return([ce_job])
      allow(client).to receive(:pipeline_bridges).with(project, 2).and_return([ee_job])
      allow(client).to receive(:pipeline).with(project, 1).and_return(ce_downstream_pipeline)
      allow(client).to receive(:pipeline).with(project, 2).and_return(ee_downstream_pipeline)
    end

    include_examples 'common package checks'

    context 'when a bridge for a pipeline is empty' do
      before do
        allow(client).to receive(:pipeline_bridges).with(project, 2).and_return([])
        allow(client).to receive(:pipeline).with(project, 2).and_return({})
      end

      it 'raises error' do
        expect { checker.execute }.to raise_error(/No bridge/)
      end
    end
  end

  context 'with an internal package type' do
    let(:package_type) { :internal }
    let(:version) { ReleaseTools::Version.new('15.11.0-internal0') }

    let(:internal_pipeline) do
      create(
        :pipeline,
        id: 3,
        web_url: 'https://gitlab.com/pipeline/3',
        ref: version.stable_branch,
        name: 'INTERNAL_RELEASE_BUILD_PIPELINE'
      )
    end

    let(:internal_job) do
      create(
        :bridge_job,
        :success,
        name: job_name,
        downstream_pipeline: internal_downstream_pipeline
      )
    end

    let(:internal_downstream_pipeline) do
      create(
        :pipeline,
        :success,
        web_url: 'https://gitlab.com/downstream/3',
        id: 3,
        ref: version.stable_branch
      )
    end

    let(:expected_results) { { "15-11-stable" => 'success' } }

    before do
      allow(version).to receive(:stable_branch).and_return('15-11-stable')
      allow(client).to receive(:pipelines).with(project, ref: version.stable_branch).and_return([internal_pipeline])
      allow(client).to receive(:pipeline_bridges).with(project, 3).and_return([internal_job])
      allow(client).to receive(:pipeline).with(project, 3).and_return(internal_downstream_pipeline)
    end

    include_examples 'common package checks'

    it 'fetches the tagless pipeline' do
      expect(client).to receive(:pipelines).with(project, ref: "15-11-stable")
      checker.execute
    end

    context 'when a bridge for a pipeline is empty' do
      before do
        allow(client).to receive(:pipeline_bridges).with(project, 3).and_return([])
      end

      it 'raises error' do
        expect { checker.execute }.to raise_error(/No bridge/)
      end
    end
  end
end
