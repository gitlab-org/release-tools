# frozen_string_literal: true

RSpec.shared_examples 'raises error when MWPS not set' do
  let(:merge_api_response) do
    build(:gitlab_response, state: 'open', merge_when_pipeline_succeeds: false, detailed_merge_status: 'unchecked')
  end

  before do
    allow(merge_request).to receive_messages(iid: 1, project_id: 1)

    allow(service.client)
      .to receive(:accept_merge_request)
      .with(
        merge_request.project_id,
        merge_request.iid,
        merge_when_pipeline_succeeds_options
      )
      .and_return(merge_api_response)
  end

  it 'raises error' do
    without_dry_run do
      expect { service.merge_when_pipeline_succeeds }.to raise_error(described_class::MWPSNotSetError)
    end
  end

  context 'when MR is merged' do
    let(:merge_api_response) do
      build(:gitlab_response, state: 'merged', merge_when_pipeline_succeeds: false, detailed_merge_status: 'not_open')
    end

    it 'does not raise error' do
      without_dry_run do
        expect { service.merge_when_pipeline_succeeds }.not_to raise_error
      end
    end
  end

  %w[ci_still_running ci_must_pass].each do |status|
    context "when #{status} is returned" do
      let(:merge_api_response) do
        build(:gitlab_response, state: 'open', merge_when_pipeline_succeeds: false, detailed_merge_status: status)
      end

      it 'does not raise error' do
        without_dry_run do
          expect { service.merge_when_pipeline_succeeds }.not_to raise_error
        end
      end
    end
  end
end
