# frozen_string_literal: true

RSpec.shared_examples 'returns failed pipelines' do
  let(:version) { ReleaseTools::Version.new('42.1.0-ee') }

  context 'when there are failed pipelines' do
    let(:version) { ReleaseTools::Version.new('15.2.0-rc32') }
    let(:failed_pipeline) { build(:pipeline, :failed) }

    it 'returns failed pipelines' do
      service = described_class.new(version)

      client = service.send(:client)
      allow(client)
        .to receive(:pipelines)
        .and_return(Gitlab::PaginatedResponse.new([failed_pipeline]))

      expect(client)
        .not_to receive(:pipeline_jobs)
        .with(service.project, failed_pipeline.id, any_args)

      played_pipelines, failed_pipelines =
        without_dry_run do
          service.execute
        end

      expect(played_pipelines.length).to eq(0)
      # There should be 1 failed pipeline per version
      expect(failed_pipelines.length).to eq(service.release_versions.length)
    end
  end
end
