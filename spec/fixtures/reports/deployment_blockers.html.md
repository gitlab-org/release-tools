## Overview

| Start date | End date | Production deployments blocked for | # of blockers |
| ------ | -------- | ------| ------ |
| 2024-04-22 | 2024-04-28 | 7.5 | 3 |

## Failure types overview

| Failure Type | Time blocked on gstg | Time blocked on gprd |
| --- | ---- | --- |
| ~"RootCause::Flaky-Test" | 6.0 | 3.5 |
| ~"RootCause::Feature-Flag" | 1.0 | 4.0 |
| **Total** | **7.0** | **7.5** |

## Weekly overview

| Issue  | Blocker type | gstg | gprd |
| ------------ | -----------| ---- | ---- |
| Blocker 4 | ~"RootCause::Flaky-Test" | 5.0 | 3.0 |
| Blocker 5 | ~"RootCause::Feature-Flag" | 1.0 | 4.0 |
| Blocker 6 | ~"RootCause::Flaky-Test" | 1.0 | 0.5 |
| **Total** | | **7.0** | **7.5** |

## Additional incidents

Below is a list of production incidents created last week.

<details><summary>Click to expand</summary>

| Issue   |
| ------- |
| Blocker 7 |


</details>

## Instructions

- [ ] Review the "Additional incidents" list and add the `Deploys-blocked` and `RootCause` labels if required.
- [ ] If values were updated and a new version of the report is required, retrigger this [pipeline job](https://example.com/foo/bar/-/jobs/4) to update this report and metrics with the updated values. Consider this will discard any manual changes added.
- [ ] Update the "weekly overview" table of this issue to also include:
  - [ ] any blocking CRs
- [ ] Update the [Deployments metric review] epic.
  - [ ] Add a new row to the Overview section: Copy and paste the information from the Overview section in this issue and link to this issue in the `Breakdown of blockers` column.
  - [ ] Update the Graph: Update the data on the [spreadsheet] and then update the graph on the [Deployments metric review] epic.

:mega: The deployment blockers per week can now be visualized in the [Deployment Blockers Dashboard].

[Deployments metric review]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1496
[spreadsheet]: https://docs.google.com/spreadsheets/d/1wzZE-3EU14jS0tkEDWrW7oCLH5Xr9ptwxQ8DEDgCxTA/edit?gid=527208806
[Deployment Blockers Dashboard]: https://dashboards.gitlab.net/d/delivery-deployment_blockers/delivery3a-deployment-blockers?orgId=1
