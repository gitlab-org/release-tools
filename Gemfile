# frozen_string_literal: true

source "https://rubygems.org"

gem 'activesupport', '~> 8.0.0'
# chef-api is deprecated but brings in fewer dependencies
# than the official chef gem
gem 'chef-api', '~> 0.10.10'
gem 'colorize', '~> 1.1.0'
gem 'cvss-suite', '~> 4.0.0'
gem 'diplomat', '~> 2.0', '>= 2.0.2'
gem 'gitlab', '~> 5.1.0'
gem 'gitlab-releases', '~> 1.1.0'
gem 'graphql-client', '~> 0.25.0'
gem 'http', '~> 5.2.0'
gem 'parallel', '~> 1.26.0'
gem 'rake', '~> 13.2.0'
gem 'retriable', '~> 3.1.2'
gem 'rugged', '~> 1.9.0'
gem 'semantic_logger', '~> 4.16.0'
gem 'sentry-ruby', '~> 5.8'
gem 'slack-ruby-block-kit', '~> 0.26.0'
gem 'tzinfo-data', '~> 1.2025.0' # Required for CI environments where the OS might not have timezone data.
gem 'unleash', '~> 5.1.0'
gem 'version_sorter', '~> 2.3.0'

group :metrics do
  gem 'prometheus-client', '~> 2.1.0'
  gem 'state_machines', '~> 0.6.0'
end

group :tracing do
  gem 'opentelemetry-exporter-otlp', '~> 0.30.0'
  gem 'opentelemetry-sdk', '~> 1.8.0'
end

group :development, :test do
  gem 'byebug', '~> 11.1.0'
  gem 'climate_control', '~> 1.2.0'
  gem 'factory_bot', '~> 6.5.0'
  gem 'fuubar', require: false
  gem 'gitlab-dangerfiles', '~> 4.8.0', require: false
  gem 'lefthook', '~> 1.11.0', require: false
  gem 'pry', '~> 0.15.0'
  gem 'rspec', '~> 3.13.0'
  gem 'rspec_junit_formatter', '~> 0.6.0', require: false
  gem 'rspec-parameterized', '>= 1.0.0'
  gem 'rubocop', '~> 1.72.1'
  gem 'rubocop-performance', '~> 1.24.0'
  gem 'rubocop-rspec', '~> 3.5.0'
  gem 'simplecov', '~> 0.22.0'
  gem 'timecop', '~> 0.9.0'
  gem 'vcr', '~> 6.3.0'
  gem 'webmock', '~> 3.25.0'
end
