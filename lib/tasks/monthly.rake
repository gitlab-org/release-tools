# frozen_string_literal: true

namespace :monthly do
  namespace :finalize do
    desc 'Notify the finalize pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :finalize, release_type: :monthly)
        .execute
    end

    desc 'Update the protected stable branches'
    task :update_protected_branches do
      ReleaseTools::Monthly::Finalize::UpdateProtectedBranches.new.execute
    end

    desc 'Create a new monthly status metric'
    task :create_release_status_metric do
      ReleaseTools::Metrics::MonthlyReleaseStatus
      .new(status: :open)
      .execute
    end

    desc 'Create a new monthly version'
    task :create_version do
      version = ReleaseTools::Version.new(GitlabReleases.active_version)
      ReleaseTools::Services::CreateVersionService.new(version: version).execute
    end
  end

  namespace :tag_day do
    desc 'Notify the tag day pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :tag_day, release_type: :monthly)
        .execute
    end

    desc 'Ensure stable branches are green'
    task :ensure_stable_branches_green do
      ReleaseTools::Monthly::TagDay::ComponentBranchVerifier.new.execute
    end

    desc 'Notify the tagging steps for the monthly release have started'
    task :start_tag do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :tag, release_type: :monthly)
        .execute
    end

    desc 'Tag the monthly release'
    task :tag do |_t, _args|
      version = ReleaseTools::Version.new(GitlabReleases.active_version)
      ReleaseTools::Tasks::Release::Tag
        .new(version)
        .execute
    end

    desc 'Verify packages are tagged by checking `check-packages-functionality` job'
    task :check_omnibus_packages_tagging do
      version = ReleaseTools::Version.new(GitlabReleases.active_version)
      ReleaseTools::Services::OmnibusPackages::Tagging.new(version: version).execute
    end

    desc 'Validate deployment in release environment'
    task :validate_release_deployment do
      version = ReleaseTools::Version.new(GitlabReleases.active_version)
      ReleaseTools::Tasks::ReleaseGitlabNet::DeployValidator
       .new(version)
       .execute
    end
  end

  namespace :release_day do
    desc 'Notify the release day pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :release_day, release_type: :monthly)
        .execute
    end

    desc 'Send slack notification that the release packages will be published'
    task :notify_release_publish do
      ReleaseTools::Monthly::ReleaseDay::NotifyReleasePublish.new.execute
    end

    desc 'Notify the release publishing steps have started'
    task :start_publish do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :publish, release_type: :monthly)
        .execute
    end

    desc 'Publish the monthly release'
    task :publish do |_t, _args|
      version = ReleaseTools::Version.new(GitlabReleases.active_version)
      ReleaseTools::Tasks::Release::Publish
        .new(version)
        .execute
    end
  end

  namespace :verify do
    desc 'Notify the verify pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :verify, release_type: :monthly)
        .execute
    end

    desc 'Verify packages are published by checking `check-packages-availability` job'
    task :check_omnibus_packages_publishing do
      version = ReleaseTools::Version.new(GitlabReleases.active_version)
      ReleaseTools::Services::OmnibusPackages::Publishing.new(version: version).execute
    end

    desc 'Check docker hub tags'
    task :check_docker_tags do
      version = ReleaseTools::Version.new(GitlabReleases.active_version)
      ReleaseTools::DockerHub::Verifier.new(release_type: :monthly, version: version).execute
    end
  end
end
