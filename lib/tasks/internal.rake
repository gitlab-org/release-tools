# frozen_string_literal: true

namespace :internal do
  desc 'Create an internal release task issue'
  task :issue, [:iteration, :version] do |_t, args|
    # `version` argument is optional
    # if not passed, the Coordinator versions will be used
    iteration = args[:iteration]

    issue = if args[:version].nil?
              ReleaseTools::InternalRelease::Issue.new(iteration: iteration)
            else
              latest_patch = ReleaseTools::GitlabReleasesGemClient
                  .latest_patch_for_version(args[:version])

              ReleaseTools::InternalRelease::Issue.new(
                iteration: iteration,
                version: ReleaseTools::Version.new(latest_patch)
              )
            end

    create_or_show_issue(issue)
  end

  namespace :prepare do
    desc 'Notify the prepare pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :start, release_type: :internal)
        .execute
    end

    desc 'Ensure stable branches are green'
    task :check_component_branch_pipeline_status do
      internal_versions = ENV['VERSIONS']
                            .split.map { |v| ReleaseTools::Version.new(v) }

      ReleaseTools::InternalRelease::Prepare::ComponentBranchVerifier
        .new(internal_versions)
        .execute
    end
  end

  namespace :release do
    desc 'Notify the release pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :release, release_type: :internal)
        .execute
    end

    desc 'Create child jobs for tagging patch releases'
    task :generate_dynamic_pipeline do
      versions = ReleaseTools::InternalRelease::Coordinator.new.versions
      release_jobs = ReleaseTools::InternalRelease::ReleaseDynamicPipeline.new(versions).generate
      File.write('dynamic-gitlab-ci.yml', release_jobs)
    end

    desc 'Perform the steps required to build a private package'
    task :build_package, [:version] do |_t, args|
      iteration = ENV['ITERATION'].to_i
      version = ReleaseTools::Version.new(args[:version])

      ReleaseTools::InternalRelease::Release.new(
        version: version,
        internal_number: iteration
      ).execute
    end
  end

  namespace :verify do
    desc 'Notify the verify pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :verify, release_type: :internal)
        .execute
    end

    desc 'Check if the package has been built and available on the pre-release channel'
    task :package_availability do
      coordinator = ReleaseTools::InternalRelease::Coordinator.new

      coordinator.versions.each do |version|
        ReleaseTools::Services::OmnibusPackages::Tagging.new(
          version: version,
          package_type: :internal
        ).execute
      end
    end
  end

  namespace :finalize do
    desc 'Notify the finalize pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :finalize, release_type: :internal)
        .execute
    end

    desc 'Notify internal release package availability'
    task :notify_releases do
      iteration = ENV['ITERATION'].to_i
      ReleaseTools::InternalRelease::NotifyPackageAvailability
        .new(iteration).execute
    end
  end
end
