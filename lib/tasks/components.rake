# frozen_string_literal: true

namespace :components do
  desc 'Prepare a merge request for updating GITALY_SERVER_VERSION on gitlab master branch'
  task :update_gitaly do
    ReleaseTools::Tasks::Components::UpdateGitaly.new.execute
  end

  desc 'Prepare a merge request for updating GITLAB_KAS_VERSION on gitlab master branch'
  task :update_kas do
    if ReleaseTools::Feature.enabled?(:automatic_kas_update)
      ReleaseTools::Tasks::Components::UpdateKas.new.execute
    else
      ReleaseTools.logger.info('automatic_kas_update feature flag disabled. Nothing to do')
    end
  end
end
