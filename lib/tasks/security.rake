namespace :security do
  # Undocumented; should be a pre-requisite for every task in this namespace!
  task :force_security do
    ENV['SECURITY'] = if ReleaseTools::SharedStatus.critical_patch_release?
                        'critical'
                      else
                        'true'
                      end
  end

  desc 'Create a patch release task issue'
  task issue: :force_security do |_t|
    issue = ReleaseTools::PatchRelease::SecurityIssue.new

    create_or_show_issue(issue)
  end

  desc 'Merges valid security merge requests'
  task :merge, [:merge_default] => :force_security do |_t, args|
    merge_default =
      if args[:merge_default] && !args[:merge_default].empty?
        true
      else
        false
      end

    ReleaseTools::Security::MergeRequestsMerger
      .new(merge_default: merge_default)
      .execute
  end

  desc 'Toggle the security merge train based on need'
  task :merge_train do |_t, _args|
    ReleaseTools::Security::MergeTrainService
      .new
      .execute
  end

  desc 'Prepare for a new patch release'
  task prepare: :force_security do |_t, _args|
    issue_task = Rake::Task['security:issue']
    issue_task.execute
  end

  desc "Check a patch release's build status"
  task status: :force_security do |t, _args|
    status = ReleaseTools::BranchStatus.for_security_release

    status.each_pair do |project, results|
      results.each do |result|
        ReleaseTools.logger.tagged(t.name) do
          ReleaseTools.logger.info(project, result.to_h)
        end
      end
    end

    ReleaseTools::Slack::ChatopsNotification.branch_status(status)
  end

  desc 'Validate deployment in release environment'
  task validate_release_deployment: :force_security do
    version = ReleaseTools::PatchRelease::Coordinator.new.versions.first
    ReleaseTools::Tasks::ReleaseGitlabNet::DeployValidator
     .new(version)
     .execute
  end

  desc 'Validates merge requests in security projects'
  task validate: :force_security do
    ReleaseTools::Security::ProjectsValidator
      .new(ReleaseTools::Security::Client.new)
      .execute
  end

  desc 'Sync default and auto-deploy branches'
  task sync_remotes: :force_security do
    ReleaseTools::Security::SyncRemotesService
      .new
      .execute
  end

  desc 'Sync Git tags'
  task :sync_git_tags, [:git_versions] => :force_security do |_t, args|
    git_versions = args[:git_versions].split

    ReleaseTools::Security::SyncGitRemotesService
      .new(git_versions)
      .execute
  end

  desc 'Enable Gitaly Update Task'
  task enable_gitaly_update_task: :force_security do
    ReleaseTools::Security::GitalyUpdateTask
      .new(action: :enable)
      .execute
  end

  desc 'Disable Gitaly Update Task'
  task disable_gitaly_update_task: :force_security do
    ReleaseTools::Security::GitalyUpdateTask
      .new(action: :disable)
      .execute
  end

  namespace :verify do
    desc 'Notify the verify pipeline has started'
    task start:  :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :verify, release_type: :patch)
        .execute
    end

    desc 'Verify packages are published by checking `check-packages-availability` job'
    task :check_omnibus_packages_publishing do
      versions = ReleaseTools::PatchRelease::Coordinator.new.versions
      versions.each do |version|
        ReleaseTools::Services::OmnibusPackages::Publishing.new(version: version).execute
      end
    end

    desc 'Verify if all images are published on docker hub'
    task check_docker_tags: :force_security do
      versions = ReleaseTools::PatchRelease::Coordinator.new.versions
      versions.each do |version|
        ReleaseTools::DockerHub::Verifier.new(release_type: :patch, version: version).execute
      end
    end
  end

  namespace :gitaly do
    desc 'Tag a new Gitaly security release'
    task :tag, [:version] => :force_security do |_, args|
      Rake::Task['release:gitaly:tag'].invoke(*args)
    end
  end

  desc 'Link/Unlink security-target issues for a patch release'
  task process_security_target_issues: :force_security do
    ReleaseTools::Security::TargetIssuesProcessor.new.execute
  end

  desc 'Check components for green pipelines'
  task check_component_branch_pipeline_status: :force_security do
    ReleaseTools::Security::Prepare::ComponentBranchVerifier.new.execute
  end

  namespace :prepare do
    desc 'Notify the prepare pipeline has started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :start, release_type: :patch)
        .execute
    end

    desc 'Notify JiHu about the patch release'
    task notify_jihu: :force_security do
      ReleaseTools::Security::Prepare::NotifyJihu.new.execute
    end

    desc 'Notify a stage team about the patch release'
    task notify_stage_team: :force_security do
      team = ENV.fetch('STAGE_TEAM', nil).to_sym

      ReleaseTools::Slack::Security::TeamNotifier
        .new(team: team)
        .execute
    end

    desc 'Reviews if the patch release includes security fixes'
    task review_security_fixes: :force_security do
      ReleaseTools::Security::Prepare::FixesVerifier.new.execute
    end

    desc 'Create AppSec Issue'
    task appsec_issue: :force_security do
      ReleaseTools::Security::Prepare::IssueCreator.new(
        issue: ReleaseTools::Security::AppSecIssue.new,
        issue_type: 'appsec_task_issue'
      ).execute
    end

    desc 'Create Comms Issue'
    task comms_issue: :force_security do
      ReleaseTools::Security::Prepare::IssueCreator.new(
        issue: ReleaseTools::Security::CommsTaskIssue.new,
        issue_type: 'comms_security_task_issue'
      ).execute
    end
  end

  namespace :release_preparation do
    desc 'Notify the release preparation steps have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :release_preparation, release_type: :patch)
        .execute
    end
  end

  namespace :disable_security_target_processor do
    desc 'Notify the disable security target processor schedule steps have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :disable_issue_processor_schedule, release_type: :patch)
        .execute
    end

    desc 'Disable the security-target issue processor'
    task execute: :force_security do
      ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor.new.execute(action: :disable)
    end

    desc 'Verify pending linked issues'
    task verify_pending_issues: :force_security do
      ReleaseTools::Security::PendingIssuesNotificationService
       .new
       .execute
    end

    desc 'Check for linked projects under managed versioning'
    task verify_managed_version_projects: :force_security do
      ReleaseTools::Security::ManagedVersioningNotificationService.new.execute
    end
  end

  namespace :early_merge do
    desc 'Notify the merging steps for the security default branch MRs have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :early_merge, release_type: :patch)
        .execute
    end

    desc 'Verify that the security default branch MRs have been merged'
    task verify_pending_merge: :force_security do
      ReleaseTools::Security::PendingMergeNotificationService
        .new
        .execute
    end

    desc 'Verify that the security default branch MRs have been deployed to production'
    task verify_pending_deploy: :force_security do
      ReleaseTools::Security::PendingDeployNotificationService
       .new
       .execute
    end
  end

  namespace :backport_merge do
    desc 'Notify the merging steps for the security stable branches MRs have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :backport_merge, release_type: :patch)
        .execute
    end
  end

  desc 'Tag a new patch release'
  task :tag, [:version] => :force_security do |_t, args|
    ReleaseTools::Tasks::Release::Tag
    .new(args[:version])
    .execute
  end

  namespace :tag do
    desc 'Notify the tagging steps for the patch release versions have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :tag, release_type: :patch)
        .execute
    end

    desc 'Create child jobs for tagging patch releases'
    task generate_dynamic_pipeline: :force_security do
      versions = ReleaseTools::PatchRelease::Coordinator.new.versions
      tag_jobs = ReleaseTools::Security::Tag::DynamicPipeline.new(versions).generate
      File.write('dynamic-tag-gitlab-ci.yml', tag_jobs)
    end

    desc 'Verify packages are tagged by checking `check-packages-functionality` job'
    task :check_omnibus_packages_tagging do
      versions = ReleaseTools::PatchRelease::Coordinator.new.versions
      versions.each do |version|
        ReleaseTools::Services::OmnibusPackages::Tagging.new(version: version).execute
      end
    end
  end

  desc 'Publish the current patch release'
  task :publish, [:version] => :force_security do |_t, args|
    ReleaseTools::Tasks::Release::Publish
    .new(args[:version])
    .execute
  end

  namespace :publish do
    desc 'Notify the release publishing steps have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :publish, release_type: :patch)
        .execute
    end

    desc 'Create child jobs for publishing patch releases'
    task generate_dynamic_pipeline: :force_security do
      versions = ReleaseTools::PatchRelease::Coordinator.new.versions
      publish_jobs = ReleaseTools::Security::Publish::DynamicPipeline.new(versions).generate
      File.write('dynamic-gitlab-ci.yml', publish_jobs)
    end

    desc 'Move the blog post from security to canonical'
    task move_blog_post: :force_security do
      ReleaseTools::Security::Publish::MoveBlogPost.new.execute
    end

    desc 'Deploy the patch release blog post'
    task deploy_blog_post: :force_security do
      ReleaseTools::Security::Publish::DeployBlogPost.new.execute
    end

    desc 'Verify the patch release blog post'
    task verify_blog_post: :force_security do
      ReleaseTools::Security::Publish::VerifyBlogPost.new.execute
    end
  end

  namespace :create_versions do
    desc 'Notify the create_versions pipeline has started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :version_creation, release_type: :patch)
        .execute
    end

    desc 'Create the versions on version.gitlab.com'
    task create: :force_security do
      versions = ReleaseTools::PatchRelease::Coordinator.new.versions
      versions.each do |version|
        ReleaseTools::Services::CreateVersionService.new(version: version).execute
      end
    end
  end

  namespace :finalize do
    desc 'Notify the finalize pipeline has started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :finalize, release_type: :patch)
        .execute
    end

    desc 'Close implementation issues'
    task close_issues: :force_security do
      ReleaseTools::Security::Finalize::CloseImplementationIssues
        .new
        .execute
    end

    desc 'Notify the completion of the patch release'
    task notify_release: :force_security do
      ReleaseTools::Security::Finalize::NotifyReleaseComplete.new.execute
    end

    desc 'Check tags are synced to canonical'
    task check_canonical_tags_synced: :force_security do
      ReleaseTools::Security::Finalize::CheckCanonicalTagsSynced.new.execute
    end

    desc 'Update Security Tracking Issue'
    task update_tracking_issue: :force_security do
      updater = ReleaseTools::Security::Finalize::CloseTrackingIssue.new
      updater.execute

      security_tracking_issue = ReleaseTools::Security::TrackingIssue.new
      create_or_show_issue(security_tracking_issue)
    end

    desc 'Enable the security-target issue processor'
    task enable_security_target_processor: :force_security do
      ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor.new.execute(action: :enable)
    end

    desc 'Notify upcoming release managers'
    task notify_upcoming_release_managers: :force_security do
      ReleaseTools::Security::Finalize::NotifyNextReleaseManagers.new.execute
    end

    desc 'Update the slack bookmark of the security tracking issue'
    task update_slack_bookmark: :force_security do
      ReleaseTools::Security::Finalize::UpdateSlackBookmark
        .new.execute
    end

    desc 'Create a new patch release status metric'
    task create_release_status_metric: :force_security do
      ReleaseTools::Metrics::PatchReleaseStatus
        .new(status: :open)
        .execute
    end

    desc 'Sync security to canonical'
    task sync_security_to_canonical: :force_security do
      ReleaseTools::Security::Finalize::SyncSecurityToCanonical.new.execute
    end
  end
end
