# frozen_string_literal: true

namespace :pre do
  desc 'Find the latest version on gprd to deploy to Pre'
  task :prepare do
    ReleaseTools::Pre::Prepare.new.execute
  end

  desc 'Send Slack message about the state of the pre deployment'
  task :notify do
    ReleaseTools::Pre::Notifier.new(
      pipeline_id: ENV.fetch('CI_PIPELINE_ID'),
      deploy_version: ENV.fetch('DEPLOY_VERSION')
    ).execute
  end

  desc 'Finalize pre deployment'
  task :release_note do
    ReleaseTools::Pre::ReleaseNote.new(
      pipeline_id: ENV.fetch('CI_PIPELINE_ID'),
      deploy_version: ENV.fetch('DEPLOY_VERSION')
    ).execute
  end
end
