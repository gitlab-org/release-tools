# frozen_string_literal: true

module ReleaseTools
  module Monthly
    module ReleaseDay
      class NotifyReleasePublish
        include ::SemanticLogger::Loggable

        def initialize
          @version = ReleaseTools::Version.new(GitlabReleases.active_version)
        end

        def execute
          logger.info("Notifying that the release packages for #{version} will be published")

          ReleaseTools::Slack::Message.post(
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            message: release_publish_message
          )
        end

        private

        attr_reader :version

        def release_publish_message
          ":mega: Packages for #{version} are built and will be published at 13:10 UTC"
        end
      end
    end
  end
end
