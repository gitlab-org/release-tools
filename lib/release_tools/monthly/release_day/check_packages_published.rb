# frozen_string_literal: true

module ReleaseTools
  module Monthly
    module ReleaseDay
      class CheckPackagesPublished
        include ::SemanticLogger::Loggable

        PackagePublishingError = Class.new(StandardError)
        PublishingInProgressError = Class.new(StandardError)

        FAILED_STATUSES = %w[failed canceled skipped manual].freeze
        SUCCESS_STATUS = "success"

        def initialize(version:)
          @version            = version
          @packages_statuses  = {}
        end

        def execute
          logger.info("Verifying CE/EE packages are published", version: version)

          Retriable.with_context(:release_package_pipeline, on: PublishingInProgressError) do
            packages_statuses = Services::PackagesStatus::Availability.new(version).execute
            if packages_statuses.empty?
              raise PackagePublishingError, "No packages found for #{version}"
            end

            if all_packages_succeeded?(packages_statuses)
              log_success(packages_statuses)
              send_slack_notification(:success)
            elsif any_package_failed?(packages_statuses)
              log_failure(packages_statuses)
              raise PackagePublishingError
            else
              log_in_progress(packages_statuses)
              raise PublishingInProgressError
            end
          end
        rescue StandardError
          send_slack_notification(:failed)
          raise
        end

        private

        attr_reader :version, :packages_statuses

        def all_packages_succeeded?(packages_statuses)
          packages_statuses.values.all?(SUCCESS_STATUS)
        end

        def any_package_failed?(packages_statuses)
          packages_statuses.values.any? { |status| FAILED_STATUSES.include?(status) }
        end

        def log_success(packages_statuses)
          logger.info("Packages for #{packages_statuses.keys.join(', ')} are published")
        end

        def log_failure(packages_statuses)
          failed_packages = packages_statuses.reject { |_, status| status == SUCCESS_STATUS }
          logger.error("Failed to publish packages: #{failed_packages}")
        end

        def log_in_progress(packages_statuses)
          in_progress = packages_statuses.reject { |_, status| status == SUCCESS_STATUS }
          logger.info("Packages still in progress: #{in_progress}")
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Verify packages published',
            status: status,
            release_type: :monthly
          ).send_notification
        end
      end
    end
  end
end
