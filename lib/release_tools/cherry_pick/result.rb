# frozen_string_literal: true

module ReleaseTools
  module CherryPick
    # Represents the result of a cherry pick
    class Result
      attr_reader :merge_request, :reason, :new_sha

      # merge_request - The merge request we attempted to pick
      # status        - Status of the pick (`:success`, `:failure`, `:denied`, `:not_required`, `:completed`)
      # new_sha       - The SHA returned by the cherry-pick API.
      def initialize(merge_request:, status:, reason: nil, new_sha: nil)
        @merge_request = merge_request
        @status = status
        @reason = reason
        @new_sha = new_sha
      end

      def success?
        @status == :success
      end

      def denied?
        @status == :denied
      end

      # Cherry-picked commit found in auto deploy branch so cherry-picking is not needed into this branch
      def not_required?
        @status == :not_required
      end

      # Merge commit found in the auto deploy branch so cherry-picking is no longer needed
      def completed?
        @status == :completed
      end

      def failure?
        @status == :failure
      end

      def title
        merge_request.title
      end

      def url
        merge_request.web_url
      end

      def to_markdown
        "[#{title}](#{url})"
      end
    end
  end
end
