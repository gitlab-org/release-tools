# frozen_string_literal: true

module ReleaseTools
  module PipelineTracer
    class ProcessJobs
      include ::SemanticLogger::Loggable

      # @param [OpenTelemetry::SDK::Trace::Tracer] tracer
      # @param [PipelineTracer::Pipeline] pipeline
      # @param [Integer] trace_depth
      def initialize(tracer, pipeline, trace_depth:)
        @tracer = tracer
        @pipeline = pipeline
        @trace_depth = trace_depth
        @processed_jobs = {}
      end

      def execute
        jobs = pipeline.jobs.lazy_paginate
          .map { |job_attributes| Job.new(job_attributes, pipeline.client) }
          .select { |job| job.completed? && !job.finished_at.nil? }
          .sort_by { |j| Time.parse(j.finished_at) }

        jobs.each do |job|
          if job.started_at.nil? || job.finished_at.nil?
            # Since jobs are usually created much before they start, we cannot use created_at and updated_at in place
            # of started_at and finished_at.
            logger.fatal("Job does not have started_at or finished_at",
                         job_name: job.name, status: job.status, started_at: job.started_at, finished_at: job.finished_at, web_url: job.web_url)
            next
          end

          span = tracer.start_span(
            job.name,
            links: links(job), # Add a link to the previous run of this job, if there was a previous run.
            kind: :internal,
            start_timestamp: Time.parse(job.started_at),
            attributes: job.root_attributes
          )

          span.finish(end_timestamp: Time.parse(job.finished_at))

          after_process_job(span, job)
        end
      end

      private

      attr_reader :tracer, :pipeline, :trace_depth

      def links(job)
        previous_span = @processed_jobs[job.name]
        return unless previous_span

        [OpenTelemetry::Trace::Link.new(previous_span.context)]
      end

      def after_process_job(span, job)
        @processed_jobs[job.name] = span

        process_triggered_pipeline(job)
      end

      def process_triggered_pipeline(job)
        return unless job.triggered_downstream_pipeline?

        logger.info('Job triggered downstream pipeline', downstream_pipeline_url: job.triggered_pipeline_url, job_url: job.web_url)

        Service.from_pipeline_url(
          job.triggered_pipeline_url,
          pipeline_name: "#{job.name} triggered downstream pipeline",
          trace_depth: trace_depth - 1
        )
        .execute
      end
    end
  end
end
