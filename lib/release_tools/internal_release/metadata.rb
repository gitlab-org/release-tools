# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    class Metadata
      include ::SemanticLogger::Loggable

      def initialize(version:)
        @version = ReleaseTools::Version.new(version)
        @release_metadata = ReleaseTools::ReleaseMetadata.new
      end

      # gather release metadata from projects under managed versioning, then uploads it
      def record
        projects.each do |project|
          add_release_data(project.metadata_project_name, latest_stable_branch_sha(project))
        end

        upload_release_metadata
      end

      private

      attr_reader :release_metadata, :version

      def projects
        ReleaseTools::ManagedVersioning::PROJECTS
      end

      def latest_stable_branch_sha(project)
        Retriable.with_context(:api) do
          GitlabClient.commit(project.security_path, ref: version.stable_branch(ee: project.ee_branch?)).id
        end
      end

      def add_release_data(name, sha)
        meta_version = version.to_normalized_version

        logger.info(
          'Recording release data',
          name: name,
          version: meta_version,
          sha: sha,
          ref: sha,
          tag: false
        )

        release_metadata.add_release(
          name: name,
          version: meta_version,
          sha: sha,
          ref: sha,
          tag: false
        )
      end

      def upload_release_metadata
        file_name = version.to_normalized_version

        return if SharedStatus.dry_run?

        ReleaseTools::ReleaseMetadataUploader.new.upload(file_name, release_metadata, auto_deploy: false)
      end
    end
  end
end
