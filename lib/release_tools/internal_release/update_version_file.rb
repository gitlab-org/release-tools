# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    class UpdateVersionFile
      include ::SemanticLogger::Loggable

      SECURITY_PROJECT_PATH = Project::GitlabEe.security_path
      VERSION_FILE_NAME = 'VERSION'

      def initialize(version:, gitlab_branch:)
        @client = ReleaseTools::GitlabClient
        @version = version
        @gitlab_branch = gitlab_branch
      end

      def execute
        logger.info("Checking stable branch #{gitlab_branch}")

        return if SharedStatus.dry_run?

        if version_matches?
          logger.info("File already contains the correct version, fetching last commit from stable branch", current_version: current_version, version: version)

          commit_from_stable_branch
        else
          logger.info("File value mismatch. Creating commit", current_version: current_version, version: version)

          create_version_commit
        end
      end

      private

      attr_reader :client, :version, :gitlab_branch

      def version_matches?
        logger.info('Comparing the current file content with the internal version',
                    file_path: VERSION_FILE_NAME,
                    current_version: current_version,
                    version: version)

        current_version == version
      end

      def current_version
        @current_version ||=
          Retriable.with_context(:api) do
            client.file_contents(
              SECURITY_PROJECT_PATH,
              VERSION_FILE_NAME,
              gitlab_branch
            )
          rescue Gitlab::Error::NotFound
            logger.info("Cannot find version file", file_path: VERSION_FILE_NAME)
            raise
          end
      end

      def create_version_commit
        message = "Update #{VERSION_FILE_NAME} file for #{version}"

        logger.info(
          "Setting version file",
          project: SECURITY_PROJECT_PATH,
          gitlab_branch: gitlab_branch,
          file_path: VERSION_FILE_NAME,
          version: version
        )

        commit = Retriable.with_context(:api) do
          client.create_commit(
            SECURITY_PROJECT_PATH,
            gitlab_branch,
            message,
            [
              {
                action: 'update',
                file_path: VERSION_FILE_NAME,
                content: version
              }
            ]
          )
        end

        logger.info('Updated version file on security Gitlab project',
                    file_path: VERSION_FILE_NAME,
                    url: commit.web_url)
        commit
      end

      def commit_from_stable_branch
        Retriable.with_context(:api) do
          client.commits(SECURITY_PROJECT_PATH, ref_name: gitlab_branch).first
        end
      end
    end
  end
end
