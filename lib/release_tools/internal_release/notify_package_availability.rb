# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    class NotifyPackageAvailability
      include ::SemanticLogger::Loggable

      TASK_PROJECT = ReleaseTools::Project::Release::Tasks

      def initialize(internal_number)
        @internal_number = internal_number
      end

      def execute
        logger.info('Notifying about internal package availability')

        return if ReleaseTools::SharedStatus.dry_run?

        notify_slack_package_availability
        notify_release_issue

        send_slack_notification(:success)
      rescue StandardError => ex
        logger.fatal(
          'Error occurred while sending package availability notification',
          error: ex.message
        )

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :internal_number

      def client
        ReleaseTools::GitlabClient
      end

      def versions
        ReleaseTools::InternalRelease::Coordinator.new(internal_number).versions
      end

      def regular_versions
        versions.map(&:to_minor).join(' and ')
      end

      def options
        {
          state: 'opened',
          labels: ['internal releases'],
          author_id: ReleaseTools::Bot::GITLAB_COM_ID
        }
      end

      def internal_task_issue
        @internal_task_issue ||=
          Retriable.with_context(:api) do
            client.issues(TASK_PROJECT, options).first
          end
      end

      def release_managers
        @release_managers ||= ReleaseTools::ReleaseManagers::Schedule.new
      end

      def usernames(users)
        users.map do |user|
          "@#{user.username}"
        end.join(' ')
      end

      def body
        ERB
          .new(template, trim_mode: '-')
          .result(binding)
      end

      def template
        File.read(
          File.expand_path('../../../templates/internal_release_issue_note.md.erb', __dir__)
        )
      end

      def notify_release_issue
        logger.info(
          'Adding a comment to internal release task issue about the package availability',
          web_url: internal_task_issue.web_url
        )

        Retriable.with_context(:api) do
          client.create_issue_note(
            TASK_PROJECT,
            issue: internal_task_issue,
            body: body
          )
        end
      end

      def notify_slack_package_availability
        ReleaseTools::Slack::InternalRelease::PackageAvailabilityNotifier.new(versions).execute
      end

      def send_slack_notification(status)
        ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: "Notifying internal release package availability",
          status: status,
          release_type: :internal
        ).send_notification
      end
    end
  end
end
