# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    class ReleaseDynamicPipeline < DynamicPipelineBase
      def stages
        {
          'stages' => versions.map { |version| "internal_release_release:build_package:#{version}" }
        }
      end

      def job(version)
        {
          'image' => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          'stage' => "internal_release_release:build_package:#{version}",
          'script' => [
            "source scripts/setup_ssh.sh",
            "source scripts/setup_git.sh",
            "bundle exec rake internal:release:build_package[#{version}]"
          ],
          'extends' => [
            '.with-bundle',
            '.common-ci-tokens'
          ],
          'needs' => [], # Make the jobs run in parallel
          'timeout' => "2h"
        }
      end

      def jobs
        versions.each_with_object({}) do |version, hash|
          hash["internal_release_release_build_package:#{version}"] = job(version)
        end
      end
    end
  end
end
