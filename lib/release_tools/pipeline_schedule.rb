# frozen_string_literal: true

module ReleaseTools
  class PipelineSchedule
    attr_reader :id, :project

    delegate :description, :active, to: :data

    def initialize(project:, id:, client: GitlabOpsClient)
      @client = client
      @project = project # Both project path and project id are accepted
      @id = id
    end

    def activate
      update_attribute(:active, true)
    end

    def deactivate
      update_attribute(:active, false)
    end

    def run
      Retriable.with_context(:api) do
        client.run_pipeline_schedule(project, id)
      end
    end

    def take_ownership
      @data = Retriable.with_context(:api) do
        client.pipeline_schedule_take_ownership(project, id)
      end
    end

    def last_pipeline
      update_data.last_pipeline
    end

    def data
      @data ||= update_data
    end

    def pipeline(id)
      Retriable.with_context(:api) do
        client.pipeline(project, id)
      end
    end

    private

    attr_reader :client

    def update_data
      @data = Retriable.with_context(:api) do
        client.pipeline_schedule(project, id)
      end
    end

    def update_attribute(attribute, value)
      attributes = { attribute => value }
      @data = Retriable.with_context(:api) do
        client.edit_pipeline_schedule(project, id, attributes)
      end
    end
  end
end
