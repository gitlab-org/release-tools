# frozen_string_literal: true

require 'http'

module ReleaseTools
  module Deployments
    # Track Sentry Releases and Deployments
    #
    # See https://docs.sentry.io/workflow/releases/
    # and https://blog.sentry.io/2017/05/09/release-deploys
    class SentryTracker
      include ::SemanticLogger::Loggable

      API_ENDPOINT = 'https://new-sentry.gitlab.net/api/0/organizations/gitlab/releases/'
      SHA_RANGE = (0...11)

      def release(sha)
        return if SharedStatus.dry_run?

        current_release(sha)
      end

      # Create a new Deploy
      #
      # environment - environment string (gprd, gprd-cny, gstg, gstg-cny, db/gstg, db/gprd)
      # version - auto-deploy version (e.g., `13.1.202006091440-ede0c8ff414.008fd19283c`)
      def deploy(environment, status, version)
        return if SharedStatus.dry_run?
        return unless status == 'success'
        return unless ReleaseTools::AutoDeploy::Version.match?(version)

        sha = ReleaseTools::AutoDeploy::Version.new(version).rails_sha
        now = Time.now.utc.iso8601

        current_deploy(environment, sha, now)
      end

      private

      def current_release(sha)
        return unless new_release?(sha[SHA_RANGE])

        params = {
          version: sha[SHA_RANGE],
          refs: [
            {
              repository: 'GitLab.org / security / 🔒 gitlab',
              commit: sha
            }
          ],
          projects: %w[gitlabcom gitlabcom-clientside]
        }

        logger.info('Creating Sentry release', version: params[:version])
        response = client.post(API_ENDPOINT, json: params)

        unless response.status.success?
          logger.warn('Creating Sentry release was not successful', status: response.status.to_s, response: response.to_s)
        end

        response
      end

      def current_deploy(environment, sha, now)
        params = {
          environment: environment,
          dateStarted: now,
          dateFinished: now
        }

        logger.info('Creating Sentry deploy', version: sha, environment: environment)
        response = client.post("#{API_ENDPOINT}#{sha}/deploys/", json: params)

        unless response.status.success?
          logger.warn('Creating Sentry deploy was not successful', status: response.status.to_s, response: response.to_s)
        end

        response
      end

      def token
        @token ||=
          ENV.fetch('SENTRY_AUTH_TOKEN') do |name|
            raise "Missing environment variable `#{name}`"
          end
      end

      def client
        @client ||= HTTP
                      .use(logging: { logger: http_logger })
                      .auth("Bearer #{token}")
      end

      def http_logger
        logger.tap do |request_logger|
          # Logging at the DEBUG level will log the full request & response headers and bodies.
          # So we default to info level if LOG_LEVEL is not set.
          request_logger.level = ENV.fetch('LOG_LEVEL', 'info').to_sym
        end
      end

      def new_release?(sha)
        client.get("#{API_ENDPOINT}#{sha}/").status.not_found?
      end
    end
  end
end
