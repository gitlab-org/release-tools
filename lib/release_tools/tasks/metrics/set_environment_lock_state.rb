# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Metrics
      # Class used for setting the auto_deploy_lock_state metric
      class SetEnvironmentLockState
        include ::SemanticLogger::Loggable

        METRIC = 'auto_deploy_lock_state'
        LOCK_REASONS = %w[locked_deployment locked_deployment_failed locked_qa locked_qa_failed locked_post_deploy_migration
                          locked_post_deploy_migration_failed].freeze

        def initialize(environment, lock_reason)
          @client = ReleaseTools::Metrics::Client.new
          @environment = environment
          @lock_reason = lock_reason
        end

        def execute
          env, stage = parse_env

          logger.info('Setting environment_lock_state', environment: environment, env: env, stage: stage, lock_reason: lock_reason)
          return if SharedStatus.dry_run?

          if lock_reason
            set_metric(lock_reason, env, stage)
          else
            unset_metrics(env, stage)
          end
        end

        private

        attr_reader :client, :environment, :lock_reason

        def set_metric(lock_reason, env, stage)
          client.set(METRIC, 1, labels: "#{lock_reason},#{env},#{stage}")

          (LOCK_REASONS - [lock_reason]).each do |other_reasons|
            client.set(METRIC, 0, labels: "#{other_reasons},#{env},#{stage}")
          end
        end

        def unset_metrics(env, stage)
          LOCK_REASONS.each do |reason|
            client.set(METRIC, 0, labels: "#{reason},#{env},#{stage}")
          end
        end

        def parse_env
          return [environment, 'main'] unless environment.end_with?('-cny')

          env = environment.delete_suffix('-cny')
          [env, 'cny']
        end
      end
    end
  end
end
