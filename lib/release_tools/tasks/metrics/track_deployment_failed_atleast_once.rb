# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Metrics
      # Publish metrics related to deployment pipeline failures
      class TrackDeploymentFailedAtleastOnce
        def execute
          client.set("deployment_failed_atleast_once", 1, labels: deploy_version)
        end

        private

        def client
          @client ||= ReleaseTools::Metrics::Client.new
        end

        def deploy_version
          ENV.fetch("DEPLOY_VERSION")
        end
      end
    end
  end
end
