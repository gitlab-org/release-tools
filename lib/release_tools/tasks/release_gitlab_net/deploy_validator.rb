# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ReleaseGitlabNet
      # Validates that release.gitlab.net is running the expected release version
      class DeployValidator
        include ::SemanticLogger::Loggable

        UnexpectedReleaseVersionError = Class.new(StandardError)

        def initialize(version)
          @version = version
          @ee_version = Version.new(version).to_ee
        end

        def execute
          Retriable.retriable(
            on: UnexpectedReleaseVersionError,
            base_interval: 60,
            tries: 110,
            max_elapsed_time: 110 * 60, # The maximum duration (seconds) for retries -- 1h 50m
            rand_factor: 0
          ) do
            if expected_version?
              logger.info("Release.gitlab.net is running the expected #{release_type} version", version: ee_version)
              send_slack_notification(:success)
              return
            end

            logger.warn("Release.gitlab.net is not running the expected #{release_type} version. Retrying", expected: ee_version, actual: actual_version)
            raise UnexpectedReleaseVersionError
          end
        rescue UnexpectedReleaseVersionError
          logger.fatal("Release.gitlab.net is not running the expected #{release_type} version. Check that the deploy has successfully completed or if the version reflects in https://release.gitlab.net/help.", expected_version: ee_version, actual_version: actual_version)
          send_slack_notification(:failed)
          raise
        end

        private

        attr_reader :ee_version, :version

        def compare_release_versions
          @compare_release_versions ||= ReleaseTools::Deployments::CompareReleaseVersions.new(ee_version)
        end

        def expected_version?
          compare_release_versions.equal_to_actual_release_version?
        end

        def actual_version
          compare_release_versions.actual_version
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: "Validate release.gitlab.net running #{ee_version}",
            status: status,
            release_type: release_type
          ).send_notification
        end

        def release_type
          patch? ? :patch : :monthly
        end

        def patch?
          ReleaseTools::Version.new(version).patch?
        end
      end
    end
  end
end
