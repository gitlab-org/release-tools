# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ReleaseEnvironment
      class Environment
        include ReleaseTools::Tasks::Helper

        attr_reader :version, :suffix

        def initialize(version, suffix: nil)
          @version = version
          @suffix = suffix
        end

        def execute
          ReleaseTools::ReleaseEnvironment::Environment
          .new(version, suffix)
          .create
        end
      end
    end
  end
end
