# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ReleaseEnvironment
      class QaNotifier
        include ReleaseTools::Tasks::Helper

        attr_reader :pipeline_url, :environment_name, :release_environment_version

        def initialize(pipeline_url:, environment_name:, release_environment_version:)
          @pipeline_url = pipeline_url
          @environment_name = environment_name
          @release_environment_version = release_environment_version
        end

        def execute
          ReleaseTools::ReleaseEnvironment::QaNotifier.new(
            pipeline_url: pipeline_url,
            environment_name: environment_name,
            release_environment_version: release_environment_version
          ).execute
        end
      end
    end
  end
end
