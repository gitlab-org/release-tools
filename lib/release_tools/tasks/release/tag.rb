# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Release
      class Tag
        include ::SemanticLogger::Loggable

        def initialize(version)
          @version = version
        end

        def execute
          if patch?
            logger.warn("Using security repository only!\n")
          end

          logger.info("Starting to tag #{version}")

          return if SharedStatus.dry_run?

          Rake::Task['release:tag'].invoke(version)
          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(error_message(ex), error: ex)
          send_slack_notification(:failed)
          raise
        end

        private

        attr_reader :version

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: "Tag #{release_type} release version #{version}",
            status: status,
            release_type: release_type
          ).send_notification
        end

        def error_message(exception)
          <<~MSG
            Tagging the #{release_type} release failed with error: #{exception.message}.
            Review the error log and consider retrying this job.
          MSG
        end

        def release_type
          patch? ? :patch : :monthly
        end

        def patch?
          ReleaseTools::Version.new(version).patch?
        end
      end
    end
  end
end
