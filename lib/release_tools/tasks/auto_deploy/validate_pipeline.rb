# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class ValidatePipeline
        include ::SemanticLogger::Loggable

        GITLAB_PROJECT = Project::GitlabEe

        def initialize(deploy_version = ENV.fetch('DEPLOY_VERSION'), ignore_failure: ENV.key?('SKIP_VALIDATE_PIPELINE'))
          @deploy_version = deploy_version
          @ignore_failure = ignore_failure
        end

        def execute
          product_version = ProductVersion.from_package_version(@deploy_version)
          log_payload = { product_version: product_version.version, deploy_version: @deploy_version }

          logger.info('current version', log_payload)

          gitlab_sha = product_version[GITLAB_PROJECT]&.sha
          unless gitlab_sha
            logger.error('cannot find the package gitlab SHA', log_payload)

            return
          end

          success = PassingBuild.new(product_version[GITLAB_PROJECT].ref, GITLAB_PROJECT).success_for_auto_deploy_rollout?(gitlab_sha)
          label = if success
                    logger.info('successful pipeline', log_payload.merge(gitlab_sha: gitlab_sha))

                    'success'
                  else
                    logger.error('unsuccessful pipeline', log_payload.merge(gitlab_sha: gitlab_sha))

                    'failed'
                  end

          ReleaseTools::Metrics::Client.new.inc("auto_deploy_gitlab_pipeline_total", labels: label)

          return if @ignore_failure
          return if success

          logger.error('The check failed, to ignore the result re-run with SKIP_VALIDATE_PIPELINE set to any value')
          exit(1)
        end
      end
    end
  end
end
