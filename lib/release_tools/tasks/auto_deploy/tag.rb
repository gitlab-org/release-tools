# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class Tag
        include ::SemanticLogger::Loggable

        def initialize(packagers_only: false)
          @branch = ReleaseTools::AutoDeployBranch.current
          @version = ReleaseTools::AutoDeploy::Tag.generate
          @metadata = ReleaseTools::ReleaseMetadata.new
          @packagers_only = packagers_only
        end

        def execute
          logger.info('Preparing for tagging', branch: branch.to_s, version: version)

          find_passing_build

          omnibus_builder = ReleaseTools::AutoDeploy::Builder::Omnibus.new(branch, commit.id, version, metadata)
          cng_builder = ReleaseTools::AutoDeploy::Builder::CNGImage.new(branch, commit.id, version, metadata)

          if omnibus_builder.changes? || cng_builder.changes?
            logger.info('Tagging a new product version')

            omnibus_builder.execute
            cng_builder.execute
            upload_metadata

            inc_packages_tagging_total_metric

            return if packagers_only

            # this triggers the package rollout in a coordinator pipeline
            tag_coordinator
          else
            logger.info('Nothing to tag on Omnibus or CNG')
          end
        end

        private

        attr_reader :branch, :version, :metadata, :packagers_only

        def find_passing_build
          logger.info('Searching for commit with passing build', branch: branch.to_s)
          commit
        end

        def tag_coordinator
          ReleaseTools::AutoDeploy::Tagger::Coordinator.new(version).tag!
        end

        def upload_metadata
          ReleaseTools::ReleaseMetadataUploader.new.upload(version, metadata, auto_deploy: true)
        end

        def commit
          @commit ||= ReleaseTools::PassingBuild.new(branch.to_s).for_auto_deploy_tag
        end

        def inc_packages_tagging_total_metric
          ReleaseTools::Metrics::Client.new.inc("packages_tagging_total", labels: 'auto_deploy,no')
        end
      end
    end
  end
end
