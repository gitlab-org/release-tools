# frozen_string_literal: true

require_relative '../../support/ubi_helper'
require_relative '../../support/fips_helper'

module ReleaseTools
  module Services
    class CNGPublishService < BasePublishService
      include ReleaseTools::Support::UbiHelper
      include ReleaseTools::Support::FIPSHelper

      def play_stages
        @play_stages ||= %w[release].freeze
      end

      def release_versions
        @release_versions ||= [
          @version.to_ce.tag,
          @version.to_ee.tag,
          ubi_tag(@version.to_ee)
        ].tap do |versions|
          versions << fips_tag(@version.to_ee) if @version >= Version.new(GITLAB_FIPS_MINIMUM_VERSION)
        end
      end

      def project
        @project ||= Project::CNGImage
      end
    end
  end
end
