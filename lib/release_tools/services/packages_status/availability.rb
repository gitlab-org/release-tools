# frozen_string_literal: true

module ReleaseTools
  module Services
    module PackagesStatus
      class Availability < Base
        private

        def bridge_job_name
          'check-packages-availability'
        end
      end
    end
  end
end
