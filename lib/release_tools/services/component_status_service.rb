# frozen_string_literal: true

module ReleaseTools
  module Services
    # Checks the pipeline status of a given project on a given branch
    # Commit depth may be specified
    class ComponentStatusService
      include ::SemanticLogger::Loggable

      MAX_COMMIT_DEPTH = 20 # For gitlab-ee project
      MIN_COMMIT_DEPTH = 1
      VALID_COMPLETION_STATUSES = %w(success failed canceled).freeze

      def initialize(project:, branch:, client: ReleaseTools::GitlabClient, commit_depth: nil, release_type: :monthly)
        @project = project
        @branch = branch
        @client = client
        @commit_depth = commit_depth || default_commit_depth(project, branch)
        @release_type = release_type
      end

      def execute
        commits = Retriable.with_context(:api) do
          client.commits(project_path, ref_name: branch)
        end

        status = nil

        commit_depth.times do
          next_commit = commits.shift
          break unless next_commit

          commit = Retriable.with_context(:api) do
            client.commit(project_path, ref: next_commit.id)
          end

          status = commit&.last_pipeline&.status
          # if the commit is running a pipeline, skipped, or does not have a pipeline
          # keep trying until the commit depth is reached
          break if VALID_COMPLETION_STATUSES.include?(status)
        end

        logger.info(
          'Checking component pipeline status',
          project: project.metadata_project_name,
          project_path: project_path,
          branch: branch,
          status: status
        )

        status
      end

      private

      attr_reader :project, :branch, :client, :commit_depth

      def project_path
        if @release_type == :monthly
          project.path
        else
          project.security_path
        end
      end

      def default_commit_depth(project, branch)
        # gitlab-ee has high commit traffic so we need to check beyond the first commit because
        # there may be several commits with running pipelines at any given time
        if project.metadata_project_name == 'gitlab-ee' && branch == project.default_branch
          MAX_COMMIT_DEPTH
        else
          MIN_COMMIT_DEPTH
        end
      end
    end
  end
end
