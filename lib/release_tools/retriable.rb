# frozen_string_literal: true

require 'retriable'

Retriable.configure do |config|
  config.contexts[:api] = {
    on: [
      Gitlab::Error::ResponseError,
      Timeout::Error,
      Errno::ECONNRESET
    ]
  }

  # Retry with exponential backoff, for a maximum of ~5 minutes
  #
  # Ideal for waiting for a pipeline to be created.
  config.contexts[:pipeline_created] = {
    base_interval: 5,
    tries: 10
  }

  # Retry with exponential backoff, for a maximum of ~17 minutes
  # Sum[30*1.4^n,{n,0,7}] ~= 17 minutes
  #
  # Ideal for waiting for a git sync pipeline to be finished.
  config.contexts[:git_sync_pipeline_finished] = {
    base_interval: 30,
    tries: 8,
    multiplier: 1.4,
    rand_factor: 0.0,
    max_interval: 300
  }

  # Retry every 30 seconds up to 30 times, for a maximum of 15 minutes
  #
  # Ideal for waiting for a running merge-train pipeline to complete.
  # If you want to change the waiting time, override "tries" and "max_elapsed_time" parameters.
  config.contexts[:merge_train_pipeline] = {
    max_elapsed_time: 900,
    tries: 900 / 30,
    base_interval: 30,
    multiplier: 1,
    rand_factor: 0.0
  }

  # Retry every 60 seconds up to 60 times, for a maximum of 60 minutes
  #
  # Ideal for waiting for a running packager pipeline to complete.
  config.contexts[:package_pipeline] = {
    base_interval: 60,
    tries: 60,
    max_elapsed_time: 60 * 60, # The maximum amount of total time in seconds that code is allowed to keep being retried
    rand_factor: 0
  }

  # Retry every 60 seconds up to 60 times, for a maximum of 60 minutes
  #
  # Ideal for waiting for a running release packager pipeline to complete.
  config.contexts[:release_package_pipeline] = {
    base_interval: 60,
    tries: 15,
    max_elapsed_time: 15 * 60, # The maximum amount of total time in seconds that code is allowed to keep being retried
    rand_factor: 0
  }

  # Retry with a very short fixed backoff, for a maximum of 75 minutes
  #
  # Ideal for retrying any tasks that may fail and a retry helps.
  # Timeout is not used since it is not recommended.
  # https://github.com/kamui/retriable?tab=readme-ov-file#options
  config.contexts[:retry_action] = {
    base_interval: 5,
    max_elapsed_time: 75.minutes.to_i, # 5 retries, 15 minutes each
    tries: 5,
    multiplier: 1,
    rand_factor: 0.0
  }

  # Wait for commits to be mirrored. Push mirroring usually occurs in a short period of time.
  # This will retry for approximately 5 minutes before failing.
  config.contexts[:mirroring] = {
    on: [
      Gitlab::Error::ResponseError,
      Timeout::Error,
      Errno::ECONNRESET
    ],
    base_interval: 10,
    multiplier: 1.5,
    tries: 8
  }

  # Retry up to 80 minutes, every 30 seconds. Waits for the build-assets-job
  # on a new dev GitLab pipeline to finish. As of February 2025, this job finishes
  # around minute 50 after the pipeline was created, so waiting for 80 minutes
  # should be more than enough.
  config.contexts[:wait_build_assets_job_on_dev] = {
    max_elapsed_time: 80.minutes.to_i,
    tries: 60.minutes.to_i / 30,
    base_interval: 30,
    multiplier: 1,
    rand_factor: 0.0
  }
end
