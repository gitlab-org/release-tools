# frozen_string_literal: true

require 'gitlab/request'

module ReleaseTools
  # Client for the version.gitlab.com API
  #
  # Because the API so closely resembles the API of GitLab CE/EE, we're able to
  # repurpose the gitlab gem's `Gitlab::Request` class to get automatic error
  # handling and response parsing.
  class VersionClient < Gitlab::Request
    base_uri 'https://version.gitlab.com'
    headers 'Private-Token' => -> { api_token }

    VERSION_TYPES = {
      monthly: 'no',
      patch_regular: 'non_critical',
      patch_critical: 'critical'
    }.freeze

    # Get the latest version information
    #
    # Returns an Array of Gitlab::ObjectifiedHash objects
    def self.versions
      get('/api/v1/versions', query: { per_page: 150 })
    end

    # Get single version using version number
    def self.get_version(version)
      get("/api/v1/versions/v#{version}")
    end

    # Creates a new version
    #
    # Returns the version that's just created
    def self.create_version(version, version_type: :monthly)
      raise ArgumentError, "Invalid security_release_type. Allowed values are: #{VERSION_TYPES.keys.join(', ')}" unless VERSION_TYPES.include?(version_type)

      body_params = {
        version: version,
        security_release_type: VERSION_TYPES[version_type]
      }

      post('/api/v1/versions', body: body_params)
    end

    def self.api_token
      ENV.fetch('RELEASE_BOT_VERSION_TOKEN') do |name|
        raise "Must specify `#{name}` environment variable!"
      end
    end

    private_class_method :api_token
  end
end
