# frozen_string_literal: true

module ReleaseTools
  module Pre
    class Notifier
      include ::SemanticLogger::Loggable
      include ReleaseTools::AutoDeploy::Pipeline

      def initialize(pipeline_id:, deploy_version:)
        @pipeline_id = pipeline_id
        @deploy_version = deploy_version
      end

      def execute
        logger.debug('Executing pre notifier', pipeline_id: pipeline_id, deploy_version: deploy_version)

        pipeline = find_downstream_pipeline

        if pipeline.present?
          logger.info('Downstream pipeline found', deployer_url: pipeline.web_url)

          send_slack_notification(pipeline)
        else
          logger.fatal('Downstream pipeline not found')
        end
      end

      private

      attr_reader :pipeline_id, :deploy_version

      def find_downstream_pipeline
        logger.info('Fetch downstream pipeline', pipeline_id: pipeline_id)

        bridge.downstream_pipeline
      rescue MissingPipelineError
        nil
      end

      def bridge
        logger.info('Fetching bridge from pipeline', pipeline_id: pipeline_id)

        Retriable.with_context(:pipeline_created) do
          bridge = ReleaseTools::GitlabOpsClient
            .pipeline_bridges(Project::ReleaseTools, pipeline_id)
            .lazy_paginate
            .find { |job| job.name == 'pre:deploy' }

          raise MissingPipelineError unless bridge&.downstream_pipeline

          bridge
        end
      end

      def send_slack_notification(pipeline)
        ReleaseTools::Slack::PreNotification.new(
          deploy_version: deploy_version,
          pipeline: pipeline
        ).execute
      end
    end
  end
end
