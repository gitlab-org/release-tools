# frozen_string_literal: true

module ReleaseTools
  module Pre
    class ReleaseNote
      include ::SemanticLogger::Loggable

      def initialize(pipeline_id:, deploy_version:)
        @pipeline_id = pipeline_id
        @deploy_version = deploy_version
      end

      def execute
        logger.info(
          'Posting pre note on the monthly issue',
          pipeline: pipeline_url,
          deploy_version: deploy_version,
          issue: monthly_issue.url
        )

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          GitlabClient.create_issue_note(
            monthly_issue.project,
            issue: monthly_issue,
            body: body
          )
        end
      end

      private

      attr_reader :pipeline_id, :deploy_version

      def pipeline
        Retriable.with_context(:api) do
          ReleaseTools::GitlabOpsClient.pipeline(Project::ReleaseTools, pipeline_id)
        end
      end

      def post_note_on_monthly_issue
        Retriable.with_context(:api) do
          GitlabClient.create_issue_note(
            monthly_issue.project,
            issue: monthly_issue,
            body: body
          )
        end
      end

      def release_manager
        logger.info('Fetching release manager', pipeline_url: pipeline_url)

        username = pipeline.user.username

        user = ReleaseManagers::Definitions.new.find_user(username, instance: :ops)
        return ":warning: Unknown release manager! OPS username #{username}" if user.nil?

        "@#{user.production}"
      end

      def pipeline_url
        pipeline.web_url
      end

      def monthly_issue
        @monthly_issue ||=
          begin
            monthly_version = ReleaseManagers::Schedule.new.active_version
            ReleaseTools::MonthlyIssue.new(version: monthly_version)
          end
      end

      def body
        ERB
          .new(template, trim_mode: '-')
          .result(binding)
      end

      def template
        File.read(
          File.expand_path('../../../templates/pre_note.md.erb', __dir__)
        )
      end
    end
  end
end
