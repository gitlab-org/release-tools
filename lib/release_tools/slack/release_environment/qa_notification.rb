# frozen_string_literal: true

require 'release_tools/time_util'

module ReleaseTools
  module Slack
    module ReleaseEnvironment
      class QaNotification
        include ::SemanticLogger::Loggable
        include ReleaseTools::TimeUtil
        include Utilities

        def initialize(job:, environment_name:, release_environment_version:)
          @job = job
          @environment_name = environment_name
          @release_environment_version = release_environment_version
        end

        def execute
          params = {
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: fallback_text,
            blocks: slack_block
          }

          logger.info('Sending slack notification',
                      job: job.web_url,
                      environment_name: environment_name,
                      release_environment_version: release_environment_version,
                      slack_channel: params[:channel])

          ReleaseTools::Slack::Message.post(**params)
        end

        private

        attr_reader :release_environment_version, :environment_name, :job

        def fallback_text
          "QA Release Environment #{environment_name}: failed #{release_environment_version}"
        end

        def slack_block
          blocks = ::Slack::BlockKit.blocks
          blocks.section { |block| block.mrkdwn(text: section_block) }

          blocks.context do |block|
            block.mrkdwn(text: clock_context_element[:text])
          end

          blocks.as_json
        end

        def section_block
          [].tap do |text|
            text << status_icon
            text << "QA Release Environment *#{environment_name}*"
            text << "<#{job.web_url}|failed>"
            text << "`#{release_environment_version}`"
          end.join(' ')
        end

        def status_icon
          STATUS_ICONS[:failed]
        end
      end
    end
  end
end
