# frozen_string_literal: true

module ReleaseTools
  module Slack
    module Utilities
      module_function

      STATUS_ICONS = {
        started: ':ci_running:',
        finished: ':ci_passing:',
        failed: ':ci_failing:',
        unknown: ':warning:'
      }.freeze

      ENVIRONMENT_ICONS = {
        'gstg-ref': ':construction:',
        'gstg-cny': ':hatching_chick:',
        gstg: ':building_construction:',
        'gprd-cny': ':canary:',
        gprd: ':party-tanuki:',
        pre: ':pretzel:'
      }.freeze

      DEPLOYER_STATUS_SUCCESS = 'finished'
      DEPLOYER_STATUS_FAILED = 'failed'
      DEPLOYER_STATUS_STARTED = 'started'

      # Formats hyperlinked text as required by the Slack API.
      def api_link(text, url)
        "<#{url}|#{text}>"
      end

      private

      def deployer_status
        return 'unknown' if pipeline.nil?

        case pipeline.status
        when 'success'
          DEPLOYER_STATUS_SUCCESS
        when 'failed', 'canceled'
          DEPLOYER_STATUS_FAILED
        else
          DEPLOYER_STATUS_STARTED
        end
      end

      def environment_icon
        ENVIRONMENT_ICONS[environment.to_sym]
      end

      def status_icon
        STATUS_ICONS[deployer_status.to_sym]
      end

      def clock_context_element
        { type: 'mrkdwn', text: ":clock1: #{current_time.strftime('%Y-%m-%d %H:%M')} UTC" }
      end

      def current_time
        Time.now.utc
      end

      def pipeline_url
        ENV.fetch('CI_PIPELINE_URL')
      end

      def user_name
        ENV.fetch('GITLAB_USER_LOGIN')
      end
    end
  end
end
