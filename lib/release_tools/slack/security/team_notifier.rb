# frozen_string_literal: true

module ReleaseTools
  module Slack
    module Security
      class TeamNotifier
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::IssueHelper

        CouldNotNotifyError = Class.new(StandardError)

        def initialize(team:)
          @versions = ReleaseTools::Versions.next_versions.join(', ')
          @team = team
        end

        def execute
          channel = slack_channels.fetch(team, nil)

          raise CouldNotNotifyError, error_message unless channel

          logger.info(
            'Notifying team about the patch release',
            team: team,
            security_tracking_issue: security_tracking_issue.id,
            versions: versions
          )

          ReleaseTools::Slack::Message.post(
            channel: channel,
            message: fallback_message,
            blocks: slack_blocks
          )
        end

        private

        attr_reader :versions, :team

        def slack_channels
          {
            developer_experience: ReleaseTools::Slack::S_DEVELOPER_EXPERIENCE,
            development_analytics: ReleaseTools::Slack::G_DEVELOPMENT_ANALYTICS,
            runner: ReleaseTools::Slack::G_RUNNER,
            releases: ReleaseTools::Slack::RELEASES,
            dedicated: ReleaseTools::Slack::G_DEDICATED_TEAM
          }
        end

        def fallback_message
          "The patch release has started (#{reference_issue.web_url}) targeting #{reference_issue.due_date}. We will be releasing the following packages: #{versions}"
        end

        def slack_blocks
          [
            {
              type: 'section',
              text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
            }
          ]
        end

        def section_block
          [].tap do |text|
            text << ":mega: :security-tanuki:"
            text << "Hello, the *<#{reference_issue.web_url}|patch release> has started"
            text << "targeting #{reference_issue.due_date} as the due date.*"
            text << "We will be releasing the following packages: *#{versions}.*"
            text << "\n"
            text << "A message will be posted to this channel when the patch release is complete."
            text << "\n"
            text << "For more information about the patch release, check out the 'Release Information' dashboard: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1"
          end.join(' ')
        end

        def error_message
          <<~MSG
            Notifying #{team} failed. Ensure the team is included in the slack_channels list and retry this job.
            If the job continues to fail, proceed to post the message on Slack manually.
          MSG
        end
      end
    end
  end
end
