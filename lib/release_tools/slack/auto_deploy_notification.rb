# frozen_string_literal: true

module ReleaseTools
  module Slack
    class AutoDeployNotification < Webhook
      def self.webhook_url
        ENV.fetch('AUTO_DEPLOY_NOTIFICATION_URL')
      end

      def self.on_create(results)
        return unless results.any?

        branch = results.first.branch
        fallback = "New auto-deploy branch: `#{branch}`"

        if results.any?(&:notify_branch_too_far_behind)
          fallback += "\n@release-managers Auto deploy branches with :warning: next to them are too far behind the default branch. " \
                      "Please check and notify the appropriate groups."
        end

        blocks = ::Slack::BlockKit.blocks

        blocks.section { |section| section.mrkdwn(text: fallback) }

        blocks.section do |section|
          results.each do |result|
            text = "<#{commits_url(result.project, branch)}|#{result.project}>"

            text.prepend(":warning: ") if result.notify_branch_too_far_behind

            section.mrkdwn_field(text: text.freeze)
          end
        end

        if results.any?(&:notify_branch_too_far_behind)
          blocks.divider

          blocks.context do |context|
            docs_url = 'https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/how-to-fix-auto-deploy-branch-behind-master.md'
            context.mrkdwn(text: "<#{docs_url}|Runbook for handling branch behind master>")
          end
        end

        if ENV['CI_JOB_URL']
          blocks.context { |context| context.mrkdwn(text: "Created via <#{ENV['CI_JOB_URL']}|scheduled pipeline>.") }
        end

        fire_hook(text: fallback, blocks: blocks.as_json, channel: F_UPCOMING_RELEASE)
      end

      def self.on_wait_failure(exception)
        text = "#{exception.message}."

        blocks = ::Slack::BlockKit.blocks
        blocks.section do |block|
          block.mrkdwn(text: "<!subteam^#{RELEASE_MANAGERS}> #{text}")
        end

        blocks.context do |block|
          block.mrkdwn(text: "Job: <#{ENV.fetch('CI_JOB_URL', nil)}>")

          if exception.pipeline
            block.mrkdwn(text: "Pipeline: <#{exception.pipeline.web_url}>")
          end
        end

        fire_hook(text: text, blocks: blocks.as_json, channel: F_UPCOMING_RELEASE)
      end

      def self.on_stale_gitaly_merge_request(merge_request)
        text = "A merge request to update the Gitaly version has been open for too long"
        attachment = {
          fallback: text,
          color: 'warning',
          title: merge_request.title,
          title_link: merge_request.url
        }

        fire_hook(text: text, attachments: [attachment], channel: GITALY_ALERTS)
      end

      def self.on_stale_kas_merge_request(merge_request)
        text = "A merge request to update the KAS version has been open for too long"
        attachment = {
          fallback: text,
          color: 'warning',
          title: merge_request.title,
          title_link: merge_request.url
        }

        fire_hook(text: text, attachments: [attachment], channel: F_AGENT_FOR_KUBERNETES)
      end

      def self.commits_url(project, branch)
        "https://gitlab.com/#{project}/-/commits/#{branch}"
      end
    end
  end
end
