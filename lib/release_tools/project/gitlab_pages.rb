# frozen_string_literal: true

module ReleaseTools
  module Project
    class GitlabPages < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-org/gitlab-pages.git',
        dev:       'git@dev.gitlab.org:gitlab/gitlab-pages.git',
        security:  'git@gitlab.com:gitlab-org/security/gitlab-pages.git'
      }.freeze

      IDS = {
        canonical: 734_943,
        security: 15_685_887
      }.freeze

      def self.version_file
        'GITLAB_PAGES_VERSION'
      end
    end
  end
end
