# frozen_string_literal: true

module ReleaseTools
  module Security
    class ImplementationIssueValidator
      MisMatchVersionsError = Class.new(StandardError)

      attr_reader :errors

      # @param [ReleaseTools::Security::ImplementationIssue] issue
      def initialize(issue)
        @issue = issue
        @errors = []
      end

      def validate
        validate_cves_information

        return unless cves.present?

        validate_cves_versions_presence
        validate_cves_affected_versions
        validate_cves_fixed_versions
      end

      def validate_cves_information
        if cves.nil?
          error('Missing CVES issue. The CVES issue must be linked to this issue', <<~ERROR)
            The security implementation issue doesn't have a CVES issue associated.
            Link the CVES issue to this issue or to the canonical issue.
          ERROR
        elsif !cves.yaml_present?
          error('Missing YAML', <<~ERROR)
            The [CVES issue](#{cves.web_url}) associated does not have YAML defined.
          ERROR
        elsif !cves.valid_yaml?
          error('Invalid YAML', <<~ERROR)
            The YAML defined on the [CVES issue](#{cves.web_url}) is invalid.

            ```
            #{invalid_yaml(cves)}
            ```
          ERROR
        end
      end

      def validate_cves_versions_presence
        if cves.pending_affected_versions?
          error('Affected versions missing', <<~ERROR)
            The [CVES issue](#{cves.web_url}) does not have `affected_versions` defined.
          ERROR
        end

        return unless cves.pending_fixed_versions?

        error('Fixed versions missing', <<~ERROR)
          The [CVES issue](#{cves.web_url}) does not have `fixed_versions` defined.
        ERROR
      end

      def validate_cves_affected_versions
        return if cves.pending_affected_versions?

        validate_security_versions(cves.affected_versions, 'affected_versions')
      end

      def validate_cves_fixed_versions
        return if cves.pending_fixed_versions?

        validate_security_versions(cves.fixed_versions, 'fixed_versions')
      end

      private

      attr_reader :issue

      # @param [String] summary
      # @param [String] details
      def error(summary, details)
        @errors << <<~HTML
          <details>
          <summary><strong>#{summary}</strong></summary>
          <br />

          #{details}

          </details>
        HTML
      end

      def invalid_yaml(cves)
        YAML.safe_load(cves.yaml_str)
      rescue Psych::DisallowedClass, Psych::BadAlias, Psych::SyntaxError => ex
        ex.inspect
      end

      def cves
        @cves ||= issue.cves_issue
      end

      def validate_security_versions(defined_versions, versions_type)
        security_versions.each do |version|
          raise MisMatchVersionsError if defined_versions.grep(/#{version.to_minor}/).empty?
        end
      rescue MisMatchVersionsError
        error("`#{versions_type}` don't match the patch release versions", <<~ERROR)
          The `#{versions_type}` defined on [CVES issue](#{cves.web_url}) does not match the patch release versions:

          * `#{versions_type}`: #{formatted_versions(defined_versions)}
          * patch release versions: #{formatted_versions(security_versions)}
        ERROR
      end

      def formatted_versions(versions)
        versions
          .map { |version| "`#{version}`" }
          .join(', ')
      end

      def security_versions
        @security_versions ||= ReleaseTools::Versions.next_versions
      end
    end
  end
end
