# frozen_string_literal: true

module ReleaseTools
  module Security
    class SyncRemotesService
      include ::SemanticLogger::Loggable
      include ReleaseTools::Services::SyncRefsHelper

      CouldNotSyncError = Class.new(StandardError)

      # Syncs default branch across all remotes
      def initialize
        @projects = [
          ReleaseTools::Project::GitlabEe,
          ReleaseTools::Project::GitlabCe,
          ReleaseTools::Project::OmnibusGitlab,
          ReleaseTools::Project::Gitaly,
          ReleaseTools::Project::HelmGitlab,
          ReleaseTools::Project::GitlabOperator,
          ReleaseTools::Project::GitlabPages
        ]

        @projects << ReleaseTools::Project::Kas if Feature.enabled?(:release_the_kas)
      end

      def execute
        @projects.each do |project|
          next if merge_train_to_canonical_enabled?(project)

          branches = [project.default_branch]

          logger.info('Syncing branches', project: project, branches: branches)
          sync_branches(project, *branches, create_mr_on_failure: true)
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)
          send_slack_notification(:failed)

          raise CouldNotSyncError
        end

        send_slack_notification(:success)
      end

      private

      def send_slack_notification(status)
        ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: 'Sync remotes',
          status: status,
          release_type: :patch
        ).send_notification
      end

      def failure_message
        <<~MSG
          Sync remotes failed. If this job continues to fail, the sync can be run using chatops:
          `/chatops run release sync_remotes --security`.
        MSG
      end

      def global_depth
        200
      end

      def merge_train_to_canonical_enabled?(project)
        Feature.enabled?(:merge_train_to_canonical) &&
          project == ReleaseTools::Project::GitlabEe
      end
    end
  end
end
