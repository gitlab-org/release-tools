# frozen_string_literal: true

# This class automate the following manual steps:
# 1. Disable the gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master pipeline schedule
# 2. Trigger the gitlab-org/security/gitlab@master -> gitlab-org/gitlab@master pipeline schedule.
# 3. Review if the repositories are synced
# 4. If the repositories are not synced, repeat step 2 and 3.

module ReleaseTools
  module Security
    module Finalize
      class SyncSecurityToCanonical
        include ::SemanticLogger::Loggable

        PipelineError = Class.new(StandardError)
        PipelineInProgressError = Class.new(StandardError)

        SCHEDULE_SYNC_CANONICAL_SECURITY_ID = 110

        def initialize
          @merge_train_project = Project::MergeTrain
          @gitlab_ee_project = Project::GitlabEe
          @canonical_security ||= PipelineSchedule.new(
            project: merge_train_project,
            id: SCHEDULE_SYNC_CANONICAL_SECURITY_ID
          )
          @client = GitlabOpsClient
        end

        def execute
          logger.info("Starting to sync gitlab-org/security/gitlab@master -> gitlab-org/gitlab@master")

          return if SharedStatus.dry_run?

          deactivate_pipeline_schedule

          # Try to sync 5 times
          Retriable.with_context(:retry_action) do
            pipeline_variables = {
              MERGE_SECURITY: '1',
              SOURCE_PROJECT: gitlab_ee_project.security_path,
              SOURCE_BRANCH: gitlab_ee_project.default_branch,
              TARGET_PROJECT: gitlab_ee_project.path,
              TARGET_BRANCH: gitlab_ee_project.default_branch
            }

            pipeline = client.create_pipeline(merge_train_project, pipeline_variables)
            wait_for_merge_train_pipeline_to_complete(pipeline)
          end

          logger.info("Successfully synced gitlab-org/security/gitlab@master -> gitlab-org/gitlab@master")

          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal("Failed to sync gitlab-org/security/gitlab@master -> gitlab-org/gitlab@master", error: ex)
          logger.fatal("Manual action is required:\n#{manual_action_guide}")
          send_slack_notification(:failed)
          raise
        end

        private

        attr_reader :merge_train_project, :gitlab_ee_project, :canonical_security, :client

        def wait_for_merge_train_pipeline_to_complete(pipeline)
          # Wait for the pipeline to complete
          # If the pipeline fails, stop the process and raise an error
          Retriable.with_context(
            :merge_train_pipeline,
            on: PipelineInProgressError
          ) do
            status = client.pipeline(merge_train_project, pipeline.id).status

            case status
            when 'success'
              logger.info('The pipeline finished', pipeline: pipeline.web_url)
              return
            when 'running', 'pending', 'created'
              logger.info('The pipeline is still in progress', pipeline.web_url)
              raise PipelineInProgressError
            else
              logger.error('Something went wrong with the pipeline', pipeline.web_url)
              raise PipelineError
            end
          end
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Sync from security to canonical',
            status: status,
            release_type: :patch
          ).send_notification
        end

        def manual_action_guide
          <<~GUIDE
            - Disable the `merge_train_to_canonical` feature flag on ops: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags.
            - Enable the `gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master` pipeline schedule on the merge-train: https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules.
            - Execute the `sync_remotes` task on Slack: `/chatops run release sync_remotes --security`. In this case, if the sync fails, a merge request will be created and release manager intervention will be required.
          GUIDE
        end

        def deactivate_pipeline_schedule
          logger.info('Taking ownership of the pipeline schedule',
                      project: merge_train_project.ops_path,
                      pipeline_schedule: canonical_security.id)
          canonical_security.take_ownership

          logger.info('Deactivating pipeline schedule', pipeline_schedule: canonical_security.description)
          canonical_security.deactivate
        end
      end
    end
  end
end
