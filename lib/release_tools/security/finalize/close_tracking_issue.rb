# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class CloseTrackingIssue
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::IssueHelper

        CouldNotCompleteError = Class.new(StandardError)

        def initialize
          @client = ReleaseTools::GitlabClient
        end

        def execute
          if security_tracking_issue.present?
            close_security_tracking_issue
            send_slack_notification(:success)
          else
            logger.fatal('Security tracking issue not found')

            send_slack_notification(:failed)

            raise CouldNotCompleteError
          end
        rescue Gitlab::Error::Error => ex
          logger.fatal(error_message, error: ex)

          send_slack_notification(:failed)

          raise CouldNotCompleteError
        end

        private

        attr_reader :client

        def close_security_tracking_issue
          logger.info('Closing security tracking issue', issue: security_tracking_issue.web_url)

          return if SharedStatus.dry_run?

          client.close_issue(project, security_tracking_issue)
        end

        def project
          ReleaseTools::Project::GitlabEe
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Close Tracking Issue',
            status: status,
            release_type: :patch
          ).send_notification
        end

        def error_message
          <<~MSG
            Updating the security tracking issue failed, review the error log and consider
            retrying this job. If the failure persist, manually close the tracking issue on the GitLab
            canonical repository.
          MSG
        end
      end
    end
  end
end
