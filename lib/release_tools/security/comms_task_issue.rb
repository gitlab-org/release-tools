# frozen_string_literal: true

module ReleaseTools
  module Security
    class CommsTaskIssue < ReleaseTools::Issue
      include ::SemanticLogger::Loggable
      include ReleaseTools::Security::IssueHelper

      def title
        "Patch release: #{versions_title}"
      end

      def confidential?
        true
      end

      def labels
        [
          'Security Release Blog Alert::development',
          'SecComms::AppSec',
          'Security_Customer_Communications',
          'SC-FY23Q3',
          'FY23-SecComms',
          'MktgOps::00: Triage',
          'Marketo',
          'Security Alert - Customer Communications',
          'email-calendar',
          'operational-email'
        ].join(', ')
      end

      def versions_title
        versions.join(', ')
      end

      def due_date
        security_tracking_issue.due_date
      end

      def assignees
        nil
      end

      def project
        ReleaseTools::Project::GlSecurity::SecurityCommunications::Communications
      end

      private

      def template_path
        File.expand_path('../../../templates/patch_release/comms_task_issue.md.erb', __dir__)
      end

      def versions
        ReleaseTools::PatchRelease::Coordinator.new.versions
      end
    end
  end
end
