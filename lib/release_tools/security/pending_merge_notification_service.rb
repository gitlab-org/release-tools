# frozen_string_literal: true

module ReleaseTools
  module Security
    # Notifies if there are pending default branch merge requests for security implementation issues
    # Call this service a while after the MWPS has been set, as it does not wait
    class PendingMergeNotificationService
      include ::SemanticLogger::Loggable

      PendingMergeError = Class.new(StandardError)

      def initialize
        @client = ReleaseTools::Security::Client.new
        @statuses = []
      end

      def execute
        collect_issue_statuses

        if statuses.all? { |obj| obj[:issue_status] == 'success' }
          logger.info('All default merge requests have been successfully merged! \u{2705}')
          send_slack_notification(:success)
        else
          logger.info(log_failure_message)

          raise PendingMergeError
        end
      rescue StandardError => ex
        error_message = "Pending merge check failed. If this job continues to fail, the status of the security implementation issues and their default MRs should be checked manually."
        logger.fatal(error_message, error: ex) unless ex.is_a?(PendingMergeError)

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :client, :statuses

      def security_implementation_issues
        Security::IssueCrawler
          .new
          .upcoming_security_issues_and_merge_requests
      end

      def collect_issue_statuses
        security_implementation_issues.each do |security_issue|
          validate_default_branch_merge_requests(security_issue)

          mr = security_issue.merge_request_targeting_default_branch
          status = security_issue.default_merge_request_handled? ? 'success' : 'failed'

          statuses << {
            issue_url: security_issue.web_url,
            mr_url: mr&.web_url,
            issue_status: status
          }
        end

        statuses
      end

      # The workflow is as follow:
      # - When there are more than one MR targeting the default branch, (and the valid one must be open waiting for us to merge), the task fails
      # - The RM checks and merges the right one manually
      # - The RM reruns this task, this time since all MRs targeting the default branch were merged, the task succeeds.
      def validate_default_branch_merge_requests(security_issue)
        return if security_issue.merge_requests_targeting_default_branch.one?

        if security_issue.opened_merge_requests_targeting_default_branch.length.positive?

          error_message = <<~MSG
            More than one merge request targets the default branch.
            It means at least one merge request is incorrectly linked to the issue.
            Please check and manually merge the correct merge request targeting the default branch,
            then rerun this job.

            In case the incorrectly linked MR was not merged or cannot be merged,
            the issue author needs to create another security issue using the template
            https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md?ref_type=heads\n"
          MSG

          logger.fatal(error_message, issue: security_issue.web_url)
          raise PendingMergeError, 'There are more than one merge requests targeting the default branch.'
        else
          logger.info('There are more than one merge requests targeting the default branch. All were merged, so continuing to process.')
        end
      end

      def send_slack_notification(status)
        ::ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: 'Pending merge',
          status: status,
          release_type: :patch
        ).send_notification
      end

      def log_failure_message
        failed_statuses =
          statuses.filter_map do |obj|
            next if obj[:issue_status] == "success"

            if obj[:mr_url].nil?
              "- Issue: #{obj[:issue_url]} - No default merge request found"
            else
              "- Issue: #{obj[:issue_url]} - Merge Request: #{obj[:mr_url]}"
            end
          end.compact

        message = "\n\n\u{274C} The following issues have not had their default merge requests successfully merged:\n\n"

        message + failed_statuses.join("\n")
      end
    end
  end
end
