# frozen_string_literal: true

module ReleaseTools
  module Security
    # Validation of security issues linked to next patch release tracking issue
    class IssueLinksValidator
      include ::SemanticLogger::Loggable
      include ParallelMethods
      include ::ReleaseTools::Security::IssueHelper

      # The namespace that security issues must reside in.
      SECURITY_NAMESPACE = 'gitlab-org/security'

      LATE_ISSUE_COMMENT_TEMPLATE = <<~TEMPLATE.strip
        The following security issues have been unlinked from this tracking issue
        since they were added after the cutoff time of %<cutoff_time>s.

        If it is absolutely essential that these security issues be included in this
        patch release, they can be linked as blockers to this patch release.

        See the [patch release process docs](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md#planned-security-releases)
        for more information about patch release due dates.

        %<issues_strings>s

        #{Messaging::COMMENT_FOOTNOTE}
      TEMPLATE

      NON_SECURITY_ISSUE_COMMENT_TEMPLATE = <<~TEMPLATE.strip
        The following issues have been unlinked from this tracking issue
        since they were not from the security namespace.

        We should only associate issues that are created on [GitLab Security](https://gitlab.com/gitlab-org/security/) projects.

        %<issues_strings>s

        #{Messaging::COMMENT_FOOTNOTE}
      TEMPLATE

      TIME_FORMAT = '%b %-d, %Y, %H:%M %Z'

      def initialize(client)
        @client = client
      end

      def execute
        remove_issues_added_after_cutoff
        remove_non_security_issues
      end

      private

      def remove_issues_added_after_cutoff
        return unless Feature.enabled?(:unlink_late_security_issues)

        # Terminate early if we've not yet reached the cutoff time.
        return if Time.current < issue_linking_cutoff_time

        logger.debug('Checking security issues added after cutoff', cutoff: issue_linking_cutoff_time, current_time: Time.current.utc)

        linked_security_issues = IssueCrawler.new.security_issues_for(security_tracking_issue.iid)

        late_issues =
          linked_security_issues.select do |issue|
            # Do not unlink security issues that block the patch release.
            next if issue.link_type == 'is_blocked_by'

            Time.iso8601(issue.link_created_at) > issue_linking_cutoff_time
          end

        if late_issues.present?
          remove_issues(late_issues, "late")
          add_comment_about_unlinked_issues(late_issues, LATE_ISSUE_COMMENT_TEMPLATE)
        else
          logger.info('No late issues to remove')
        end
      end

      def remove_non_security_issues
        return unless Feature.enabled?(:unlink_non_security_issues)

        logger.debug('Checking linked non-security issues')

        non_security_issues =
          tracking_issue_links.reject do |issue|
            issue.web_url.include?(SECURITY_NAMESPACE)
          end

        if non_security_issues.present?
          remove_issues(non_security_issues, "non-security")
          add_comment_about_unlinked_issues(non_security_issues, NON_SECURITY_ISSUE_COMMENT_TEMPLATE)
        else
          logger.info('No non-security issues to remove')
        end
      end

      def issue_linking_cutoff_time
        @issue_linking_cutoff_time ||= Date.parse(security_tracking_issue.due_date).in_time_zone('UTC') - 1.day
      end

      def remove_issues(issues, reason)
        parallel_map(issues) do |issue|
          logger.info("Removing #{reason} issue", issue_url: issue.web_url, issue_link_created_at: issue.link_created_at)

          delete_issue_link(issue) unless ReleaseTools::SharedStatus.dry_run?

          issue
        end
      end

      def add_comment_about_unlinked_issues(issues, template)
        issues_strings = issues.collect { |issue| comment_issue_string(issue) }

        comment_body = format(
          template,
          cutoff_time: issue_linking_cutoff_time.strftime(TIME_FORMAT),
          issues_strings: issues_strings.join("\n")
        )

        logger.debug('Adding comment about removed issues', body: comment_body)

        return if ReleaseTools::SharedStatus.dry_run?

        @client.create_issue_note(
          security_tracking_issue.project_id,
          security_tracking_issue.iid,
          comment_body
        )
      end

      def comment_issue_string(issue)
        issue_usernames_string =
          if issue.assignees.present?
            issue.assignees.collect { |user| "@#{user.username}" }.join(', ')
          else
            "@#{issue.author.username}"
          end

        created_at = Time.iso8601(issue.link_created_at).utc.strftime(TIME_FORMAT)

        "1. #{issue.web_url} added on #{created_at} - #{issue_usernames_string}"
      end

      def delete_issue_link(issue)
        Retriable.with_context(:api) do
          GitlabClient
            .client
            .delete_issue_link(issue.project_id, issue.iid, issue.issue_link_id)
        end
      end

      def tracking_issue_links
        @tracking_issue_links ||= Retriable.with_context(:api) do
          @client.issue_links(ReleaseTools::Project::GitlabEe.path, security_tracking_issue.iid)
        end
      end
    end
  end
end
