# frozen_string_literal: true

module ReleaseTools
  module Security
    # Toggle the Canonical to Security merge train based on mirror status
    #
    # When the built-in push mirroring is failing to mirror the main branch from
    # Canonical to Security, we want to fall back to our custom merge train in
    # order to keep development flowing.
    #
    # Once the mirroring has been resolved, we can toggle the merge train back
    # off until it's needed again.
    class MergeTrainService
      include ::SemanticLogger::Loggable

      # Map a project to the scheduled task handling its merge-train execution
      # for default branch.
      # See https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
      PROJECTS = {
        Project::GitlabEe => 110,
        Project::OmnibusGitlab => 123,
        Project::Gitaly => 311
      }.freeze

      # Map a project to the scheduled tasks handling its merge-train execution
      # for stable branches.
      # The schedule ID list is in the order N-1, N-2
      # See https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
      PROJECTS_STABLE_BRANCHES = {
        Project::GitlabEe => [472, 473]
      }.freeze

      # hourly
      CRON = '0 * * * *'

      def initialize
        @client = GitlabOpsClient
        @remote_mirrors_cache = {}
      end

      def execute
        PROJECTS.each do |project, schedule_id|
          logger.info("Starting merge-train service for default branch", project: project)

          next if ReleaseTools::SharedStatus.dry_run?

          run_merge_train_default_branch(project, schedule_id)
        end

        return unless Feature.enabled?(:internal_release_merge_train_stable_branches)

        PROJECTS_STABLE_BRANCHES.each do |project, schedule_ids|
          logger.info("Starting merge-train service for stable branches", project: project)

          next if ReleaseTools::SharedStatus.dry_run?

          run_merge_train_stable_branches(project, schedule_ids)
        end
      end

      private

      attr_reader :client

      # Toggle a merge-train pipeline schedule for default branch if needed
      def run_merge_train_default_branch(project, schedule_id)
        required = merge_train_required?(project)
        active = merge_train_active?(schedule_id)

        # If these are the same, we've already achieved the desired result
        return if required == active

        pipeline_schedule_take_ownership(schedule_id)

        toggle_merge_train(schedule_id, required)
      end

      # Toggle a merge-train pipeline schedule for stable branches.
      # Useful to keep N-1 and N-2 stable branches up to date after an internal release.
      def run_merge_train_stable_branches(project, schedule_ids)
        branches = internal_release_stable_branches(project)
        branch_schedule_pairs = branches.zip(schedule_ids).to_h
        logger.info(branch_schedule_pairs)

        branch_schedule_pairs.each do |branch, schedule_id|
          logger.info("Checking if merge-train is required", project: project, branch: branch)

          required = merge_train_required?(project, branch: branch)
          active = merge_train_active?(schedule_id)

          # If these are the same, we've already achieved the desired result
          next if required == active

          pipeline_schedule_take_ownership(schedule_id)

          # Update the variables of the pipeline schedule
          update_pipeline_schedule_variables(schedule_id, branch)

          toggle_merge_train(schedule_id, required)
        end
      end

      # @return [Array<String>]
      def internal_release_stable_branches(project)
        InternalRelease::Coordinator.new.versions.map { |n| n.stable_branch(ee: project.ee_branch?) }
      end

      def merge_train_active?(schedule_id)
        client
          .pipeline_schedule(Project::MergeTrain, schedule_id)
          .active
      end

      def get_remote_mirrors(project)
        @remote_mirrors_cache[project] ||=
          Retriable.with_context(:api) do
            GitlabClient.remote_mirrors(project)
          end
      end

      def merge_train_required?(subject_project, branch: subject_project.default_branch)
        mirror = get_remote_mirrors(subject_project)
          .detect { |m| m.url.include?('gitlab-org/security') }

        if mirror.nil?
          logger.fatal('Unable to detect project security mirror', project: subject_project)
          return false
        end

        return false if mirror.last_error.blank?

        mirror.last_error
          .split("\n")
          .any? { |l| l.include?("refs/heads/#{branch}") }
      rescue ::Gitlab::Error::Error => ex
        logger.warn(
          'Unable to get project mirror status',
          project: subject_project,
          error: ex.message
        )

        false
      end

      def toggle_merge_train(schedule_id, active)
        logger.info('Toggling the merge-train scheduled task', id: schedule_id, active: active)

        params = {
          active: active,
          cron: CRON
        }

        schedule = client.edit_pipeline_schedule(
          Project::MergeTrain.ops_path,
          schedule_id,
          **params
        )

        ReleaseTools::Slack::MergeTrainNotification.toggled(schedule)
      end

      def update_pipeline_schedule_variables(schedule_id, branch)
        client.edit_pipeline_schedule_variable(Project::MergeTrain, schedule_id, "SOURCE_BRANCH", { value: branch })
        client.edit_pipeline_schedule_variable(Project::MergeTrain, schedule_id, "TARGET_BRANCH", { value: branch })
      end

      def pipeline_schedule_take_ownership(schedule_id)
        client.pipeline_schedule_take_ownership(Project::MergeTrain, schedule_id)
      end
    end
  end
end
