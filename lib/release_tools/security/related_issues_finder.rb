# frozen_string_literal: true

module ReleaseTools
  module Security
    class RelatedIssuesFinder
      def initialize(issue)
        @security_issue = issue
      end

      def cves_issue
        issues.each do |issue|
          cve_issue = fetch_cve_issue(issue)

          return cve_issue if cve_issue.present?
        end

        nil
      end

      def canonical_issue
        canonical_path = security_issue.reference.sub('security/', '').split('#').first

        issue_links(project_id: security_issue.project_id, issue_iid: security_issue.iid)
          .find { |linked_issue| full_reference(linked_issue).include?(canonical_path) }
      end

      private

      attr_reader :security_issue

      def issues
        [security_issue, canonical_issue].compact
      end

      def fetch_cve_issue(issue)
        issue_links(project_id: issue.project_id, issue_iid: issue.iid)
          .find { |linked_issue| full_reference(linked_issue).include?(cve_path) }
      end

      def issue_links(project_id:, issue_iid:)
        Retriable.with_context(:api) do
          client.issue_links(
            project_id,
            issue_iid
          )
        end
      end

      def full_reference(issue)
        issue.references.full
      end

      def cve_path
        ReleaseTools::Project::Cves.to_s
      end

      def client
        ReleaseTools::GitlabClient
      end
    end
  end
end
