# frozen_string_literal: true

module ReleaseTools
  module Security
    class ManagedVersioningNotificationService
      include ::SemanticLogger::Loggable
      include ReleaseTools::Security::IssueHelper

      def initialize
        @client = ReleaseTools::GitlabClient
        @issue_crawler = Security::IssueCrawler.new
        @managed_versioning_issues = []
      end

      def execute
        collect_managed_versioning_issues

        if managed_versioning_issues.empty?
          logger.info("None of the linked issues are for projects are under managed versioning.", security_tracking_issue: security_tracking_issue.web_url)
        else
          logger.info('Notifying RMs that issues for projects under managed versioning were linked to patch release tracking issue', security_tracking_issue: security_tracking_issue.web_url)

          notify_release_managers

          logger.info(managed_versioning_log_message)
        end

        send_slack_notification(:success)
      rescue StandardError => ex
        error_message = <<~MSG
          Managed versioning check failed. If this job continues to fail, manually check if there are any issues for
          projects under managed versioning (https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/security_release.md#release-manager-process)
          linked to the tracking issue.

          If any of the issues are for https://gitlab.com/gitlab-org/security/gitaly, follow our documentation for gitaly:
          https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_kas_security_merge_requests.md.
        MSG

        logger.fatal(error_message, error: ex)

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :client, :issue_crawler, :managed_versioning_issues

      def security_target_issues
        @security_target_issues ||= issue_crawler.related_security_issues
      end

      def collect_managed_versioning_issues
        security_target_issues.each do |issue|
          managed_versioning_issues << issue if ManagedVersioning::PROJECTS_NEEDING_MANUAL_RELEASES.map(&:security_id).include?(issue.project_id)
        end
      end

      def notify_release_managers
        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          client.create_issue_note(
            security_tracking_issue.project_id,
            issue: security_tracking_issue,
            body: notification_message
          )
        end
      end

      def notification_message
        message = <<~MSG
          @gitlab-org/release/managers, there are managed versioning project issues linked to this patch release. Please
          [follow the release manager instructions](https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/security_release.md#release-manager-process)
          and adjust the [patch release task issue](#{security_task_issue.web_url}) to include any additional steps needed.

          If any of the issues are for https://gitlab.com/gitlab-org/security/gitaly or https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent, follow our [documentation for gitaly/kas](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_kas_security_merge_requests.md).
        MSG

        "#{message}\n#{issues_list_message}"
      end

      def issues_list_message
        managed_versioning_issues.filter_map do |issue|
          "- #{issue.web_url}"
        end.join("\n")
      end

      def send_slack_notification(status)
        ::ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: 'Managed versioning check',
          status: status,
          release_type: :patch
        ).send_notification
      end

      def managed_versioning_log_message
        message = "\n\nIssues for projects that are under managed versioning:\n\n"

        message + issues_list_message
      end
    end
  end
end
