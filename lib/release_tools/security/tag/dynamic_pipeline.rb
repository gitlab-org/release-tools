# frozen_string_literal: true

module ReleaseTools
  module Security
    module Tag
      class DynamicPipeline < DynamicPipelineBase
        def stages
          {
            'stages' => versions.map { |version| "security_release:tag:#{version}" }
          }
        end

        # The tag jobs must run in sequences as they may otherwise fail to concurrently push changes
        # to the same project/branch. Thus, each job is in one stage.
        def job(version)
          {
            'image' => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
            'stage' => "security_release:tag:#{version}",
            'script' => [
              "source scripts/setup_ssh.sh",
              "source scripts/setup_git.sh",
              "bundle exec rake security:tag[#{version}]"
            ],
            'extends' => [
              '.with-bundle',
              '.common-ci-tokens'
            ]
          }
        end

        def jobs
          versions.each_with_object({}) do |version, hash|
            hash["security_release_tag:#{version}"] = job(version)
          end
        end
      end
    end
  end
end
