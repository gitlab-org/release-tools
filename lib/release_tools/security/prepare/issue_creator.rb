# frozen_string_literal: true

module ReleaseTools
  module Security
    module Prepare
      class IssueCreator
        include ::SemanticLogger::Loggable

        # @param issue [Issuable] => An issuable object that responds to URL
        # @param issue_type [string] => 'appsec_task_issue' or 'comms_security_task_issue'
        def initialize(issue:, issue_type:)
          @issue = issue
          @issue_type = issue_type
        end

        def execute
          logger.info('Creating Issue', issue_type: issue_type)

          if issue.exists?
            logger.info('Issue already exists, skipping', web_url: issue.url)
          else
            create_issue
            notify_appsec
          end

          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)
          send_slack_notification(:failed)

          raise
        end

        private

        attr_reader :issue, :issue_type

        def create_issue
          logger.info('Creating issue', issue_type: issue_type)

          return if SharedStatus.dry_run?

          issue.create
          logger.info('Issue created', web_url: issue.url)
        end

        def notify_appsec
          ReleaseTools::Slack::Security::AppSecNotifier
            .new(issuable: issue, issue_type: issue_type)
            .send_notification
        end

        def failure_message
          <<~MSG
            #{issue_type} creation failed. Retry this job. If the failure persist, notify the
            AppSec team and indicate the issue will need to be created manually.
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: "#{issue_type.titleize} creator",
            status: status,
            release_type: :patch
          ).send_notification
        end
      end
    end
  end
end
