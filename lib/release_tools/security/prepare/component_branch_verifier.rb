# frozen_string_literal: true

module ReleaseTools
  module Security
    module Prepare
      class ComponentBranchVerifier
        def execute
          branches_status = ReleaseTools::Services::BranchesStatusService.new(
            release_type: :patch,
            versions: patch_versions
          )

          branches_status.execute
        end

        def patch_versions
          ReleaseTools::Versions.next_versions
        end
      end
    end
  end
end
