# frozen_string_literal: true

module ReleaseTools
  module Security
    module Publish
      class DynamicPipeline < DynamicPipelineBase
        def stages
          {
            'stages' => versions.map { |version| "security_release:publish:#{version}" }
          }
        end

        def job(version)
          {
            'image' => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
            'stage' => "security_release:publish:#{version}",
            'script' => [
              "source scripts/setup_ssh.sh",
              "source scripts/setup_git.sh",
              "bundle exec rake security:publish[#{version}]"
            ],
            'extends' => [
              '.with-bundle',
              '.common-ci-tokens'
            ]
          }
        end

        def jobs
          versions.each_with_object({}) do |version, hash|
            hash["security_release_publish:#{version}"] = job(version)
          end
        end
      end
    end
  end
end
