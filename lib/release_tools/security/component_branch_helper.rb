# frozen_string_literal: true

module ReleaseTools
  module Security
    module ComponentBranchHelper
      def generate_link_list(projects, versions, release_type = :patch)
        projects.each do |project|
          branches = versions.map { |version| version.stable_branch(ee: project.ee_branch?) }

          branches.each do |branch|
            link = if release_type == :monthly
                     "https://gitlab.com/#{project.path}/-/commits/#{branch}"
                   else
                     "https://gitlab.com/#{project.security_path}/-/commits/#{branch}"
                   end

            yield(project.metadata_project_name, branch, link)
          end
        end
      end
    end
  end
end
