# frozen_string_literal: true

module ReleaseTools
  module Tracker
    # Interface for adding of environment labels to deployed merge requests.
    module MergeRequestLabeler
      # environment - The name of the environment that was deployed to.
      # deployments - An Array of `Tracker::Deployment` instances,
      #               containing data about a deployment.
      def label_merge_requests(environment, deployments)
        label = deployment_labels[environment]

        unless label
          logger.warn(
            'Not updating merge requests as there is no label for this environment',
            environment: environment
          )

          return
        end

        logger.info(
          'Adding label to deployed merge requests',
          environment: environment,
          label: label
        )

        return if SharedStatus.dry_run?

        ReleaseTools::Tracker::MergeRequestUpdater
          .for_successful_deployments(deployments)
          .add_label(label)
      end

      private

      # Defines the labels to apply to a merge request when it is deployed.
      #
      # The keys are the environments deployed to, the values the label to
      # apply.
      #
      # Example
      #
      # {
      #    'gstg' => 'workflow::staging',
      #    'gprd-cny' => 'workflow::canary',
      #    'gprd' => 'workflow::production',
      #  }.freeze
      def deployment_labels
        raise NotImplementedError
      end
    end
  end
end
