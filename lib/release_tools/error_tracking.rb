# frozen_string_literal: true

module ReleaseTools
  module ErrorTracking
    module_function

    def with_exception_captured(&)
      ::Sentry.with_exception_captured(&)
    end

    def capture_exception(ex)
      ::Sentry.capture_exception(ex)
    end
  end
end
