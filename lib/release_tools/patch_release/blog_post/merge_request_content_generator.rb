# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    module BlogPost
      # This class is responsible for generating the content for a blog merge request.
      class MergeRequestContentGenerator
        include Utils
        extend Forwardable

        # Projects that are not included in the table of fixes because their
        # fixes are often due to external CVEs
        NON_TABLE_OF_FIXES_PROJECTS = [
          Project::OmnibusGitlab.security_id,
          Project::CNGImage.security_id
        ].freeze

        attr_reader :bug_fixes, :security_fixes, :metadata

        def_delegator :metadata, :blog_title
        def_delegator :metadata, :blog_description
        def_delegator :metadata, :blog_tags
        def_delegator :metadata, :canonical_path
        def_delegator :metadata, :author_gitlab
        def_delegator :metadata, :release_name
        def_delegator :metadata, :versions

        # Instanciates the generator class with bug fixes, security fixes and metadata. The metadata
        # exposes all of static data needed to generate the blog post.
        def initialize(bug_fixes:, security_fixes:, metadata:)
          @bug_fixes = bug_fixes
          @security_fixes = security_fixes
          @metadata = metadata
        end

        # Entrypoint method
        def execute
          generate_blog_content
        end

        protected

        # Binding methods (needed to be accessed in the ERB template)

        def security_fixes?
          security_fixes.present?
        end

        def bug_fixes?
          bug_fixes.present?
        end

        def sorted_security_content
          security_fixes
            .sort_by do |issue|
              if issue.cves_issue&.valid_yaml?
                issue.cves_issue&.cvss_base_score || -1
              else
                -1
              end
            end
            .reverse
        end

        def sorted_table_content
          sorted_security_content.reject do |issue|
            NON_TABLE_OF_FIXES_PROJECTS.include?(issue.project_id)
          end
        end

        def security_issue_header(security_issue)
          security_issue&.cves_issue&.title || security_issue.issue.title
        end

        def security_issue_slug(security_issue)
          security_issue_header(security_issue)
            .downcase
            .scan(/\w|\d| |-/)
            .join
            .tr(' ', '-')
        end

        def security_issue_blog_content(security_issue)
          return security_issue_blog_content_with_valid_yaml(security_issue.cves_issue) if security_issue.cves_issue&.valid_yaml?

          logger.warn "The content of the security issue (#{security_issue.web_url}) was skipped due to invalid CVES YAML or because of missing CVES."
          security_issue_blog_content_default
        end

        def displayed_severity(security_issue)
          return 'TODO' unless security_issue.cves_issue&.valid_yaml?

          severity = security_issue.cves_issue&.cvss_severity
          return 'TODO' if severity == 'None'

          severity
        end

        def generate_blog_content
          ERB.new(File.read(metadata.template_path), trim_mode: '-').result(binding)
        end

        private

        # TODO: move this to template
        def security_issue_blog_content_with_valid_yaml(cves_issue)
          <<~STR
            #{cves_issue.vulnerability_description.gsub(/[.]+\z/, '')}.
            This is a #{cves_issue.cvss_severity.downcase} severity issue ([`#{cves_issue.cvss_string}`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=#{cves_issue.cvss_string}), #{cves_issue.cvss_base_score}).
            #{cves_id_link(cves_issue)}

            #{cves_issue.credit.gsub(/[.]+\z/, '')}.
          STR
        end

        def security_issue_blog_content_default
          <<~STR
            TODO: add description for this issue
          STR
        end

        def cves_id_link(cves_issue)
          if cves_issue.cve_id.blank?
            "We have requested a CVE ID and will update this blog post when it is assigned."
          else
            "It is now mitigated in the latest release and is assigned [#{cves_issue.cve_id}](https://cve.mitre.org/cgi-bin/cvename.cgi?name=#{cves_issue.cve_id})."
          end
        end
      end
    end
  end
end
