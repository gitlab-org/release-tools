# frozen_string_literal: true

module ReleaseTools
  class Issue < Issuable
    def create
      ReleaseTools::GitlabClient.create_issue(self, project)
    end

    def update
      ReleaseTools::GitlabClient.update_issue(self, project)
    end

    def remote_issuable
      @remote_issuable ||= ReleaseTools::GitlabClient.find_issue(self, project)
    end

    def confidential?
      false
    end

    def release_pipeline
      @release_pipeline ||= begin
        logger.info("Creating #{self.class::RELEASE_TYPE} release pipeline")

        if SharedStatus.dry_run?
          fake_pipeline = Struct.new(:web_url)
          fake_pipeline.new(web_url: 'https://example.com/foo/bar/-/pipelines/1')
        else
          GitlabOpsClient.create_pipeline(
            Project::ReleaseTools,
            pipeline_variables
          )
        end
      end
    end

    private

    # Required in subclasses to provide specific release pipeline environment variables
    def pipeline_variables
      raise NotImplementedError
    end
  end
end
