# frozen_string_literal: true

module ReleaseTools
  class BranchStatus
    include ::SemanticLogger::Loggable
    extend ParallelMethods

    PROJECTS = [
      Project::GitlabEe,
      Project::GitlabCe,
      Project::OmnibusGitlab
    ].freeze

    def self.for_security_release
      versions = ReleaseTools::Versions
        .next_versions
        .map { |v| ReleaseTools::Version.new(v) }

      self.for(versions)
    end

    # Returns a Hash of `project => status` pairs, where `status` is an Array of
    # the results of the latest pipeline for each given version
    def self.for(versions)
      PROJECTS.each_with_object({}) do |project, memo|
        memo[project] = parallel_map(versions) do |version|
          project_pipeline(project, version)
        end
      end
    end

    def self.project_pipeline(project, version)
      logger.trace(__method__, project: project, version: version)

      client(version).pipelines(
        project_path(project, version),
        ref: pipeline_ref(project, version),
        per_page: 1
      ).first
    end

    def self.project_path(project, version)
      if version.rc?
        project
      else
        project.security_path
      end
    end

    def self.pipeline_ref(project, version)
      if version.rc?
        rc_tag_package(project, version)
      else
        version.stable_branch(ee: project == Project::GitlabEe)
      end
    end

    def self.client(version)
      if version.rc?
        ReleaseTools::GitlabDevClient
      else
        ReleaseTools::GitlabClient
      end
    end

    def self.rc_tag_package(project, version)
      if project == Project::GitlabEe
        version.tag(ee: true)
      elsif project == Project::GitlabCe
        version.tag
      else
        version.to_omnibus(ee: true)
      end
    end

    private_class_method :project_pipeline, :project_path, :pipeline_ref, :client, :rc_tag_package
  end
end
