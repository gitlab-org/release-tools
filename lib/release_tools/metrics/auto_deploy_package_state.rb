# frozen_string_literal: true

module ReleaseTools
  module Metrics
    class AutoDeployPackageState
      include ::SemanticLogger::Loggable

      METRIC = 'auto_deploy_package_state'
      STATES = %w[missing pending building ready failed].freeze

      def initialize
        @client = Metrics::Client.new
      end

      def execute
        [ReleaseTools::Project::CNGImage, ReleaseTools::Project::OmnibusGitlab].each do |project|
          pipeline = find_pipeline(project)

          pkg_state = pipeline_state(pipeline)

          logger.info('Auto-deploy package state', project: project, version: deploy_version, pkg_state: pkg_state, pipeline_status: pipeline&.status, pipeline_url: pipeline&.web_url)
          next if SharedStatus.dry_run?

          set_metric(project, pkg_state)
        end
      end

      private

      def find_pipeline(project)
        ref = product_version[project.metadata_project_name].ref

        logger.info('Finding packager pipeline', ref: ref, project: project)

        pipelines =
          Retriable.with_context(:api) do
            ReleaseTools::GitlabDevClient.pipelines(
              project,
              ref: ref,
              order_by: 'id',
              sort: 'desc'
            )
          end

        pipelines&.first
      end

      def product_version
        @product_version ||= ProductVersion.new(AutoDeploy::Tag.current)
      end

      def deploy_version
        @deploy_version ||= product_version.auto_deploy_package
      end

      def set_metric(project, state)
        @client.set(METRIC, 1, labels: "#{project.dev_path},#{state},#{deploy_version}")

        # Set other states to 0
        (STATES - [state]).each do |other_state|
          @client.set(METRIC, 0, labels: "#{project.dev_path},#{other_state},#{deploy_version}")
        end
      end

      def pipeline_state(pipeline)
        case pipeline&.status
        when nil
          'missing' # Pipeline missing
        when 'created', 'scheduled', 'preparing', 'waiting_for_resource', 'pending'
          'pending'
        when 'running'
          'building'
        when 'manual', 'success'
          'ready'
        when 'failed', 'canceled', 'skipped'
          'failed'
        else
          logger.error('Unknown packager pipeline status', version: deploy_version, status: pipeline.status)
        end
      end
    end
  end
end
