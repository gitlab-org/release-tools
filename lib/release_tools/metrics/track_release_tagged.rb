# frozen_string_literal: true

module ReleaseTools
  module Metrics
    # Publish metrics related to release tags
    class TrackReleaseTagged
      def initialize(version)
        @version = version
      end

      def execute
        if SharedStatus.critical_patch_release?
          client.inc("packages_tagging_total", labels: 'patch,critical')
        elsif SharedStatus.security_release?
          client.inc("packages_tagging_total", labels: 'patch,regular')
        elsif @version.monthly?
          client.inc("packages_tagging_total", labels: 'monthly,no')
        elsif @version.patch?
          client.inc("packages_tagging_total", labels: 'patch,no')
        elsif @version.rc?
          client.inc("packages_tagging_total", labels: 'rc,no')
        end
      end

      def client
        @client ||= ReleaseTools::Metrics::Client.new
      end
    end
  end
end
