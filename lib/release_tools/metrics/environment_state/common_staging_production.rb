# frozen_string_literal: true

module ReleaseTools
  module Metrics
    module EnvironmentState
      module CommonStagingProduction
        # Promote job can be in the following states when deployment pipeline has reached promotion stage
        PROMOTE_JOB_VALID_STATES = %w[manual running success failed].freeze

        def packages_available_for_promotion?
          version_on_env = ProductVersion.from_metadata_sha(latest_successful_deployment.sha)

          # Search for a package that is newer than on prod, and whose deployment pipeline's promote job is playable
          # or has already been played.
          ProductVersion.any? do |version|
            # ProductVersion returns all versions, including self-managed releases.
            next unless version.auto_deploy?
            # Since ProductVersion.each returns packages in descending order, we can break out of the loop once we reach
            # the version that is already on the environment.
            break false if version <= version_on_env

            ready_for_promotion?(version)
          end
        end

        def deployment_pipeline_for(version)
          Retriable.with_context(:api) do
            GitlabOpsClient.pipelines(Project::ReleaseTools, name: 'Coordinator pipeline', ref: version.to_s).first
          end
        end

        def ready_for_promotion?(version)
          pipeline = deployment_pipeline_for(version)
          return false unless pipeline

          promote_job =
            Retriable.with_context(:api) do
              GitlabOpsClient
                .pipeline_jobs(Project::ReleaseTools, pipeline.id, scope: PROMOTE_JOB_VALID_STATES)
                .auto_paginate
                .detect { |job| job.name == 'promote' }
            end

          promote_job.present?
        end

        def latest_successful_deployment
          Retriable.with_context(:api) do
            GitlabOpsClient.deployments(
              Project::Release::Metadata,
              environment,
              status: 'success',
              order_by: 'id',
              sort: 'desc',
              opts: {
                per_page: 1
              }
            ).first
          end
        end
      end
    end
  end
end
