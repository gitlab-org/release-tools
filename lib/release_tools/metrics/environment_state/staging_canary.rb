# frozen_string_literal: true

require 'state_machines/core'

module ReleaseTools
  module Metrics
    module EnvironmentState
      class StagingCanary
        include ::SemanticLogger::Loggable
        extend StateMachines::MacroMethods
        include EnvironmentState::Common

        state_machine :current_state, initial: lambda(&:query_current_state) do
          event :start do
            transition %i[initial ready locked] => :locked
          end

          event :success do
            transition %i[initial locked] => :ready
          end
        end

        def environment
          'gstg'
        end

        def stage
          'cny'
        end
      end
    end
  end
end
