# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module Builder
      class CNGImage
        include ::SemanticLogger::Loggable

        PROJECT = ReleaseTools::Project::CNGImage
        VARIABLES_FILE = 'ci_files/variables.yml'

        # target_branch - AutoDeploy::Branch object
        # commit_id - SHA of a passing gitlab-rails commit
        # version_string - Auto deploy normalized version string (ex: 17.7.202412040800)
        def initialize(target_branch, commit_id, version_string, release_metadata = ReleaseTools::ReleaseMetadata.new)
          @target_branch = target_branch
          @target_name = target_branch.to_s
          @commit_id = commit_id
          @release_metadata = release_metadata
          @version_string = version_string
        end

        def changes?
          component_changes? || builder_changes?
        end

        # Update the project, if necessary, and tag
        def execute
          if component_changes?
            commit(current_variables.merge(version_map))
          else
            logger.warn('No changes to CNG component versions')
          end

          logger.warn('No changes to CNG itself') unless builder_changes?

          tag
        end

        def version_map
          @version_map ||= ReleaseTools::ComponentVersions.for_cng(@commit_id)
        end

        private

        def component_changes?
          version_map.any? do |component, version|
            current_variables[component] != version
          end
        end

        # Check if the builder itself has changes on the target branch
        def builder_changes?
          refs = GitlabClient.commit_refs(PROJECT.auto_deploy_path, @target_branch)

          # When our target branch has no associated tags, then there have been
          # changes on the branch since we last tagged it, and should be
          # considered changed
          refs.none? { |ref| ref.type == 'tag' }
        end

        def commit(new_variables)
          return if SharedStatus.dry_run?

          action = {
            action: 'update',
            file_path: VARIABLES_FILE,
            content: { 'variables' => new_variables }.to_yaml
          }

          commit = Retriable.with_context(:api) do
            GitlabClient.create_commit(
              PROJECT.auto_deploy_path,
              @target_name,
              'Update component versions',
              [action]
            )
          end

          logger.info('Updated CNG versions', url: commit.web_url)

          commit
        rescue ::Gitlab::Error::ResponseError => ex
          logger.warn(
            'Failed to commit CNG version changes',
            target: @target_name,
            error_code: ex.response_status,
            error_message: ex.response_message
          )

          raise ex
        end

        def tag
          Tagger::CNGImage
            .new(@target_branch, version_map, @version_string, @release_metadata)
            .tag!
        end

        def current_variables
          @current_variables ||= Retriable.with_context(:api) do
            variables = GitlabClient.file_contents(
              PROJECT.auto_deploy_path,
              VARIABLES_FILE,
              @target_name
            ).chomp

            YAML.safe_load(variables).fetch('variables')
          end
        rescue ::Gitlab::Error::ResponseError => ex
          logger.warn(
            'Failed to load current CNG variables',
            target: @target_name,
            error: ex.message
          )

          raise ex
        end
      end
    end
  end
end
