# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module Builder
      class Omnibus
        include ::SemanticLogger::Loggable

        PROJECT = ReleaseTools::Project::OmnibusGitlab

        # target_branch - AutoDeploy::Branch object
        # commit_id - SHA of a passing gitlab-rails commit
        # version_string - Auto deploy normalized version string (ex: 17.7.202412040800)
        def initialize(target_branch, commit_id, version_string, release_metadata = ReleaseTools::ReleaseMetadata.new)
          @target_branch = target_branch
          @target_name = target_branch.to_s
          @commit_id = commit_id
          @release_metadata = release_metadata
          @version_string = version_string
        end

        def changes?
          component_changes? || builder_changes?
        end

        # Update the project, if necessary, and tag
        def execute
          if component_changes?
            commit
          else
            logger.warn('No changes to Omnibus component versions')
          end

          logger.warn('No changes to Omnibus itself') unless builder_changes?

          tag
        end

        def version_map
          @version_map ||= ReleaseTools::ComponentVersions.for_omnibus(@commit_id)
        end

        private

        # Check if the versions defined by gitlab-rails are now differing from
        # those defined by Omnibus
        def component_changes?
          return @component_changes if defined? @component_changes

          @component_changes = version_map.any? do |filename, current|
            GitlabClient.file_contents(
              PROJECT.auto_deploy_path,
              filename,
              @target_name
            ).chomp != current
          end
        rescue ::Gitlab::Error::ResponseError => ex
          logger.warn(
            'Failed to find Omnibus version file',
            target: @target_name,
            error_code: ex.response_status,
            error_message: ex.message
          )

          false
        end

        # Check if the builder itself has changes on the target branch
        def builder_changes?
          refs = GitlabClient.commit_refs(PROJECT.auto_deploy_path, @target_branch)

          # When our target branch has no associated tags, then there have been
          # changes on the branch since we last tagged it, and should be
          # considered changed
          refs.none? { |ref| ref.type == 'tag' }
        end

        def commit
          return if SharedStatus.dry_run?

          actions = version_map.map do |filename, contents|
            {
              action: 'update',
              file_path: "/#{filename}",
              content: "#{contents.strip}\n"
            }
          end

          commit = Retriable.with_context(:api) do
            GitlabClient.create_commit(
              PROJECT.auto_deploy_path,
              @target_name,
              'Update component versions',
              actions
            )
          end

          logger.info('Updated Omnibus versions', url: commit.web_url)

          commit
        rescue ::Gitlab::Error::ResponseError => ex
          logger.warn(
            'Failed to commit Omnibus version changes',
            target: @target_name,
            error_code: ex.response_status,
            error_message: ex.message
          )

          raise ex
        end

        def tag
          Tagger::Omnibus
            .new(@target_branch, version_map, @version_string, @release_metadata)
            .tag!
        end
      end
    end
  end
end
