# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      class ReleaseNote
        include ::SemanticLogger::Loggable

        def initialize(production_status, pending_post_migrations)
          @production_status = production_status
          @pending_post_migrations = pending_post_migrations
          @ci_pipeline_id = ENV.fetch('CI_PIPELINE_ID')
          @ci_pipeline_url = ENV.fetch('CI_PIPELINE_URL')
        end

        def execute
          logger.info(
            'Posting post_deploy_migrations note on the monthly issue',
            pending_post_migrations: pending_post_migrations,
            issue: monthly_issue.url
          )

          return if SharedStatus.dry_run?

          Retriable.with_context(:api) do
            GitlabClient.create_issue_note(
              monthly_issue.project,
              issue: monthly_issue,
              body: body
            )
          end
        end

        private

        attr_reader :ci_pipeline_id, :ci_pipeline_url, :production_status, :pending_post_migrations

        def release_manager
          release_user = ENV.fetch('RELEASE_USER', nil)
          logger.info('Fetching release manager', pipeline_url: ci_pipeline_url, release_user: release_user)

          # When this is triggered from chatops, RELEASE_USER variable will be set to the
          # user who ran the chatops command.
          ops_user = release_user || pipeline.user.username
          return ':warning: Unknown release manager!' if ops_user.nil?

          user = ReleaseManagers::Definitions.new.find_user(ops_user, instance: :ops)
          return ":warning: Unknown release manager! OPS username #{ops_user}" if user.nil?

          "@#{user.production}"
        end

        def pipeline
          Retriable.with_context(:api) do
            ReleaseTools::GitlabOpsClient.pipeline(Project::ReleaseTools, ci_pipeline_id)
          end
        end

        def monthly_issue
          monthly_version = ReleaseManagers::Schedule.new.active_version

          ReleaseTools::MonthlyIssue.new(version: monthly_version)
        end

        def deployment_sha
          @deployment_sha ||= Retriable.with_context(:api) do
            GitlabClient
              .last_successful_deployment(Project::GitlabEe.security_path, 'gprd')
              .sha
          end
        end

        def file_url(filename)
          "https://gitlab.com/#{Project::GitlabEe.security_path}/-/blob/#{deployment_sha}/db/post_migrate/#{filename}"
        end

        def body
          ERB
            .new(template, trim_mode: '-') # Omit blank lines when using `<% -%>`
            .result(binding)
        end

        def template
          File.read(
            File.expand_path('../../../../templates/post_deploy_migrations_execution_start_note.md.erb', __dir__)
          )
        end
      end
    end
  end
end
