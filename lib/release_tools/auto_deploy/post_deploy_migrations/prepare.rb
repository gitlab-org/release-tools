# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      class Prepare
        include ::SemanticLogger::Loggable

        # The sleep intervals to use when waiting for the job trace.
        #
        # At 5 intervals of 30 seconds, we wait at most 2.5 minutes.
        WAIT_INTERVALS = Array.new(5, 30)

        # Error raised when the job trace hasn't achieved the minimum length
        IncompleteTrace = Class.new(StandardError)

        def execute
          if pending_post_migrations.empty?
            logger.warn('No pending post-migrations to be executed, skipping the post-deploy pipeline')

            initial_notification.no_pending_post_migrations_message

            return cancel_job
          end

          logger.info('Pending post-migrations available', post_migrations: pending_post_migrations)

          return cancel_job unless production_status_fine?
          return cancel_job if wraparound_vacuum_running?(pending_post_migrations)

          post_note_to_release_issue(pending_post_migrations)
        end

        def pending_post_migrations
          @pending_post_migrations ||= raw_pending_post_migrations.filter_map do |line|
            # Each line is in the form: `20240701083851 post 17.2 FooBar`
            parts = line.strip.split
            recast("#{parts[0]} #{parts[3]}")
          end.uniq
        end

        private

        def raw_pending_post_migrations
          YAML.safe_load ENV.fetch('PENDING_POST_DEPLOY_MIGRATIONS', '[]')
        end

        # Converts '20240701083851 FooBar' into '20240701083851_foo_bar.rb'
        def recast(raw_migration)
          raw_migration
            .underscore
            .gsub(/\s+/, '_')
            .concat('.rb')
        end

        def production_status_fine?
          if production_status.fine?
            logger.info('Production checks have succeeded, proceeding with post-deploy migrations')

            true
          else
            logger.warn('Production checks have failed', failed_checks: production_status.failed_checks.map(&:name))

            initial_notification.production_status_failed_message(production_status)

            false
          end
        end

        def wraparound_vacuum_running?(migrations)
          if ReleaseTools::Prometheus::WraparoundVacuumChecks.running?(migrations)
            logger.warn('Wraparound autovacuum detected')

            initial_notification.wraparound_vacuum_on_post_migrations_message

            true
          else
            logger.info('No wraparound autovacuum detected')

            false
          end
        end

        def cancel_job
          logger.warn('Canceling the prepare job')

          ::SemanticLogger.flush # Flushing the logs so they're available on Elastic

          client = ReleaseTools::GitlabOpsClient
          project = ReleaseTools::Project::ReleaseTools
          job_id = ENV.fetch('CI_JOB_ID')

          Retriable.retriable(intervals: WAIT_INTERVALS, on: IncompleteTrace) do
            trace_log = client.job_trace(project, job_id)

            raise IncompleteTrace unless trace_log.include?('Canceling the prepare job')
          end

          client.cancel_job(project, job_id)
        end

        def initial_notification
          ReleaseTools::Slack::PostDeployPipelineInitialNotification.new
        end

        def production_status
          @production_status ||= ReleaseTools::Promotion::ProductionStatus.new(
            :canary_up,
            :active_gprd_deployments,
            :active_gstg_deployments,
            :orchestrator_status
          )
        end

        def post_note_to_release_issue(pending_post_migrations)
          ReleaseNote.new(production_status, pending_post_migrations).execute
        end
      end
    end
  end
end
