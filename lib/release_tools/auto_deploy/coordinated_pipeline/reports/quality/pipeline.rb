# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Reports
        module Quality
          class Pipeline
            include Helper

            attr_reader :jobs

            def initialize(data, environment, pipeline_type)
              @data = data
              @environment = environment
              @pipeline_type = pipeline_type
              @jobs = load_jobs
            end

            def web_url
              data.web_url
            end

            private

            attr_reader :data, :environment, :pipeline_type

            def load_jobs
              failed_jobs.map do |raw_job|
                job = Job.new(raw_job, environment, pipeline_type)
                job.errors
                job
              end
            end

            def failed_jobs
              client.pipeline_jobs(
                project,
                data.id,
                scope: 'failed'
              )
            end
          end
        end
      end
    end
  end
end
