# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    class Tag
      include ::SemanticLogger::Loggable

      # Return the current auto-deploy tag string
      #
      # If a manually-specified tag is available, that will be used first.
      #
      # If running on a tagged release-tools pipeline, this will simply return
      # that tag.
      def self.current
        ENV.fetch('AUTO_DEPLOY_TAG') do
          ENV.fetch('CI_COMMIT_TAG')
        end
      end

      # Build the tag string from the current auto-deploy branch
      # and the current UTC timestamp.
      def self.generate
        new.to_s
      end

      def initialize
        @branch = ReleaseTools::AutoDeployBranch.current
        @current_time = Time.now.utc
      end

      def to_s
        "#{@branch.version.to_minor}.#{timestamp}"
      end

      def timestamp
        @current_time.strftime('%Y%m%d%H%M')
      end

      def omnibus_package
        component_ref(component: 'omnibus-gitlab-ee').sub('+', '-')
      end

      def component_ref(component:)
        product_version[component]&.ref
      end

      def product_version
        @product_version ||= ProductVersion.new(self.class.current)
      end
    end
  end
end
