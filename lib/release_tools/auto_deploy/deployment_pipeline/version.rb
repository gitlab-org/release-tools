# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module DeploymentPipeline
      # Test if a version is suitable for deployment
      class Version
        include ::SemanticLogger::Loggable

        attr_reader :product_version, :skip_checks

        PIPELINE_SUCCESS_STATUS = 'success'

        # @param [ProductVersion] product_version is the version that we want to check for suitability
        # @param [Boolean] skip_checks should be set to true if the suitability of the version
        #   should not be checked. When skip_checks is set to true, most checks are skipped.
        def initialize(product_version, skip_checks: false)
          @product_version = product_version
          @skip_checks = skip_checks
        end

        # @return [Boolean]
        # Checks if the version is a valid auto deploy version,
        # if the version is newer than the latest deployment pipeline version,
        # and if the packager pipelines have completed.
        def suitable?
          unless valid_auto_deploy_version?
            logger.info('Not suitable since not valid auto deploy version', version: product_version.to_s)
            return false
          end

          if skip_checks
            logger.info('Skipping some suitability checks because skip_checks flag is set', version: product_version.to_s)
            return true
          end

          unless newer_than_latest_deployment?
            logger.info('Not suitable since version not newer than latest deployment pipeline version', version: product_version.to_s)
            return false
          end

          unless packager_pipelines_complete?
            logger.info('Not suitable since packager pipelines for version not complete', version: product_version.to_s)
            return false
          end

          logger.info('Version is suitable', version: product_version.to_s)

          true
        end

        def valid_auto_deploy_version?
          product_version.auto_deploy?
        end

        def newer_than_latest_deployment?
          return @newer_than_latest_deployment if defined?(@newer_than_latest_deployment)

          latest_deployment_version = Latest.version

          logger.info(
            'Checking if version is newer than latest deployment',
            latest_deployment_version: latest_deployment_version.to_s,
            product_version: product_version.to_s
          )

          @newer_than_latest_deployment = product_version > latest_deployment_version
        end

        private

        def packager_pipelines_complete?
          @packager_pipelines_complete ||=
            packager_pipeline_completed?(Project::OmnibusGitlab) &&
            packager_pipeline_completed?(Project::CNGImage)

          logger.info('Checked if packager pipelines completed', version: product_version.to_s, packager_pipelines_complete: @packager_pipelines_complete)

          @packager_pipelines_complete
        end

        def packager_pipeline_completed?(project)
          pipeline =
            Retriable.with_context(:api) do
              GitlabDevClient.pipelines(
                project,
                name: 'AUTO_DEPLOY_BUILD_PIPELINE',
                ref: product_version[project].ref,
                per_page: 1
              ).first
            end

          pipeline&.status == PIPELINE_SUCCESS_STATUS
        end
      end
    end
  end
end
