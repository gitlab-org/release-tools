# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module DeploymentPipeline
      class Trigger
        include ::SemanticLogger::Loggable

        InvalidVersionError = Class.new(StandardError)

        # @param [ProductVersion] product_version is the product version which is to be deployed
        def initialize(product_version)
          @product_version = product_version
        end

        def execute
          logger.info('Starting deployment pipeline', version: product_version.to_s)
          return if SharedStatus.dry_run?

          response =
            Retriable.with_context(:api) do
              GitlabOpsClient.create_pipeline(Project::ReleaseTools, { AUTO_DEPLOY_TAG: product_version.to_s })
            end

          logger.info('Started deployment pipeline', url: response.web_url, version: product_version.to_s)

          response
        end

        private

        attr_reader :product_version
      end
    end
  end
end
