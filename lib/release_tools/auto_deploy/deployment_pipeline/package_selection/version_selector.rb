# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module DeploymentPipeline
      module PackageSelection
        # Select a particular version for deployment.
        # This class raises an error if the given version is not suitable for deployment.
        class VersionSelector
          include ::SemanticLogger::Loggable

          UnsuitableVersionError = Class.new(StandardError)

          # @param [Version] version to be deployed
          # @param skip_suitable_check [Boolean] should be set to true if the suitability of the version
          #   should not be checked.
          def initialize(version:, skip_suitable_check: false)
            @version = version
            @skip_suitable_check = skip_suitable_check
          end

          # @return [ProductVersion]
          def product_version
            raise UnsuitableVersionError, "'#{version}' is not suitable for deployment" unless suitable?

            product_version_object
          end

          private

          attr_reader :version, :skip_suitable_check

          def suitable?
            DeploymentPipeline::Version.new(product_version_object, skip_checks: skip_suitable_check).suitable?
          end

          def product_version_object
            @product_version_object ||= ProductVersion.new(version)
          end
        end
      end
    end
  end
end
