# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module DeploymentPipeline
      # This class helps sequence deployment pipelines so that they do
      # not overlap.
      class Sequencer
        include ::SemanticLogger::Loggable

        JOB_FAILURE_STATUS = %w[failed skipped].freeze

        class << self
          # This method returns true once the latest deployment pipeline completes deploying to gprd-cny.
          # Once the latest pipeline completes deploying to gprd-cny, a new deployment pipeline can be
          # started that will deploy to gstg-cny.
          #
          # @return [Boolean] true if the gprd-cny downstream pipeline has completed, or if the pipeline has
          #   been canceled, or if the pipeline has failed.
          def can_start_deployment?
            pipeline = Latest.pipeline
            bridge_jobs = GitlabOpsClient.pipeline_bridges(Project::ReleaseTools, pipeline.id).auto_paginate
            prod_canary = bridge_jobs.detect { |job| job.name == 'deploy:gprd-cny' }

            if job_completed?(prod_canary)
              logger.info(
                'Latest pipeline has completed deploying to gprd-cny, new pipeline can be started',
                pipeline_url: pipeline.web_url,
                gprd_cny_job_status: prod_canary.status
              )
              return true
            end

            if canceled?(prod_canary, pipeline)
              logger.info(
                'Latest gprd-cny deployment or coordinated pipeline has been canceled, new pipeline can be started',
                pipeline_status: pipeline.status,
                gprd_cny_job_status: prod_canary.status,
                pipeline_url: pipeline.web_url
              )
              return true
            end

            if job_failed?(prod_canary, pipeline)
              logger.info(
                'Latest pipeline has not completed deploying to gprd-cny, but sufficient time has passed, so new pipeline can be started',
                pipeline_status: pipeline.status,
                pipeline_url: pipeline.web_url,
                gprd_cny_job_status: prod_canary.status
              )
              return true
            end

            logger.info(
              'New pipeline cannot be started since previous pipeline has not completed deploying to gprd-cny',
              pipeline_status: pipeline.status,
              pipeline_url: pipeline.web_url,
              gprd_cny_job_status: prod_canary.status
            )

            false
          end

          private

          def job_completed?(job)
            job.status == 'success'
          end

          def canceled?(job, pipeline)
            job.status == 'canceled' || pipeline.status == 'canceled'
          end

          def job_failed?(job, coordinator_pipeline)
            pipeline_created_duration = Time.current - Time.parse(coordinator_pipeline.created_at)

            # When a pipeline has failed, we want to give the RM time to investigate and retry if required.
            # Check if at least 90 mins have passed since the pipeline started.
            # This replicates the current behavior where new coordinator pipelines are started approximately
            # every 2 hours.
            JOB_FAILURE_STATUS.include?(job.status) && pipeline_created_duration >= 90.minutes
          end
        end
      end
    end
  end
end
