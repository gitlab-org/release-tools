# frozen_string_literal: true

module ReleaseTools
  module Qa
    class ProjectChangeset
      include ParallelMethods

      SHORT_REF_REGEXP = /(?<path>\S*)!(?<iid>\d+)/
      ABSOLUTE_REF_REGEXP = %r{https://gitlab.com/(?<path>\S+)/-/merge_requests/(?<iid>\d+)}
      REF_REGEXP = Regexp.union(SHORT_REF_REGEXP, ABSOLUTE_REF_REGEXP).freeze
      MERGE_COMMIT_REGEXP = /See merge request #{REF_REGEXP}/

      attr_reader :project, :from, :to

      def initialize(project:, from:, to:)
        @project = project
        @from = from
        @to = to

        verify_refs!(from, to)
      end

      def merge_requests
        @merge_requests ||= gather_merge_requests
      end

      def commits
        @commits ||= ReleaseTools::GitlabClient
          .compare(project.security_path, from: from, to: to)
          .commits
      end

      def shas
        commits.map { |commit| commit['id'] }
      end

      private

      def gather_merge_requests
        mrs = commits.filter_map { |commit| parsed_merge_request(commit) }.uniq
        parallel_map(mrs) { |path, iid| retrieve_merge_request(path, iid) }
      end

      def parsed_merge_request(commit)
        MERGE_COMMIT_REGEXP.match(commit['message']) { |match| [match[:path], match[:iid]] }
      end

      def retrieve_merge_request(path, iid)
        Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.merge_request(path, iid: iid)
        end
      end

      def verify_refs!(*refs)
        refs.each do |ref|
          begin
            ReleaseTools::GitlabClient.commit(project.security_path, ref: ref)
          rescue Gitlab::Error::NotFound
            raise ArgumentError.new("Invalid ref for this repository: #{ref}")
          end
        end
      end
    end
  end
end
