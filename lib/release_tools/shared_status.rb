# frozen_string_literal: true

module ReleaseTools
  module SharedStatus
    extend self

    CRITICAL_RELEASE_LABEL = "severity::1"

    def dry_run?
      !ENV.fetch('TEST', 'false').casecmp?('false')
    end

    def critical_patch_release?
      critical_security_fixes?
    end

    def rspec?
      File.basename($PROGRAM_NAME) == 'rspec'
    end

    def security_release?
      return true if ENV['SECURITY'].present?

      @security_release == true
    end

    # rubocop:disable Style/OptionalBooleanParameter
    def as_security_release(security_release = true)
      @security_release = security_release
      yield
    ensure
      @security_release = false
    end
    # rubocop:enable Style/OptionalBooleanParameter

    def user
      `git config --get user.name`.strip
    end

    private

    def critical_security_fixes?
      security_fixes.present? &&
        security_fixes.any? { |issue| issue.labels.include?(CRITICAL_RELEASE_LABEL) }
    end

    def security_fixes
      @security_fixes ||= ReleaseTools::Security::IssueCrawler.new.related_security_issues
    end
  end
end
