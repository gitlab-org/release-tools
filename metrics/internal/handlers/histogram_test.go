package handlers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type mockHistogram struct {
	mockMetric
}

func (m *mockHistogram) Observe(value float64, labels ...string) {
	m.value = value
}

func TestHistogramVecHandler(t *testing.T) {
	meta := &mockHistogram{}
	meta.expectedLabels = []string{"green", "medium", "pre-production"}
	handler := NewHistogram(meta).(*histogram)

	req, err := meta.apiRequest("observe", "11.1")
	require.NoError(t, err)

	meta.testRequest(t, req, handler, 11.1)

	resetReq, err := meta.apiReset()
	if err != nil {
		t.Fatal(err)
	}

	meta.testRequest(t, resetReq, handler, 0)
}
