package handlers

import (
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

type jobsClient interface {
	ListPipelineJobs(pid interface{}, pipelineID int, opts *gitlab.ListJobsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Job, *gitlab.Response, error)
}

func newClient(token string) (jobsClient, error) {
	gitlab, err := gitlab.NewClient(token, gitlab.WithBaseURL("https://ops.gitlab.net/api/v4"))
	if err != nil {
		return nil, err
	}

	return gitlab.Jobs, nil
}
