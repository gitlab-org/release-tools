package handlers

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	gitlab "gitlab.com/gitlab-org/api/client-go"

	"gitlab.com/gitlab-org/release-tools/metrics/mocks"
)

func TestReleaseToolsJobWebhookHandlerWithWrongToken(t *testing.T) {
	_, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	tokenMap := map[string]string{
		ProjectReleaseToolsOps: "test1",
		ProjectDeployer:        "test2",
	}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)

	req, err := webhookRequest(webhookRequestJson(), "/ops/gitlab-org/release/tools", "test2")
	require.NoError(t, err)

	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusUnauthorized, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusUnauthorized)
}

func TestDeployerJobWebhookHandlerWithWrongToken(t *testing.T) {
	_, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	tokenMap := map[string]string{
		ProjectReleaseToolsOps: "test1",
		ProjectDeployer:        "test2",
	}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)

	req, err := webhookRequest(webhookRequestJson(), "/ops/gitlab-com/gl-infra/deployer", "test1")
	require.NoError(t, err)

	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusUnauthorized, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusUnauthorized)
}

func TestDeployerJobWebhookHandlerWithoutToken(t *testing.T) {
	_, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	tokenMap := map[string]string{ProjectReleaseToolsOps: "test1"}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)

	req, err := webhookRequest(webhookRequestJson(), "/ops/gitlab-com/gl-infra/deployer", "")
	require.NoError(t, err)

	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusUnauthorized, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusUnauthorized)
}

func TestJobWebhookDeployerPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectDeployer,
		"/ops/gitlab-com/gl-infra/deployer",
		"auto-deploy: gprd - 15.9.202301250820-cda2eac9bf1.b2fd4b7b8ae",
	)
}

func TestJobWebhookDeployerPipelineLostSecondsWhenJobFail(t *testing.T) {
	testJobWebhookPipelineForLostSeconds(
		t,
		ProjectDeployer,
		"/ops/gitlab-com/gl-infra/deployer",
		"auto-deploy: gprd - 15.9.202301250820-cda2eac9bf1.b2fd4b7b8ae",
	)
}

func TestJobWebhookDeployerPipelineLostSeconds(t *testing.T) {
	testJobWebhookPipelineForLostSecondsWithRunningPipeline(
		t,
		ProjectDeployer,
		"/ops/gitlab-com/gl-infra/deployer",
		"auto-deploy: gprd - 15.9.202301250820-cda2eac9bf1.b2fd4b7b8ae",
	)
}

func TestJobWebhookReleaseToolsPipeline(t *testing.T) {
	testJobWebhookPipelineName(t, ProjectReleaseToolsOps, "/ops/gitlab-org/release/tools", "Coordinator pipeline")
}

func TestJobWebhookQualityStagingCanaryPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectQualityStagingCanaryOps,
		"/ops/gitlab-org/quality/staging-canary",
		"Deployment QA pipeline - 15.9.202301301120-a8fdcfa502e.541ff7fe8a6",
	)
}

func TestJobWebhookQualityStagingPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectQualityStagingOps,
		"/ops/gitlab-org/quality/staging",
		"Deployment QA pipeline - 15.9.202301301120-a8fdcfa502e.541ff7fe8a6",
	)
}

func TestJobWebhookK8sWorkloadsPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectK8sWorkloadsGitlabComOps,
		"/ops/gitlab-com/gl-infra/k8s-workloads/gitlab-com",
		"auto-deploy: gstg-cny - 15-9-202301301120-a8fdcfa502e",
	)
}

func TestJobWebhookCNGPipeline(t *testing.T) {
	testJobWebhookPipelineName(t, ProjectCNGDev, "/dev/gitlab/charts/components/images", "AUTO_DEPLOY_BUILD_PIPELINE")
}

func TestJobWebhookOmnibusPipeline(t *testing.T) {
	testJobWebhookPipelineName(t, ProjectOmnibusDev, "/dev/gitlab/omnibus-gitlab", "AUTO_DEPLOY_BUILD_PIPELINE")
}

func TestJobWebhookNonAutoDeployPipeline(t *testing.T) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	jsonData := webhookRequestJson()
	jsonData["commit"].(map[string]interface{})["name"] = "Some other pipeline"

	jobEventMetricsCounter.On("Inc", ProjectReleaseToolsOps, "others").Once()

	assertRequest(t, jsonData, "/ops/gitlab-org/release/tools", ProjectReleaseToolsOps)
}

func TestJobWebhookRecordOnlyCreated(t *testing.T) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	jobEventMetricsCounter.On("Inc", ProjectDeployer, "auto_deploy").Once()

	// Set build_status to something other than "created"
	jsonData := webhookRequestJson()
	jsonData["build_status"] = "failed"

	assertRequest(t, jsonData, "/ops/gitlab-com/gl-infra/deployer", ProjectDeployer)
}

func TestJobWebhookRecordOnlyRetried(t *testing.T) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	jobEventMetricsCounter.On("Inc", ProjectDeployer, "auto_deploy").Once()

	jsonData := webhookRequestJson()
	jsonData["retries_count"] = 0

	assertRequest(t, jsonData, "/ops/gitlab-com/gl-infra/deployer", ProjectDeployer)
}

func TestJobFailureLastOccuerrenceClient(t *testing.T) {
	createdAt, _ := time.Parse(time.RFC3339, "2021-02-23T02:41:37.886Z")
	finishedAt, _ := time.Parse(time.RFC3339, "2021-02-23T02:50:37.886Z")
	jobs := []*gitlab.Job{
		{
			CreatedAt:  &createdAt,
			FinishedAt: &finishedAt,
		},
		{
			CreatedAt:  &createdAt,
			FinishedAt: &finishedAt,
		},
	}
	retried := true
	opts := gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
		IncludeRetried: &retried,
	}

	client := newMockJobsClient(t)
	client.On("ListPipelineJobs", 123, 123, &opts).Once().Return(jobs, nil, nil)
	jobs, _, err := client.ListPipelineJobs(123, 123, &opts)

	assert.NoError(t, err)
	assert.Lenf(t, jobs, 2, "this should only return two jobs")
}

func TestJobFailureLastOccuerrencePagination(t *testing.T) {
	layout := time.RFC3339

	createdAt1, _ := time.Parse(layout, "2023-02-23T02:41:37Z")
	finishedAt1, _ := time.Parse(layout, "2023-02-23T02:50:37Z")

	createdAt2, _ := time.Parse(layout, "2024-02-23T02:41:37Z")
	finishedAt2, _ := time.Parse(layout, "2024-02-23T02:50:37Z")

	jobs := []*gitlab.Job{
		{
			CreatedAt:  &createdAt1,
			FinishedAt: &finishedAt1,
			Status:     "failed",
			Name:       "teapot",
		},
	}
	jobs2 := []*gitlab.Job{
		{
			CreatedAt:  &createdAt2,
			FinishedAt: &finishedAt2,
			Status:     "failed",
			Name:       "teaspoon",
		},
	}
	retried := true
	opts := gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
		IncludeRetried: &retried,
	}
	opts2 := gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    2,
			PerPage: 10,
		},
		IncludeRetried: &retried,
	}

	resp := gitlab.Response{
		NextPage: 2,
	}

	resp2 := gitlab.Response{
		NextPage: 0,
	}

	client := newMockJobsClient(t)
	client.On("ListPipelineJobs", 123, 123, &opts).Once().Return(jobs, &resp, nil)
	client.On("ListPipelineJobs", 123, 123, &opts2).Once().Return(jobs2, &resp2, nil)

	oldClient := gitlabOpsJobsClient
	gitlabOpsJobsClient = client
	defer func() { gitlabOpsJobsClient = oldClient }()

	finishedAt := jobFailureLastOccurrence(123, 123, "teaspoon")
	assert.Equal(t, finishedAt, &finishedAt2)
}

func TestJobFailureLastOccuerrence(t *testing.T) {
	layout := time.RFC3339

	createdAt1, _ := time.Parse(layout, "2023-02-23T02:41:37Z")
	finishedAt1, _ := time.Parse(layout, "2023-02-23T02:50:37Z")

	createdAt2, _ := time.Parse(layout, "2024-02-23T02:41:37Z")
	finishedAt2, _ := time.Parse(layout, "2024-02-23T02:50:37Z")

	jobs := []*gitlab.Job{
		{
			CreatedAt:  &createdAt1,
			FinishedAt: &finishedAt1,
			Status:     "failed",
			Name:       "teapot",
		},
		{
			CreatedAt:  &createdAt2,
			FinishedAt: &finishedAt2,
			Status:     "failed",
			Name:       "teaspoon",
		},
	}
	retried := true
	opts := gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
		IncludeRetried: &retried,
	}

	resp := gitlab.Response{
		NextPage: 0,
	}

	client := newMockJobsClient(t)
	client.On("ListPipelineJobs", 123, 123, &opts).Twice().Return(jobs, &resp, nil)

	oldClient := gitlabOpsJobsClient
	gitlabOpsJobsClient = client
	defer func() { gitlabOpsJobsClient = oldClient }()

	finishedAt := jobFailureLastOccurrence(123, 123, "teaspoon")
	assert.Equal(t, finishedAt, &finishedAt2)

	finishedAtNotEqual := jobFailureLastOccurrence(123, 123, "toto")
	assert.Nil(t, finishedAtNotEqual)
}

func testJobWebhookPipelineName(t *testing.T, project, path, pipelineName string) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	jobRetriesCounter, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	jsonData := webhookRequestJson()
	jsonData["commit"].(map[string]interface{})["name"] = pipelineName

	jobEventMetricsCounter.On("Inc", project, "auto_deploy").Once()
	jobRetriesCounter.On("Inc", project, jsonData["build_name"]).Once()

	assertRequest(t, jsonData, path, project)
}

func testJobWebhookPipelineForLostSeconds(t *testing.T, project, path, pipelineName string) {

	jobFailureLostSecondCounter, resetJobFailureLostSecondsStub := stubJobFailuresLostSecondsMetric(t)
	defer resetJobFailureLostSecondsStub()

	jsonData := webhookRequestJson()
	jsonData["build_status"] = "failed"
	jsonData["build_duration"] = 3.0

	jobFailureLostSecondCounter.On("Add", jsonData["build_duration"], project, jsonData["build_name"])

	assertRequest(t, jsonData, path, project)
}

func testJobWebhookPipelineForLostSecondsWithRunningPipeline(t *testing.T, project, path, pipelineName string) {

	jobFailureLostSecondCounter, resetJobFailureLostSecondsStub := stubJobFailuresLostSecondsMetric(t)
	defer resetJobFailureLostSecondsStub()

	jsonData := webhookRequestJson()
	jsonData["build_status"] = "running"
	jsonData["build_created_at"] = "2024-02-23T02:50:30Z"
	jsonData["retries_count"] = 2
	jsonData["build_name"] = "auto_deploy:metrics:end_time"
	jsonData["project_id"] = 123
	jsonData["pipeline_id"] = 1234

	layout := time.RFC3339

	createdAt1, _ := time.Parse(layout, "2023-02-23T02:41:37Z")
	finishedAt1, _ := time.Parse(layout, "2023-02-23T02:50:37Z")

	createdAt2, _ := time.Parse(layout, "2024-02-23T02:41:37Z")
	finishedAt2, _ := time.Parse(layout, "2024-02-23T02:45:37Z")

	jobs := []*gitlab.Job{
		{
			CreatedAt:  &createdAt1,
			FinishedAt: &finishedAt1,
			Status:     "pending",
			Name:       "auto_deploy:metrics:end_time",
		},
		{
			CreatedAt:  &createdAt2,
			FinishedAt: &finishedAt2,
			Status:     "failed",
			Name:       "auto_deploy:metrics:end_time",
		},
	}
	retried := true
	opts := gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
		IncludeRetried: &retried,
	}

	resp := gitlab.Response{
		NextPage: 0,
	}

	client := newMockJobsClient(t)
	client.On("ListPipelineJobs", 123, 1234, &opts).Once().Return(jobs, &resp, nil)

	oldClient := gitlabOpsJobsClient
	gitlabOpsJobsClient = client
	defer func() { gitlabOpsJobsClient = oldClient }()

	times := 293.0

	jobFailureLostSecondCounter.On("Add", times, project, jsonData["build_name"])
	assertRequest(t, jsonData, path, project)
}

func assertRequest(t *testing.T, jsonData map[string]interface{}, path, project string) {
	req, err := webhookRequest(jsonData, path, "test1")
	require.NoError(t, err)

	tokenMap := map[string]string{project: "test1"}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)
	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusOK, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusOK)
}

func stubJobEventMetrics(t *testing.T) (*mocks.Counter, func()) {
	t.Helper()

	origCounter := jobEventMetric
	resetState := func() { jobEventMetric = origCounter }
	counter := mocks.NewCounter(t)

	jobEventMetric = counter

	return counter, resetState
}

func stubJobRetriesMetric(t *testing.T) (*mocks.Counter, func()) {
	t.Helper()

	origCounter := jobRetriesMetric
	resetState := func() { jobRetriesMetric = origCounter }
	counter := mocks.NewCounter(t)

	jobRetriesMetric = counter

	return counter, resetState
}

func stubJobFailuresLostSecondsMetric(t *testing.T) (*mocks.Counter, func()) {
	t.Helper()

	origCounter := jobFailureLostSecondsMetric
	resetState := func() { jobFailureLostSecondsMetric = origCounter }
	counter := mocks.NewCounter(t)

	jobFailureLostSecondsMetric = counter

	return counter, resetState
}
