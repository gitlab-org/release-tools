package metrics

import (
	"fmt"

	"gitlab.com/gitlab-org/release-tools/metrics/internal/metrics/consul"

	"github.com/sirupsen/logrus"
)

type Metadata interface {
	Namespace() string
	Name() string
	Subsystem() string

	CheckLabels(values []string) error
}

type MetricOption func(*descriptionOpts)

func WithName(name string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.name = name
	}
}

func WithSubsystem(subsystem string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.subsystem = subsystem
	}
}

func WithHelp(help string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.help = help
	}
}

// WithLabel defines a new label for the metrics with a list of allowed values.
// NOTE: when multiple labels are defined, it is important to not alter the order of
// invocation of the WithLabel functions, the API interface for labels is positional
// and we should not change the order
func WithLabel(label Label) MetricOption {
	return func(desc *descriptionOpts) {
		desc.labels = append(desc.labels, label)
	}
}

// WithLabelReset initializes the label set provided as parameter.
// Vector metrics are not published until a first read or write operation happens for a given label set,
// this funcition will force a read on the metric initializing the label set to 0
func WithLabelReset(labels ...string) MetricOption {
	return func(desc *descriptionOpts) {
		desc.labelsToInitialize = append(desc.labelsToInitialize, labels)
	}
}

// WithCartesianProductLabelReset initializes the provided label set with an n-th cartesian product of the allowed values.
// NOTE: it must be invoked after all the WithLabel initializers!
func WithCartesianProductLabelReset() MetricOption {
	return func(desc *descriptionOpts) {
		if len(desc.labels) == 0 {
			return
		}

		// index is a slice containing the current permutation of label values indexes.
		//  index[i] will be the current index for desc.labelValues[i],
		//  on each step of the loop we will produce something like this:
		//  given n = len(desc.labelValues)-1
		//   { labelValues[0][index[0]], ... labelValues[i][index[i]], .... labelValues[n][index[n]] }

		index := make([]int, len(desc.labels))
		labelValues := make([][]string, len(desc.labels))
		for i, label := range desc.labels {
			labelValues[i] = label.Values()
		}

		// nextIndex sets index to the next permutation of labels, the index is built incrementing
		//  elements from the end of the slice.
		// Each element goes from the first to the last label value index.
		// This function will increment index values such that for each i>0, 0 <= index[i] < lens(i).
		//
		// Example:
		// - assuming we have 3 labels with 2, 2, and 3 values respectivery,
		// - and index = { 0, 0, 0 }
		//
		// nextIndex will update index with the following values on each invocation:
		// - { 0, 0, 1 }
		// - { 0, 0, 2 }
		// - { 0, 1, 0 }
		// - { 0, 1, 1 }
		// - { 0, 1, 2 }
		// - { 1, 0, 0 }
		// - { 1, 0, 1 }
		// - { 1, 0, 2 }
		// - { 1, 1, 0 }
		// - { 1, 1, 1 }
		// - { 1, 1, 2 }
		// - { 2, 0, 0 } <-- this is not a valid index, we will use this as the exit condition for our initializer
		nextIndex := func() {
			// we loop over the labels from the last one to the first one, increasing each index (and returning)
			//  until we reach the last valid index for that label.
			// We the last index is found, we reset to 0 and move to the next label.
			for labelIdx := len(index) - 1; labelIdx >= 0; labelIdx-- {
				index[labelIdx]++

				// we don't want to go to the next label until we processed all the possible label values for the current labelIdx.
				// here is where we identify if the current values in index are a valid next index and return
				if index[labelIdx] < len(labelValues[labelIdx]) || // the current index[labelIdx] is valid
					labelIdx == 0 { // or we are handling the last index (we loop in backward)

					// here index contains a new permutation
					return
				}

				// here we have already built all the permutations for labelIdx
				// it's time to set it back to 0 and let the loop move to the next label
				index[labelIdx] = 0
			}
		}

		// we loop on every permutation from nextIndex until we reach an invalid index
		for firsLabelSize := len(labelValues[0]); index[0] < firsLabelSize; nextIndex() {
			var labels []string

			for labelIdx, labelValueIdx := range index {
				labels = append(labels, labelValues[labelIdx][labelValueIdx])
			}

			WithLabelReset(labels...)(desc)
		}
	}
}

func WithBuckets(buckets []float64) MetricOption {
	return func(desc *descriptionOpts) {
		desc.buckets = buckets
	}
}

func WithConsulMemory(consulHost string, consulPort int, logger *logrus.Logger) MetricOption {
	return func(desc *descriptionOpts) {
		desc.logger = logger

		consulClient, err := consul.CreateMetricClient(consulHost, consulPort, logger)
		if err != nil {
			logger.
				WithError(err).
				WithField("metric", desc.FullName()).
				Errorf("Could not initialize consulClient for metric. Metric will not be restored from Consul or saved to Consul.")

			return
		}
		desc.consulMetricClient = consulClient
	}
}

func applyMetricOptions(opts []MetricOption) *descriptionOpts {
	desc := &descriptionOpts{
		description: &description{
			labels: make([]Label, 0),
		},
		labelsToInitialize: make([][]string, 0),
	}

	for _, applyOpt := range opts {
		applyOpt(desc)
	}

	return desc
}

type description struct {
	name      string
	subsystem string

	labels []Label
}

type descriptionOpts struct {
	*description

	help string

	labelsToInitialize [][]string

	buckets []float64

	consulMetricClient consul.MetricClientInterface

	logger *logrus.Logger
}

func (d *description) Namespace() string { return "delivery" }
func (d *description) Name() string      { return d.name }
func (d *description) Subsystem() string { return d.subsystem }
func (d *description) FullName() string {
	return fmt.Sprintf("%s_%s_%s", d.Namespace(), d.Subsystem(), d.Name())
}

// CheckLabels verifies user provided labels for cardinality and allowed values
// - values is a slice with user provided label values
//
// values has the same order of m.labels
func (m *description) CheckLabels(values []string) error {
	expectedLabels := len(m.labels)
	if len(values) != expectedLabels {
		return fmt.Errorf("Expected %d labels", expectedLabels)
	}

	for i, labelValue := range values {
		if !m.labels[i].CheckValue(labelValue) {
			return fmt.Errorf("%q is not a valid %q value", labelValue, m.labels[i].Name())
		}
	}

	return nil
}

func (d *description) labelsNames() []string {
	labels := make([]string, len(d.labels))

	for i, label := range d.labels {
		labels[i] = label.Name()
	}

	return labels
}
