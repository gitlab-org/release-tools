package consul

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
)

type ClientInterface interface {
	PutKeyValue(string, []byte) error
	GetKeyValue(string) ([]byte, error)
	DeleteKeyValue(string) error
	DeleteTree(string) error
}

type MetricClientInterface interface {
	IncMetric(metric string, labels []string) error
	DecMetric(metric string, labels []string) error
	SetMetric(metric string, labels []string, metricValue float64) error
	ResetMetric(metric string) error
	GetMetric(metric string, labels []string) (float64, error)
	GetAllMetricValues(metric string) (map[string]float64, error)
}

type Client struct {
	client *api.Client
	logger *logrus.Logger
}

const namespace = "group-delivery/delivery-metrics"
const metricStorageNamespace = "metric-data"

// Creates a new Consul client
func CreateMetricClient(consulHost string, consulPort int, logger *logrus.Logger) (MetricClientInterface, error) {
	config := api.DefaultConfig()
	config.Address = fmt.Sprintf("%s:%d", consulHost, consulPort)
	logger.WithField("address", config.Address).Info("Creating consul client")

	client, err := api.NewClient(config)
	if err != nil {
		logger.WithError(err).Errorf("Failed to create Consul client")
		return nil, err
	}

	return &Client{logger: logger, client: client}, nil
}

// Adds a K/V pair to consul
func (c *Client) PutKeyValue(key string, value []byte) error {
	pair := &api.KVPair{Key: key, Value: value}

	kv := c.client.KV()
	_, err := kv.Put(pair, nil)

	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", key).
			WithField("value", value).
			Errorf("Failed to put key")

		return err
	}

	return nil
}

// Gets a value from consul
func (c *Client) GetKeyValue(key string) ([]byte, error) {
	kv := c.client.KV()

	pair, _, err := kv.Get(key, nil)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", key).
			Errorf("Failed to get key")

		return nil, err
	}

	if pair == nil {
		c.logger.
			WithField("key", key).
			Info("Key is not present in Consul")
		return nil, nil
	}

	return pair.Value, nil
}

// Deletes a value from consul
func (c *Client) DeleteKeyValue(key string) error {
	kv := c.client.KV()

	_, err := kv.Delete(key, nil)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", key).
			Errorf("Failed to delete key")

		return err
	}

	return nil
}

func (c *Client) DeleteTree(key string) error {
	kv := c.client.KV()

	_, err := kv.DeleteTree(key, nil)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", key).
			Errorf("Failed to delete key")

		return err
	}

	return nil
}

func (c *Client) IncMetric(metric string, labels []string) error {
	metricValue, err := c.GetMetric(metric, labels)
	if err != nil {
		return err
	}

	metricValue++

	return c.SetMetric(metric, labels, metricValue)
}

func (c *Client) DecMetric(metric string, labels []string) error {
	metricValue, err := c.GetMetric(metric, labels)
	if err != nil {
		return err
	}

	metricValue--

	return c.SetMetric(metric, labels, metricValue)
}

func (c *Client) SetMetric(metric string, labels []string, metricValue float64) error {
	key := metricLabelsKey(metric, labels)

	str := strconv.FormatFloat(metricValue, 'f', -1, 64)

	return c.PutKeyValue(key, []byte(str))
}

func (c *Client) ResetMetric(metric string) error {
	return c.DeleteTree(metricTree(metric))
}

func (c *Client) GetMetric(metric string, labels []string) (float64, error) {
	key := metricLabelsKey(metric, labels)

	val, err := c.GetKeyValue(key)
	if err != nil {
		return 0, err
	}

	if val == nil {
		return 0, nil
	}

	metricValue, err := c.getMetricValue(val)

	return metricValue, err
}

// GetAllMetricValues returns a map of (value => labels) where the metric value is the map key, and the labels slice
// is the map value.
func (c *Client) GetAllMetricValues(metric string) (map[string]float64, error) {
	kv := c.client.KV()

	treeKey := metricTree(metric)
	pairs, _, err := kv.List(treeKey, nil)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", treeKey).
			Errorf("Failed to get list of keys")

		return nil, err
	}

	values := make(map[string]float64)
	for _, pair := range pairs {
		if pair.Value == nil {
			continue
		}

		metricValue, err := c.getMetricValue(pair.Value)
		if err != nil {
			logrus.
				WithError(err).
				WithField("metric", metric).
				WithField("key", pair.Key).
				Errorf("Could not parse metric value from Consul")

			continue
		}

		labels := labelsFromKey(pair.Key)
		values[labels] = metricValue
	}

	return values, nil
}

func (c *Client) getMetricValue(val []byte) (float64, error) {
	metricValue, err := strconv.ParseFloat(string(val), 64)

	if err != nil {
		c.logger.
			WithError(err).
			WithField("value", val).
			WithField("value_as_string", string(val)).
			Errorf("Could not convert bytes to float64")

		return 0, err
	}

	return metricValue, nil
}

// labelsFromKey extracts the labels from the key and returns them as a string.
func labelsFromKey(key string) string {
	splitKey := strings.Split(key, "/")
	return splitKey[len(splitKey)-1]
}

func rootKey() string {
	return namespace
}

func metricLabelsKey(metric string, labels []string) string {
	return fmt.Sprintf("%s/%s", metricTree(metric), strings.Join(labels, ","))
}

func metricTree(metric string) string {
	return fmt.Sprintf("%s/%s/%s", rootKey(), metricStorageNamespace, metric)
}
