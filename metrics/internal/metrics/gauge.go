package metrics

import (
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"gitlab.com/gitlab-org/release-tools/metrics/internal/metrics/consul"
)

type Gauge interface {
	Metadata
	Inc(labels ...string)
	Dec(labels ...string)
	Set(value float64, labels ...string)
	Reset()
}

type gaugeVec struct {
	*description

	consulClient consul.MetricClientInterface

	metric *prometheus.GaugeVec
}

func NewGaugeVec(opts ...MetricOption) (Gauge, error) {
	descOpts := applyMetricOptions(opts)

	prom := promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: descOpts.Namespace(),
			Subsystem: descOpts.Subsystem(),
			Name:      descOpts.Name(),
			Help:      descOpts.help,
		}, descOpts.labelsNames())

	for _, labels := range descOpts.labelsToInitialize {
		_, e := prom.GetMetricWithLabelValues(labels...)
		if e != nil {
			return nil, e
		}
	}

	gauge := &gaugeVec{
		description: descOpts.description,
		metric:      prom,
	}

	if descOpts.consulMetricClient == nil {
		return gauge, nil
	}

	gauge.consulClient = descOpts.consulMetricClient

	values, err := gauge.consulClient.GetAllMetricValues(descOpts.FullName())
	if err != nil {
		descOpts.logger.
			WithError(err).
			WithField("metric", descOpts.FullName()).
			Errorf("Could not restore metric data from Consul")

		// Ignore error so that we don't prevent metrics from operating.
		return gauge, nil
	}

	for labels, value := range values {
		labelsSlice := strings.Split(labels, ",")
		gauge.metric.WithLabelValues(labelsSlice...).Set(value)
	}

	descOpts.logger.
		WithField("metric", descOpts.FullName()).
		WithField("data", values).
		Info("Successfully loaded metric data from Consul")

	return gauge, nil
}

func (g *gaugeVec) Inc(labels ...string) {
	g.metric.WithLabelValues(labels...).Inc()

	if g.consulClient != nil {
		_ = g.consulClient.IncMetric(g.FullName(), labels)
	}
}

func (g *gaugeVec) Dec(labels ...string) {
	g.metric.WithLabelValues(labels...).Dec()

	if g.consulClient != nil {
		_ = g.consulClient.DecMetric(g.FullName(), labels)
	}
}

func (g *gaugeVec) Set(value float64, labels ...string) {
	g.metric.WithLabelValues(labels...).Set(value)

	if g.consulClient != nil {
		_ = g.consulClient.SetMetric(g.FullName(), labels, value)
	}
}

func (g *gaugeVec) Reset() {
	g.metric.Reset()

	if g.consulClient != nil {
		_ = g.consulClient.ResetMetric(g.FullName())
	}
}
