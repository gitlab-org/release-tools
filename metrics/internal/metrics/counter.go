package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Counter interface {
	Metadata
	Inc(labels ...string)
	Reset()
	Add(value float64, labels ...string)
}

type counterVec struct {
	*description

	metric *prometheus.CounterVec
}

func NewCounterVec(opts ...MetricOption) (Counter, error) {
	descOpts := applyMetricOptions(opts)

	prom := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: descOpts.Namespace(),
			Subsystem: descOpts.Subsystem(),
			Name:      descOpts.Name(),
			Help:      descOpts.help,
		}, descOpts.labelsNames())

	for _, labels := range descOpts.labelsToInitialize {
		_, e := prom.GetMetricWithLabelValues(labels...)
		if e != nil {
			return nil, e
		}
	}

	return &counterVec{
		description: descOpts.description,
		metric:      prom,
	}, nil
}

func (c *counterVec) Inc(labels ...string) {
	c.metric.WithLabelValues(labels...).Inc()
}

func (c *counterVec) Add(value float64, labels ...string) {
	c.metric.WithLabelValues(labels...).Add(value)
}

func (c *counterVec) Reset() {
	c.metric.Reset()
}
