package labels

func Nil(name string) *base {
	return &base{name: name}
}

type base struct {
	name string
}

func (b *base) Name() string {
	return b.name
}

func (r *base) Values() []string {
	return nil
}

func (r *base) CheckValue(value string) bool {
	return false
}
